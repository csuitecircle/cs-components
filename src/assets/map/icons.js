/**
 * Some of the icons used in the map component
 */

function getMarkerIcon(color) {
  return {
    path:
      'M19.75,4a12.741,12.741,0,0,0-9.68,21.036c.227.257,5.57,6.329,7.355,8.031a3.364,3.364,0,0,0,4.651,0c2.075-1.979,7.143-7.789,7.365-8.044A12.741,12.741,0,0,0,19.75,4Zm0,16.5a3.75,3.75,0,1,1,3.75-3.75A3.75,3.75,0,0,1,19.75,20.5Z',
    fillColor: color,
    fillOpacity: 1,
    strokeColor: 'white',
    strokeWeight: 0,
    anchor: new window.google.maps.Point(15, 15),
  };
}

function getPointIcon(color) {
  return {
    path:
      'M 12 3 A 9.5 9.5 0 0 0 3 12 A 9.5 9.5 0 0 0 12 22 A 9.5 9.5 0 0 0 22 12 A 9.5 9.5 0 0 0 12 3 z',
    fillColor: color,
    fillOpacity: 1,
    strokeColor: 'white',
    strokeWeight: 0,
    anchor: new window.google.maps.Point(12, 12),
  };
}

function getMarkerForStaticMap(color) {
  if (!color) return '';
  color = color.replace(/[ #]*/, '');
  return `data:image/svg+xml;utf-8, <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 25.5 30" width="50px" height="50px"><path fill="%23${color}" stroke="white" stroke-width="0" d="M19.75,4a12.741,12.741,0,0,0-9.68,21.036c.227.257,5.57,6.329,7.355,8.031a3.364,3.364,0,0,0,4.651,0c2.075-1.979,7.143-7.789,7.365-8.044A12.741,12.741,0,0,0,19.75,4Zm0,16.5a3.75,3.75,0,1,1,3.75-3.75A3.75,3.75,0,0,1,19.75,20.5Z" transform="translate(-7 -4)"/></svg>`;
}

const markerIcon = {
  url:
    'data:image/svg+xml;utf-8, <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 25.5 30" width="50px" height="50px"><path fill="#b5c0c7" d="M19.75,4a12.741,12.741,0,0,0-9.68,21.036c.227.257,5.57,6.329,7.355,8.031a3.364,3.364,0,0,0,4.651,0c2.075-1.979,7.143-7.789,7.365-8.044A12.741,12.741,0,0,0,19.75,4Zm0,16.5a3.75,3.75,0,1,1,3.75-3.75A3.75,3.75,0,0,1,19.75,20.5Z" transform="translate(-7 -4)"/></svg>',
};

const pointIcon = {
  url:
    'data:image/svg+xml;utf-8, <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 25.5 30" width="50px" height="50px"><path fill="#b5c0c7" d="M 12 2 A 10 10 0 0 0 2 12 A 10 10 0 0 0 12 22 A 10 10 0 0 0 22 12 A 10 10 0 0 0 12 2 z" transform="translate(-7 -4)"/></svg>',
};

export {
  markerIcon,
  pointIcon,
  getMarkerIcon,
  getPointIcon,
  getMarkerForStaticMap,
};
