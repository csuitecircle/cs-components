import { arrayIncludes } from '@/utils/utils';

export const variantValidator = (x) => {
  const variants = [
    'primary',
    'secondary',
    'info',
    'success',
    'warning',
    'danger',
    'default',
  ];
  return arrayIncludes(variants, x);
};

export const sizeValidator = (x) => {
  const variants = ['small', 'medium', 'large'];
  return arrayIncludes(variants, x);
};

export const inputSizeValidator = (x) => {
  const variants = ['medium', 'large'];
  return arrayIncludes(variants, x);
};

export const fillValidator = (x) => {
  const variants = ['solid', 'outline', 'clear'];
  return arrayIncludes(variants, x);
};

export const alignValidator = (x) => {
  const variants = ['top', 'bottom'];
  return arrayIncludes(variants, x);
};

export const layoutOptions = {
  null: '',
  small: 'small',
  medium: 'medium',
  large: 'large',
};
const layoutValues = Object.values(layoutOptions);

export const layoutValidator = (x) => {
  return arrayIncludes(layoutValues, x);
};
