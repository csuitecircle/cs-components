// Form events: ['blur', 'change', 'contextmenu', 'focus', 'input', 'invalid', 'reset', 'search', 'select', 'submit',]
// Keyboard events: ['keyup', 'keydown', 'keypress',]
// Don't include any events that are emitted by the component or they will be emitted twice

export const checkboxToggleEvents = ['blur', 'change', 'contextmenu', 'focus'];

export const datetimeEvents = ['blur', 'change', 'contextmenu', 'focus'];

export const inputEvents = [
  'blur',
  'change',
  'contextmenu',
  'focus',
  'invalid',
  'reset',
  'search',
  'select',
  'submit',
  'keyup',
  'keydown',
  'keypress',
];

export const placeEvents = [
  'blur',
  'change',
  'contextmenu',
  'focus',
  'keyup',
  'keydown',
  'keypress',
];

export const selectEvents = ['blur', 'change', 'contextmenu', 'focus'];

export const textareaEvents = [
  'blur',
  'change',
  'contextmenu',
  'focus',
  'keyup',
  'keydown',
  'keypress',
];
