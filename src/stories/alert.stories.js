import { action } from '@storybook/addon-actions';
import { withKnobs, text, select, boolean } from '@storybook/addon-knobs';
import CsAlert from '../components/Alert.vue';

const variantOptions = ['primary', 'secondary', 'success', 'warning', 'danger', 'info', 'default'];

export default {
  title: 'Alerts',
  component: CsAlert,
  decorators: [withKnobs],
};

export const Default = () => ({
  components: { CsAlert },
  props: {
    variant: {
      default: select('variant', variantOptions, 'default'),
    },
    title: {
      default: text('title', 'Form Completed.'),
    },
    content: {
      default: text(
        'content',
        'Form Completed. You’re all signed up for the newsletter Check it please or contact us.'
      ),
    },
    icon: {
      default: text('icon'),
    },
    showIcon: {
      default: boolean('show-icon', false),
    },
    canClose: {
      default: boolean('can-close', false),
    },
    closeIcon: {
      default: text('close-icon'),
    },
  },
  template: `
    <cs-alert
      :variant="variant"
      :title="title"
      :content="content"
      :icon="icon"
      :show-icon="showIcon"
      :can-close="canClose"
      :close-icon="closeIcon"
      @close="close"
    />
  `,
  methods: { close: action('close') },
});

export const Slots = () => ({
  components: { CsAlert },
  props: {
    variant: {
      default: select('variant', variantOptions, 'info'),
    },
    showIcon: {
      default: boolean('show-icon', false),
    },
    canClose: {
      default: boolean('can-close', false),
    },
  },
  template: `
    <cs-alert
      :variant="variant"
      :show-icon="showIcon"
      :can-close="canClose"
      @close="close"
    >
      <i slot="icon" class="cs-icons-email-filled"/>
      <div slot="title">Info about email</div>
      <div>Content with no slot name</div>
      <div slot="content">You can only sign for an <b>account</b> once with a <em>given</em> e-mail address. Check it please or contact us.</div>
      <i slot="close-icon" class="cs-icons-delete"/>
    </cs-alert>
  `,
  methods: { close: action('close') },
});
