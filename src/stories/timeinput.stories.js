import { withKnobs, text, select, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsTimeInput from '../components/TimeInput.vue';

import { datetimeEvents } from '@/utils/formevents';
import formEventsMixin from './mixins/formevents';

export default {
  title: 'Form/Time Input',
  component: CsTimeInput,
  decorators: [withKnobs],
};

export const TimeInput = () => ({
  mixins: [formEventsMixin],
  components: { CsTimeInput },
  props: {
    label: {
      default: text('label', 'Start Time'),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    required: {
      default: boolean('required', false),
    },
    icon: {
      default: text('icon'),
    },
    value: {
      default: text('value', new Date().toGMTString()),
    },
    requiredLabel: {
      default: text('requiredLabel', '* Required'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
  },
  data() {
    return {
      val: new Date(),
      $_formEventTriggers: datetimeEvents,
    };
  },
  template: `
    <div>
      <cs-time-input
        v-model="value"
        :label="label"
        :disabled="disabled"
        :required="required"
        :icon="icon"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        @input="onInput"
        @validate="onValidate"
        v-on="$_formEvents"
      />
      <cs-time-input
        v-model="value"
        :label="label"
        :disabled="disabled"
        :required="required"
        :icon="icon"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        :success="true"
      > 
        <cs-input-note slot="input-notes" v-if="true" type="success" label="This is a success message"/>
      </cs-time-input>
      <cs-time-input
        v-model="value"
        :label="label"
        :disabled="disabled"
        :required="required"
        :icon="icon"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        :invalid="true"
      > 
        <cs-input-note slot="input-notes" v-if="value !== null" type="info" label="This is a default message"/>
        <cs-input-note slot="input-notes" v-if="value !== null" type="error" label="This is an error message"/>
      </cs-time-input>
    </div>
    `,
  methods: {
    onInput: action('input'),
    onValidate: action('validate'),
  },
});
