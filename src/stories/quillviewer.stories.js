import { withKnobs, text, boolean, select } from '@storybook/addon-knobs';

import CsQuillViewer from '../components/QuillViewer.vue';

export default {
  title: 'Quill Viewer',
  component: CsQuillViewer,
  decorators: [withKnobs],
};

export const QuillViewer = () => ({
  components: {
    CsQuillViewer,
  },
  props: {
    body: {
      default: text('body', ''),
    },
  },
  template: `
    <cs-quill-viewer v-model="body"></cs-quill-viewer>`,
});
