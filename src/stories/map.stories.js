import { action } from '@storybook/addon-actions';
import { withKnobs, text, number, object, color, boolean } from '@storybook/addon-knobs';
import CsMap from '../components/Map.vue';

export default {
  component: CsMap,
  title: 'Map',
  decorators: [withKnobs],
};

const markers = [
  {
    title: 'Bondi Beach',
    lat: -33.890542,
    lng: 151.274856,
    location: '168 Green Mountain Apt. 092',
    description: 'Details',
  },
  {
    title: 'Manly Beach',
    lat: -33.80010128657071,
    lng: 151.28747820854187,
  },
  {
    title: 'Cronulla Beach',
    lat: -34.028249,
    lng: 151.157507,
  },
  {
    title: 'Maroubra Beach',
    lat: -33.950198,
    lng: 151.259302,
  },
  {
    title: 'Coogee Beach',
    lat: -33.923036,
    lng: 151.259052,
  },
];

export const Default = () => ({
  components: { CsMap },
  props: {
    apiKey: {
      default: text('api-key'),
    },
    validApiKeyLabel: {
      default: text('valid-api-key-label')
    }
  },
  template: `
    <cs-map
      :api-key="apiKey"
      :valid-api-key-label="validApiKeyLabel"
    ></cs-map>
  `,
});

Default.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const Custom = () => ({
  components: { CsMap },
  props: {
    apiKey: {
      default: text('api-key'),
    },
    zoom: {
      default: number('zoom', 10),
    },
    showZoomControls: {
      default: boolean('show-zoom-controls', true),
    },
    center: {
      default: object('center', { lat: -33.890542, lng: 151.274856 }),
    },
    markers: {
      default: object('markers', markers),
    },
    height: {
      default: text('--cs-map-height', '50vh'),
    },
    width: {
      default: text('--cs-map-width', '100%'),
    },
    iconColor: {
      default: color('--cs-map-marker-color', '#00C472'),
    },
    validApiKeyLabel: {
      default: text('valid-api-key-label')
    }
    
  },
  template: `
    <cs-map
      @tap="onTooltipTap"
      :center="center"
      :api-key="apiKey"
      :show-zoom-controls="showZoomControls"
      :markers="markers"
      :valid-api-key-label="validApiKeyLabel"
      :zoom="zoom"
      :style="{
        '--cs-map-height': height,
        '--cs-map-width': width,
        '--cs-map-marker-color': iconColor
      }"
    ></cs-map>
  `,
  methods: { onTooltipTap: action('tap') },
});

Custom.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};
