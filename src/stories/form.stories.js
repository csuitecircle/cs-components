import { action } from '@storybook/addon-actions';
import { withKnobs, text, select, boolean, number } from '@storybook/addon-knobs';

import CsForm from '../components/Form.vue';
import CsInput from '../components/Input.vue';
import CsCheckbox from '../components/Checkbox.vue';
import CsDateInput from '../components/DateInput.vue';
import CsTimeInput from '../components/TimeInput.vue';
import CsPlaceInput from '../components/PlaceInput.vue';
import CsFileInput from '../components/FileInput.vue';
import CsRadioGroup from '../components/RadioGroup.vue';
import CsSelect from '../components/Select.vue';
import CsTextarea from '../components/Textarea.vue';
import CsToggle from '../components/Toggle.vue';

export default {
  title: 'Form/Form',
  component: CsForm,
  decorators: [withKnobs],
};

export const Form = () => ({
  components: {
    CsForm,
    CsCheckbox,
    CsInput,
    CsDateInput,
    CsTimeInput,
    CsPlaceInput,
    CsFileInput,
    CsRadioGroup,
    CsSelect,
    CsTextarea,
    CsToggle,
  },
  data() {
    return {
      formValid: true,
      selectOptions: [
        {
          label: 'Africa',
          value: 'af',
        },
        {
          label: 'Antarctica',
          value: 'aa',
        },
        {
          label: 'Asia',
          value: 'as',
        },
        {
          label: 'Australia',
          value: 'au',
        },
        {
          label: 'Europe',
          value: 'eu',
        },
        {
          label: 'North America',
          value: 'na',
        },
        {
          label: 'South America',
          value: 'sa',
        },
      ],
      selectRadioValue: '',
    };
  },
  props: {
    appId: {
      default: text('cs-file-input application-id', ''),
    },
    apiKey: {
      default: text('cs-place-input api-key', ''),
    },
  },
  template: `
		<cs-form v-model="formValid">
			<div>Form valid: {{ formValid }} (valid status just for reference)</div><br/>

			<cs-input
				label="First name (min 3, max 12)"
				required
				placeholder="Input"
				:min-length="3"
				:max-length="12"
			/>
			<cs-date-input
				label="Date of birth"
				required
			/>
			<cs-time-input
				label="Appointment time"
				required
			/>
			<cs-textarea
				label="Description"
				placeholder="Textarea"
				required
			/>
			<cs-toggle
				label-checked="I want to share all my data"
				label-unchecked="I do not want to share all my data"
				required
			/>
			<cs-checkbox
				required
				label="I accept your terms and conditions"
			/>
			<cs-file-input
				label="Upload a file"
				:application-id="appId"
				value="http://picsum.photos/300"
				required
			/>
			<cs-select
				label="Select your continent"
				:options="selectOptions"
				required
				v-model="selectRadioValue"
			/>
			<cs-radio-group
				label="Radio group mapped to same data as Select"
				:options="selectOptions"
				required
				v-model="selectRadioValue"
			/>
			<cs-place-input
				label="Find a place"
				:api-key="apiKey"
				required
			/>

      <cs-button :disabled="!formValid">Submit</cs-button>
		</cs-form>`,
  methods: {
    onInput: action('input'),
  },
});
