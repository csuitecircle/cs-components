import { action } from '@storybook/addon-actions';
import {
  withKnobs,
  text,
  select,
  boolean,
  number,
} from '@storybook/addon-knobs';

import CsInput from '../components/Input.vue';

import { inputEvents } from '@/utils/formevents';
import formEventsMixin from './mixins/formevents';

export default {
  title: 'Form/Input',
  component: CsInput,
  decorators: [withKnobs],
};

const InputTypeOptions = [
  'text',
  'password',
  'email',
  'number',
  'url',
  'tel',
  'search',
  'color',
];

export const Input = () => ({
  mixins: [formEventsMixin],
  components: { CsInput },
  data() {
    return {
      val: '',
      hasNumbers: null,
      $_formEventTriggers: inputEvents,
    };
  },
  props: {
    label: {
      default: text('label', 'Input label'),
    },
    placeholder: {
      default: text('placeholder', ''),
    },
    type: {
      default: select('type', InputTypeOptions, 'text'),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    required: {
      default: boolean('required', false),
    },
    minLength: {
      default: number('min-length', 3),
    },
    maxLength: {
      default: number('max-length', 8),
    },
    successLabel: {
      default: text('success-label', ''),
    },
    defaultLabel: {
      default: text('default-label', ''),
    },
    successIcon: {
      default: text('cs-icons-check-success'),
    },
    errorIcon: {
      default: text('cs-icons-error'),
    },
    value: {
      default: text('value'),
    },
  },
  template: `<div>
    <cs-input
			:value="value"
      :label="label"
      :type="type"
      :disabled="disabled"
      :required="required"
      :placeholder="placeholder"
			:min-length="minLength"
      :max-length="maxLength"
      :success-label="successLabel"
      :default-label="defaultLabel"
      :success-icon="successIcon"
      :error-icon="errorIcon"
			@input="onInput"
      v-on="$_formEvents"
    />
    <cs-input
			v-model="val"
      label="Input with custom notes/validation"
      :placeholder="placeholder"
      :required="required"
			:min-length="minLength"
      :max-length="maxLength"
			@validate="checkForNumbers"
      :invalid="hasNumbers"
    >
      <cs-input-note slot="input-notes" v-if="hasNumbers === null" type="info" label="This input does not accept numbers"/>
      <cs-input-note slot="input-notes" v-if="hasNumbers === false" type="success" label="No numbers"/>
      <cs-input-note slot="input-notes" v-if="hasNumbers === true" type="error" label="You have entered numbers"/>
    </cs-input>
	</div>`,
  methods: {
    onInput: action('input'),
    checkForNumbers() {
      if (!this.val) {
        this.hasNumbers = null;
        return;
      }
      const matches = this.val.match(/\d+/g);
      this.hasNumbers = matches != null;
    },
  },
});
