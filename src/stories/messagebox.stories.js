import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs';
import { layoutOptions } from '../utils/validators';

import CsMessageBox from '../components/MessageBox.vue';

export default {
  component: CsMessageBox,
  title: 'Message Box',
  decorators: [withKnobs],
};

const variantOptions = ['primary', 'secondary', 'success', 'warning', 'danger', 'info', 'default'];
const alignOptions = ['top', 'bottom'];

export const Mobile = () => ({
  components: { CsMessageBox },
  props: {
    placeholder: {
      default: text('placeholder'),
    },
    icon: {
      default: text('icon (visible on mobile)'),
    },
    showBorder: {
      default: boolean('show-border'),
    },
    variant: {
      default: select('variant', variantOptions, 'primary'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  data() {
    return {
      val: '',
    };
  },
  template: `
    <cs-message-box
      v-model="val"
      :placeholder="placeholder"
      :icon="icon"
      :variant="variant"
      :show-border="showBorder"
      :layout="layout"
      @send="send"
    />
  `,
  methods: {
    send: action('send'),
  },
});
Mobile.parameters = {
  viewport: {
    defaultViewport: 'mobile1',
  },
};

export const Tablet = () => ({
  components: { CsMessageBox },
  props: {
    placeholder: {
      default: text('placeholder'),
    },
    buttonLabel: {
      default: text('button-label (visible on desktop)'),
    },
    showBorder: {
      default: boolean('show-border'),
    },
    variant: {
      default: select('variant', variantOptions, 'primary'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  data() {
    return {
      val: '',
    };
  },
  template: `
      <cs-message-box
        v-model="val"
        :placeholder="placeholder"
        :button-label="buttonLabel"
        :variant="variant"
        :show-border="showBorder"
        :layout="layout"
        @send="send"
      >
      </cs-message-box >
    `,
  methods: { send: action('Sent') },
});
Tablet.parameters = {
  viewport: {
    defaultViewport: 'tablet',
  },
};

export const Attachments = () => ({
  components: { CsMessageBox },
  props: {
    placeholder: {
      default: text('placeholder'),
    },
    buttonLabel: {
      default: text('button-label'),
    },
    icon: {
      default: text('icon'),
    },
    addIcon: {
      default: text('add-icon'),
    },
    cancelIcon: {
      default: text('cancel-icon'),
    },
    showBorder: {
      default: boolean('show-border'),
    },
    variant: {
      default: select('variant', variantOptions, 'primary'),
    },
    align: {
      default: select('align', alignOptions, 'top'),
    },
    showAttachments: {
      default: boolean('show attachments initially', true)
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  data() {
    return {
      val: '',
    };
  },
  template: `
    <cs-message-box
      v-model="val"
      :placeholder="placeholder"
      :buttonLabel="buttonLabel"
      :icon="icon"
      :addIcon="addIcon"
      :cancelIcon="cancelIcon"
      :variant="variant"
      :show-border="showBorder"
      :align="align"
      :show-attachments="showAttachments"
      :layout="layout"
      @send="send"
    >
      <i slot="attachment-icon" class="cs-icons-twitter" @click="onAttachTwitter"/>
      <i slot="attachment-icon" class="cs-icons-email" @click="onAttachEmail"/>
    </cs-message-box>
  `,
  methods: {
    send: action('send'),
    onAttachTwitter: action('twitter'),
    onAttachEmail: action('email'),
  },
});

export const MediaSlot = () => ({
  components: { CsMessageBox },
  props: {
    placeholder: {
      default: text('placeholder'),
    },
    buttonLabel: {
      default: text('button-label'),
    },
    icon: {
      default: text('icon'),
    },
    addIcon: {
      default: text('add-icon'),
    },
    cancelIcon: {
      default: text('cancel-icon'),
    },
    showBorder: {
      default: boolean('show-border'),
    },
    variant: {
      default: select('variant', variantOptions, 'primary'),
    },
    align: {
      default: select('align', alignOptions, 'bottom'),
    },
    showAttachments: {
      default: boolean('show attachments initially', false)
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  data() {
    return {
      val: '',
    };
  },
  template: `
    <cs-message-box
      v-model="val"
      :placeholder="placeholder"
      :buttonLabel="buttonLabel"
      :icon="icon"
      :addIcon="addIcon"
      :cancelIcon="cancelIcon"
      :variant="variant"
      :show-border="showBorder"
      :align="align"
      :show-attachments="showAttachments"
      :layout="layout"
      @send="send"
    >
      <img slot="media" src="https://picsum.photos/300">
      <i slot="attachment-icon" class="cs-icons-twitter" @click="onAttachTwitter"/>
      <i slot="attachment-icon" class="cs-icons-email" @click="onAttachEmail"/>
    </cs-message-box>
  `,
  methods: {
    send: action('send'),
    onAttachTwitter: action('twitter'),
    onAttachEmail: action('email'),
  },
});

MediaSlot.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};