import { action } from '@storybook/addon-actions';
import { withKnobs, boolean, color, text } from '@storybook/addon-knobs';
import CsToggle from '../components/Toggle.vue';
import CsToggleGroup from '../components/ToggleGroup.vue';

import { checkboxToggleEvents } from '@/utils/formevents';
import formEventsMixin from './mixins/formevents';

export default {
  title: 'Form/Toggle',
  components: [CsToggle, CsToggleGroup],
  decorators: [withKnobs],
};

export const Toggle = () => ({
  mixins: [formEventsMixin],
  components: { CsToggle },
  data() {
    return {
      $_formEventTriggers: checkboxToggleEvents,
    };
  },
  props: {
    labelChecked: {
      default: text('label-checked', 'On'),
    },
    labelUnchecked: {
      default: text('label-unchecked', 'Off'),
    },
    value: {
      default: boolean('value'),
    },
    disabled: {
      default: boolean('disabled'),
    },
    required: {
      default: boolean('required', false),
    },
    color: {
      default: color('--cs-toggle-color'),
    },
    requiredLabel: {
      default: text('requiredLabel', '* Required'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
  },
  template: `
		<div>
	    <cs-toggle
	      :value="value"
	      :label-checked="labelChecked"
		  	:label-unchecked="labelUnchecked"
				:disabled="disabled"
        :required="required"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
				:style="{
					'--cs-toggle-color': color
				}"
				@input="onInput"
        @validate="onValidate"
        v-on="$_formEvents"
	    />
			<cs-toggle
	      :value="value"
				:disabled="disabled"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
				:style="{
					'--cs-toggle-color': color
				}"
				@input="onInput"
	      @change="onChange"
        @validate="onValidate"
	    >
				<template slot="label-checked">(slot) Yes</template>
				<template slot="label-unchecked">(slot) No</template>
			</cs-toggle>
      <cs-toggle
	      :value="value"
	      :label-checked="labelChecked"
		  	:label-unchecked="labelUnchecked"
				:disabled="disabled"
        :required="required"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        :success="true"
				:style="{
					'--cs-toggle-color': color
				}"
	    >
        <cs-input-note slot="input-notes" v-if="true" type="success" label="This is a success message"/>
      </cs-toggle>
      <cs-toggle
	      :value="value"
	      :label-checked="labelChecked"
		  	:label-unchecked="labelUnchecked"
				:disabled="disabled"
        :required="required"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        :invalid="true"
				:style="{
					'--cs-toggle-color': color
				}"
	    >
        <cs-input-note slot="input-notes" v-if="value !== null" type="info" label="This is a default message"/>
        <cs-input-note slot="input-notes" v-if="value !== null" type="error" label="This is an error message"/>
      </cs-toggle>
		</div>
  `,
  methods: {
    onChange: action('change'),
    onInput: action('input'),
    onValidate: action('validate'),
  },
});

export const ToggleGroup = () => ({
  components: { CsToggleGroup, CsToggle },
  props: {
    label: {
      default: text('label', 'Toggle Group'),
    },
    color: {
      default: color('--cs-toggle-group-color'),
    },
  },
  template: `
		<div>
      <cs-toggle-group :label="label">
        <cs-toggle
          label-checked="This is checked!"
          label-unchecked="This is unchecked :("
          :style="{
            '--cs-toggle-color': color
          }"
          @input="onInput"
          @change="onChange"
        />
        <cs-toggle
          :style="{
            '--cs-toggle-color': color
          }"
          @input="onInput"
          @change="onChange"
        >
          <template slot="label-checked">(slot) Yes</template>
          <template slot="label-unchecked">(slot) No</template>
        </cs-toggle>
      </cs-toggle-group>
		</div>
  `,
  methods: {
    onChange: action('change'),
    onInput: action('input'),
  },
});
