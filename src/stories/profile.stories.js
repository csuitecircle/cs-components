import { withKnobs, text, boolean, color, select } from '@storybook/addon-knobs';
import CsProfile from '../components/Profile.vue';
import CsSocialIconButton from '../components/SocialIconButton.vue';

const positionOptions = ['center', 'left'];

const sizeOptions = ['small', 'medium', 'large'];

export default {
  title: 'Profile',
  component: CsProfile,
  subcomponents: { CsSocialIconButton },
  decorators: [withKnobs],
};

export const Default = () => ({
  components: { CsProfile, CsSocialIconButton },
  props: {
    name: {
      default: text('name', 'John Doe'),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/300'),
    },
    position: {
      default: select('avatar-position', positionOptions, 'left'),
    },
    size: {
      default: select('avatar-size', sizeOptions, 'medium'),
    },
    noAvatar: {
      default: boolean('no-avatar'),
    },
    detail: {
      default: text('detail', 'CEO at C-Suite Circle'),
    },
    logo: {
      default: text('company-logo'),
    },
    location: {
      default: text('location', '56 cities'),
    },
    color: {
      default: color('--cs-profile-background'),
    },
    href: {
      default: text('href', ''),
    },
    to: {
      default: text('to (Router link)', ''),
    },
  },
  template: `
    <div style="margin-top: 15px;">
      <cs-profile
        :picture="picture"
        :name="name"
        :detail="detail"
				:company-logo="logo"
        :location="location"
        :avatar-position="position"
        :avatar-size="size"
        :no-avatar="noAvatar"
        :href="href"
        :to="to"
        :style="{'--cs-profile-background': color}"
      >
      </cs-profile>
    </div>
  `,
});

export const ProfileWithSocialMedia = () => ({
  components: { CsProfile, CsSocialIconButton },
  props: {
    name: {
      default: text('name', 'John Doe'),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/300'),
    },
    position: {
      default: select('avatar-position', positionOptions, 'left'),
    },
    size: {
      default: select('avatar-size', sizeOptions, 'medium'),
    },
    noAvatar: {
      default: boolean('no-avatar'),
    },
    detail: {
      default: text('detail', 'CEO at C-Suite Circle'),
    },
    logo: {
      default: text('company-logo', 'https://picsum.photos/300'),
    },
    location: {
      default: text('location', 'Singapore'),
    },
    color: {
      default: color('--cs-profile-background'),
    },
    href: {
      default: text('href', ''),
    },
    to: {
      default: text('to (Router link)', ''),
    },
  },
  template: `
    <div style="margin-top: 15px;">
      <cs-profile
        :picture="picture"
        :name="name"
        :detail="detail"
        :company-logo="logo"
        :location="location"
        :avatar-position="position"
        :avatar-size="size"
        :no-avatar="noAvatar"
        :href="href"
        :to="to"
        :style="{'--cs-profile-background': color}"
      >
        <cs-social-icon-button
          network="facebook"
          size="small"
          :style="{'--social-icon-button-size': '24px'}"
          slot="social-icons"
        ></cs-social-icon-button >

        <cs-social-icon-button
          network="instagram"
          size="small"
          :style="{'--social-icon-button-size': '24px'}"
          slot="social-icons"
        ></cs-social-icon-button >

        <cs-social-icon-button
          network="linkedin"
          size="small"
          :style="{'--social-icon-button-size': '24px'}"
          slot="social-icons"
        ></cs-social-icon-button >

        <cs-social-icon-button
          network="twitter"
          size="small"
          :style="{'--social-icon-button-size': '24px'}"
          slot="social-icons"
        ></cs-social-icon-button >
      </cs-profile>
    </div>
  `,
});
