import { withKnobs, text, number, date, boolean } from '@storybook/addon-knobs';
import CsConversationItem from '../components/ConversationItem.vue';

export default {
  title: 'Conversation Item',
  component: CsConversationItem,
  decorators: [withKnobs],
};

export const Default = () => ({
  components: { CsConversationItem },
  props: {
    subject: {
      default: text('Subject', 'John Doe'),
    },
    role: {
      default: text('Role', 'C-Suite Advisor'),
    },
    message: {
      default: text('Message', 'Hello, allow me to introduce myself'),
    },
    unreadCount: {
      default: text('Unread Count', '3'),
    },
    date: {
      default: date('Date/Time', new Date()),
    },
    picture: {
      default: text('Picture', 'https://picsum.photos/100'),
    },
    highlightAvatar: {
      default: boolean('Highlight Avatar', true),
    },
  },
  template: `
    <div>
      <cs-conversation-item
        :subject="subject"
        :role="role"
        :message="message"
        :unread-count="unreadCount"
        :date="date"
        :picture="picture"
        :highlight-avatar="highlightAvatar"
      >
      </cs-conversation-item>
      <cs-conversation-item
        :subject="subject"
        :role="role"
        :message="message"
        :unread-count="unreadCount"
        :date="date"
        :picture="picture"
      >
      </cs-conversation-item>
    </div>
  `,
});
