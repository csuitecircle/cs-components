import { withKnobs, text, boolean, color } from '@storybook/addon-knobs';
import CsRadio from '../components/Radio.vue';

export default {
  title: 'Form/Radio',
  component: CsRadio,
  decorators: [withKnobs],
};

export const Radio = () => ({
  components: { CsRadio },
  props: {
    label: {
      default: text('label', 'Email'),
    },
    value: {
      default: text('value', 'email'),
    },
    disabled: {
      default: boolean('disabled'),
    },
    checked: {
      default: boolean('checked'),
    },
    color: {
      default: color('--cs-radio-color'),
    },
  },
  template: `
    <cs-radio
      :label="label"
      :value="value"
      :disabled="disabled"
      :checked="checked"
      :style="{'--cs-radio-color': color}"
    />
  `,
});
