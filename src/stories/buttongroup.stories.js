import { action } from '@storybook/addon-actions';
import { withKnobs, object, select, boolean } from '@storybook/addon-knobs';

import CsButtonGroup from '../components/ButtonGroup.vue';

export default {
  component: CsButtonGroup,
  title: 'Button Group',
  decorators: [withKnobs],
};

const sizeOptions = ['small', 'medium', 'large'];
const fillOptions = ['solid', 'clear', 'outline'];

const buttons = [
  {
    label: 'Button 1',
    value: 'b1',
  },
  {
    label: 'Button 2',
    value: 'b2',
  },
  {
    label: 'Button 3',
    value: 'b3',
  },
];
const buttonValues = buttons.map((b) => b.value);

export const ButtonGroupSingleSelect = () => ({
  components: { CsButtonGroup },
  props: {
    values: {
      default: object('values', [
        {
          label: 'Button 2',
          value: 'b2',
        },
      ]),
    },
    size: {
      default: select('size', sizeOptions, 'medium'),
    },
    buttons: {
      default: object('buttons', buttons),
    },
    block: {
      default: boolean('block', true),
    },
    selectMultiple: {
      default: boolean('select-multiple', false),
    },
    fade: {
      default: boolean('fade', true),
    },
    wrap: {
      default: boolean('wrap', false),
    },
    fill: {
      default: select('fill', fillOptions, 'solid'),
    },
  },
  template: `
    <cs-button-group
      :size="size"
      :buttons="buttons"
      :values="values"
      :block="block"
      :select-multiple="selectMultiple"
      :fade="fade"
      :wrap="wrap"
      :fill="fill"
      @change="change"
      @input="input"
    >
    </cs-button-group>
    `,
  methods: {
    change: action('change'),
    input: action('input'),
  },
});

export const ButtonGroupMultiSelect = () => ({
  components: { CsButtonGroup },
  props: {
    values: {
      default: object('values', [
        {
          label: 'Button 1',
          value: 'b1',
        },
        {
          label: 'Button 2',
          value: 'b2',
        },
      ]),
    },
    size: {
      default: select('size', sizeOptions, 'medium'),
    },
    buttons: {
      default: object('buttons', buttons),
    },
    block: {
      default: boolean('block', false),
    },
    selectMultiple: {
      default: boolean('select-multiple', true),
    },
    fade: {
      default: boolean('fade', false),
    },
    wrap: {
      default: boolean('wrap', true),
    },
    fill: {
      default: select('fill', fillOptions, 'outline'),
    },
  },
  template: `
    <cs-button-group
      :size="size"
      :buttons="buttons"
      :values="values"
      :block="block"
      :select-multiple="selectMultiple"
      :fade="fade"
      :wrap="wrap"
      :fill="fill"
      @change="change"
      @input="input"
    >
    </cs-button-group>
    `,
  methods: {
    change: action('change'),
    input: action('input'),
  },
});
