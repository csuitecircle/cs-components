import { withKnobs, text, select, boolean, color } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsTag from '../components/SmallTag.vue';

export default {
  component: CsTag,
  title: 'SmallTag',
  decorators: [withKnobs],
};

export const SmallTag = () => ({
  components: { CsTag },
  data() {
    return {
      selected: false,
    };
  },
  props: {
    selectable: {
      default: boolean('selectable', false),
    },
    content: {
      default: text('default slot', 'Business'),
    },
  },
  template: `
      <cs-tag
        v-model="selected"
        :selectable="selectable"
        @select="onSelect"
      >
        {{content}}
      </cs-tag >
      `,
  methods: { onSelect: action('select') },
});

export const SmallTagSelectedProp = () => ({
  components: { CsTag },

  props: {
    selectable: {
      default: boolean('selectable', true),
    },
    content: {
      default: text('default slot', 'Business'),
    },
    selected: {
      default: boolean('selected', true),
    },
  },
  template: `
      <cs-tag
        :selected.sync="selected"
        :selectable.sync="selectable"
        @select="onSelect"
      >
        {{content}}
      </cs-tag >
      `,
  methods: { onSelect: action('select') },
});

export const SmallTagCustomColor = () => ({
  components: { CsTag },

  props: {
    selectable: {
      default: boolean('selectable', true),
    },
    content: {
      default: text('default slot', 'Business'),
    },
    tagBackground: {
      default: color('---cs-small-tag-background', '#DFD2FF'),
    },
    tagText: {
      default: color('---cs-small-tag-text-color', '#3B1897'),
    },
  },
  template: `
      <cs-tag
        :selectable.sync="selectable"
        @select="onSelect"
        :style="{
          '---cs-small-tag-background': tagBackground,
          '---cs-small-tag-text-color': tagText,
        }"
      >
        {{content}}
      </cs-tag >
      `,
  methods: { onSelect: action('select') },
});
