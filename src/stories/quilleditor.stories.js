import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs';

import CsQuillEditor from '../components/QuillEditor.vue';

export default {
  title: 'Quill Editor',
  component: CsQuillEditor,
  decorators: [withKnobs],
};

export const QuillEditor = () => ({
  components: { CsQuillEditor },
  data() {
    return {
      val: '',
    };
  },
  props: {
    applicationId: {
      default: text('application-id', '15600fe9-713d-4ba1-a1cd-4a2d2c916b5c'),
    },
  },
  template: `
    <cs-quill-editor
      :applicationId='applicationId'
      @input="onInput"
      @attachment-add="onAttachmentAdd"
      @audio-attach="onAudioAttach"
      @image-attach="onImageAttach"
      @file-attach="onFileAttach"
      @video-attach="onVideoAttach"
    ></cs-quill-editor>`,
  methods: {
    onInput: action('input'),
    onAttachmentAdd: action('attachment-add'),
    onAudioAttach: action('audio-attach'),
    onImageAttach: action('image-attach'),
    onFileAttach: action('file-attach'),
    onVideoAttach: action('video-attach'),
  },
});
