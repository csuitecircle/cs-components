import { action } from '@storybook/addon-actions';
import { withKnobs, text, select, boolean, date, object, color } from '@storybook/addon-knobs';
import CsComment from '../components/Comment.vue';
import { layoutOptions } from '../utils/validators';


export default {
  title: 'Comment',
  component: CsComment,
  decorators: [withKnobs],
};

const author = {
  name: 'John Doe',
  picture: 'https://picsum.photos/100',
};

const actionButtons = [
  {
    label: 'Like',
    icon: 'cs-icons-like-filled',
    enableToggle: true,
    highlight: false,
    action: () => {
      console.log('like');
    },
  },
  {
    label: 'Share',
    icon: 'cs-icons-share-filled',
    enableToggle: false,
    highlight: false,
    action: () => {
      console.log('Share');
    },
  },
];
const counters = [
  {
    label: '',
    icon: 'cs-icons-like-filled',
    count: 10,
    action: () => {
      console.log('like-count');
    },
  },
  {
    label: '',
    icon: 'cs-icons-share-filled',
    count: 11,
    action: () => {
      console.log('share-count');
    },
  },
];

export const Default = () => ({
  components: { CsComment },
  props: {
    author: {
      default: object('author', author),
    },
    authorName: {
      default: text('author-name', author.name),
    },
    authorPicture: {
      default: text('author-picture', author.picture),
    },
    date: {
      default: date('date', new Date()),
    },
    message: {
      default: text(
        'message',
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat'
      ),
    },
    showMenu: {
      default: boolean('show-menu'),
    },
    menuIcon: {
      default: text('menu-icon'),
    },
    nowLabel: {
      default: text('now-label'),
    },
    actionButtons: {
      default: object('action-buttons', actionButtons),
    },
    counters: {
      default: object('counters', counters),
    },
    backgroundColor: {
      default: color('---cs-comment-background-color', '#ffffff'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template: `
    <div style="margin-top:10px;">
      <cs-comment
        :author="author"
        :author-name="authorName"
        :author-picture="authorPicture"
        :message="message"
        :date="date"
        :menu-icon="menuIcon"
        :show-menu="showMenu"
        :now-label="nowLabel"
        :action-buttons="actionButtons"
        :counters="counters"
        :layout="layout"
        @author="onAuthor"
        @action-button="onActionButtonClick"
        @counter-click="onCounterClick"
        @click="onClick"
        @open-menu="onOpen"
        :style="{
          '---cs-comment-background-color': backgroundColor
        }"
      />
    </div>
  `,
  methods: {
    onAuthor: action('author'),
    onActionButtonClick: action('action-button'),
    onCounterClick: action('counter-click'),
    onClick: action('click'),
    onOpen: action('open-menu'),
  },
});

export const SlotContent = () => ({
  components: { CsComment },
  props: {
    author: {
      default: object('author', author),
    },
    date: {
      default: date('date', new Date()),
    },
    message: {
      default: text('message', 'Image in the slot of the comment'),
    },
    enableLikes: {
      default: boolean('enable-likes', true),
    },
    enableReplies: {
      default: boolean('enable-replies', true),
    },
    showMenu: {
      default: boolean('show-menu', true),
    },
    actionButtons: {
      default: object('action-buttons', actionButtons),
    },
    counters: {
      default: object('counters', counters),
    },
    backgroundColor: {
      default: color('---cs-comment-background-color', '#ffffff'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template: `
    <div style="margin-top:10px;">
      <cs-comment
        :author="author"
        :date="date"
        :message="message"
        :enable-likes="enableLikes"
        :enable-replies="enableReplies"
        :show-menu="showMenu"
        :action-buttons="actionButtons"
        :counters="counters"
        :layout="layout"
        @author="onAuthor"
        @action-button="onActionButtonClick"
        @counter-click="onCounterClick"
        @click="onClick"
        @open-menu="onOpen"
        :style="{
          '---cs-comment-background-color': backgroundColor
        }"
      >
        <img src="https://picsum.photos/200"/>
        <div>Custom icons in icon slots</div>
        <i class="cs-icons-menu" slot="menu-icon"/>
      </cs-comment>
    </div>
  `,
  methods: {
    onAuthor: action('author'),
    onActionButtonClick: action('action-button'),
    onCounterClick: action('counter-click'),
    onClick: action('click'),
    onOpen: action('open-menu'),
  },
});

export const CommentWithReactions = () => ({
  components: { CsComment },
  props: {
    author: {
      default: object('author', author),
    },
    authorName: {
      default: text('author-name', author.name),
    },
    authorPicture: {
      default: text('author-picture', author.picture),
    },
    date: {
      default: date('date', new Date()),
    },
    message: {
      default: text(
        'message',
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat'
      ),
    },
    showMenu: {
      default: boolean('show-menu'),
    },
    menuIcon: {
      default: text('menu-icon'),
    },
    nowLabel: {
      default: text('now-label'),
    },
    actionButtons: {
      default: object('action-buttons', actionButtons),
    },
    counters: {
      default: object('counters', counters),
    },
    backgroundColor: {
      default: color('---cs-comment-background-color', '#ffffff'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  data() {
    return {
      counters: counters
    };
  },
  mounted() {
    this.counters[0].icon = ''
  },
  template: `
    <div style="margin-top:10px;">
      <cs-comment
        :author="author"
        :author-name="authorName"
        :author-picture="authorPicture"
        :message="message"
        :date="date"
        :menu-icon="menuIcon"
        :show-menu="showMenu"
        :now-label="nowLabel"
        :action-buttons="actionButtons"
        :counters="counters"
        :layout="layout"
        @author="onAuthor"
        @action-button="onActionButtonClick"
        @counter-click="onCounterClick"
        @click="onClick"
        @open-menu="onOpen"
        :style="{
          '---cs-comment-background-color': backgroundColor
        }"s
      >
      <i slot="reactions" class="cs-icons-twitter-filled" style="color: red; margin-left: -6px;"></i>
      <i slot="reactions" class="cs-icons-edit-filled" style="color: green; margin-left: -6px;"></i>
      <i slot="reactions" class="cs-icons-facebook-filled" style="color: blue; margin-left: -6px;"></i>
      </cs-comment>
    </div>
  `,
  methods: {
    onAuthor: action('author'),
    onActionButtonClick: action('action-button'),
    onCounterClick: action('counter-click'),
    onClick: action('click'),
    onOpen: action('open-menu'),
  },
});