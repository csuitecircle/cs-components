import {
  withKnobs,
  text,
  number,
  object,
  boolean,
  color,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsRadioGroup from '../components/RadioGroup.vue';
import CsRadio from '../components/Radio.vue';

export default {
  title: 'Form/Radio Group',
  component: CsRadioGroup,
  decorators: [withKnobs],
};

const options = [
  {
    label: 'Tweet',
    value: 'tweet',
  },
  {
    label: 'Maps',
    value: 'maps',
    disabled: true,
  },
];

export const OptionsString = () => ({
  components: { CsRadioGroup },
  props: {
    label: {
      default: text('label', 'Radio Group'),
    },
    options: {
      default: object(
        'options',
        options.map((o) => o.label)
      ),
    },
    value: {
      default: text('value'),
    },
    required: {
      default: boolean('required'),
    },
    disabled: {
      default: boolean('disabled'),
    },
    color: {
      default: color('--cs-radio-group-color'),
    },
    requiredLabel: {
      default: text('requiredLabel', '* Required'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
  },
  template: `
    <cs-radio-group
      :label="label"
      :options="options"
      :value="value"
      :required="required"
      :disabled="disabled"
      :error-icon="errorIcon"
      :required-label="requiredLabel"
      :style="{'--cs-radio-group-color': color}"
      @input="onInput"
      @validate="onValidate"
    />
  `,
  methods: {
    onInput: action('input'),
    onValidate: action('validate'),
  },
});

export const OptionsObject = () => ({
  components: { CsRadioGroup },
  props: {
    label: {
      default: text('label', 'RadioGroup Label'),
    },
    options: {
      default: object('options', options),
    },
    value: {
      default: text('value'),
    },
    required: {
      default: boolean('required', true),
    },
    disabled: {
      default: boolean('disabled'),
    },
    color: {
      default: color('--cs-radio-group-color'),
    },
    requiredLabel: {
      default: text('requiredLabel', '* Required'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
  },
  template: `
    <cs-radio-group
      :label="label"
      :options="options"
      :value="value"
      :required="required"
      :disabled="disabled"
      :error-icon="errorIcon"
      :required-label="requiredLabel"
      :style="{'--cs-radio-group-color': color}"
      @input="input"
      @validate="onValidate"
    />
  `,
  methods: {
    input: action('input'),
    onValidate: action('validate'),
  },
});

export const Slots = () => ({
  components: {
    CsRadioGroup,
    CsRadio,
  },
  props: {
    label: {
      default: text('label'),
    },
    options: {
      default: object('options', options),
    },
    value: {
      default: text('value', 'sms'),
    },
    required: {
      default: boolean('required'),
    },
    disabled: {
      default: boolean('disabled'),
    },
    color: {
      default: color('--cs-radio-group-color'),
    },
    requiredLabel: {
      default: text('requiredLabel', '* Required'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
  },
  template: `
    <cs-radio-group
      :label="label"
      :options="options"
      :value="value"
      :required="required"
      :disabled="disabled"
      :error-icon="errorIcon"
      :required-label="requiredLabel"
      :style="{'--cs-radio-group-color': color}"
      @input="input"
      @validate="onValidate"
    >
      <cs-radio disabled value="call">Call <i class="cs-icons-phone"/></cs-radio>
      <cs-radio value="sms"><i class="cs-icons-chat"/>SMS<div>(carrier rates apply)</div></cs-radio>
      <cs-radio label="Email" value="email"></cs-radio>
    </cs-radio-group>`,
  methods: {
    input: action('input'),
    onValidate: action('validate'),
  },
});

export const RadioGroupValidation = () => ({
  components: { CsRadioGroup },
  props: {
    label: {
      default: text('label', 'Radio Group with Validation'),
    },
    options: {
      default: object(
        'options',
        options.map((o) => o.label)
      ),
    },
    value: {
      default: text('value'),
    },
    required: {
      default: boolean('required'),
    },
    disabled: {
      default: boolean('disabled'),
    },
    invalid: {
      default: boolean('invalid', false),
    },
    color: {
      default: color('--cs-radio-group-color'),
    },
    requiredLabel: {
      default: text('requiredLabel', '* Required'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
  },
  template: `
    <cs-radio-group
      :label="label"
      :options="options"
      :value="value"
      :required="required"
      :disabled="disabled"
      :invalid="invalid"
      :error-icon="errorIcon"
      :required-label="requiredLabel"
      :style="{'--cs-radio-group-color': color}"
      @input="onInput"
      @validate="onValidate"
    >
      <cs-input-note slot="input-notes" v-if="true" type="info" label="This is a default message"/>
      <cs-input-note slot="input-notes" v-if="true" type="success" label="This is a success message"/>
      <cs-input-note slot="input-notes" v-if="true" type="error" label="This is an error message"/>
    </cs-radio-group>
  `,
  methods: {
    onInput: action('input'),
    onValidate: action('validate'),
  },
});
