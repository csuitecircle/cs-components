import {
  withKnobs,
  text,
  number,
  object,
  boolean,
  color,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsCheckboxGroup from '../components/CheckboxGroup.vue';
import CsCheckbox from '../components/Checkbox.vue';

export default {
  title: 'Form/Checkbox Group',
  component: CsCheckboxGroup,
  decorators: [withKnobs],
};

export const Slots = () => ({
  components: {
    CsCheckboxGroup,
    CsCheckbox,
  },
  props: {
    label: {
      default: text('label', 'Checkbox Group'),
    },
  },
  template: `
    <cs-checkbox-group>
      <cs-checkbox> Call <i class="cs-icons-phone"/></cs-checkbox>
      <cs-checkbox :value="true"> <i class="cs-icons-chat"/>SMS<div>(carrier rates apply)</div></cs-checkbox>
      <cs-checkbox label="Email"></cs-checkbox>
    </cs-checkbox-group>`,
  methods: {
    input: action('input'),
  },
});
