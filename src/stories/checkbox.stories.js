import { withKnobs, text, boolean, color } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsCheckbox from '../components/Checkbox.vue';

import { checkboxToggleEvents } from '@/utils/formevents';
import formEventsMixin from './mixins/formevents';

export default {
  title: 'Form/Checkbox',
  component: CsCheckbox,
  decorators: [withKnobs],
};

export const Checkbox = () => ({
  mixins: [formEventsMixin],
  components: { CsCheckbox },
  data() {
    return {
      modelValue: this.value,
      $_formEventTriggers: checkboxToggleEvents,
    };
  },
  props: {
    label: {
      default: text('label', 'Okay'),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    required: {
      default: boolean('required', false),
    },
    color: {
      default: color('---cs-checkbox-color'),
    },
    value: {
      default: boolean('value', false),
    },
    requiredLabel: {
      default: text('requiredLabel', '* Required'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
  },
  template: `
		<div>
	    <cs-checkbox
	      v-model="modelValue"
				:label="label"
	      :disabled="disabled"
				:required="required"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
				:style="{
					'---cs-checkbox-color': color,
				}"
	      @input="action"
        @validate="onValidate"
        v-on="$_formEvents"
	    />
      <cs-checkbox
	      v-model="modelValue"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
				:style="{
					'---cs-checkbox-color': color,
				}"
	    >With slot content
			</cs-checkbox>
      <cs-checkbox
        v-model="modelValue"
				:label="label"
	      :disabled="disabled"
				:required="required"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        :success="true"
				:style="{
					'---cs-checkbox-color': color,
				}"
	    >
        <cs-input-note slot="input-notes" type="success" label="This is a success message"/>
      </cs-checkbox>
      <cs-checkbox
        v-model="modelValue"
				:label="label"
	      :disabled="disabled"
				:required="required"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        :invalid="true"
				:style="{
					'---cs-checkbox-color': color,
				}"
	    >
        <cs-input-note slot="input-notes" v-if="modelValue !== null" type="info" label="This is a default message"/>
        <cs-input-note slot="input-notes" v-if="modelValue !== null" type="error" label="This is an error message"/>
      </cs-checkbox>
		</div>
  `,
  watch: {
    value() {
      this.modelValue = this.value;
    },
  },
  methods: {
    action: action('input'),
    onValidate: action('validate'),
  },
});
