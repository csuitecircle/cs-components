import { action } from '@storybook/addon-actions';
import { withKnobs, text, select, boolean } from '@storybook/addon-knobs';
import CsFileInput from '../components/FileInput.vue';

export default {
  component: CsFileInput,
  title: 'Form/File Input',
  decorators: [withKnobs],
};
const acceptTypes = [
  '*/*',
  'image/*',
  'audio/*',
  'video/*',
  'application/*',
  '.pdf',
  '.jpg',
  'application/pdf',
];

export const FileInput = () => ({
  components: { CsFileInput },
  props: {
    applicationId: {
      default: text('application-id', ''),
    },
    appIdEnvVarName: {
      default: text('app-id-env-var-name', 'VUE_APP_GRAPHEXPRESS_ID'),
    },
    acceptType: {
      default: select('accept-types', acceptTypes, '*/*'),
    },
    value: {
      default: text('v-model', ''),
    },
    label: {
      default: text('label', 'Upload'),
    },
    required: {
      default: boolean('required'),
    },
    uploadIcon: {
      default: text('upload-icon'),
    },
    forwardIcon: {
      default: text('forward-icon'),
    },
    deleteIcon: {
      default: text('delete-icon'),
    },
    editIcon: {
      default: text('edit-icon'),
    },
    uploadNoteLabel: {
      default: text('upload-note-label'),
    },
    uploadLabel: {
      default: text('upload-label'),
    },
    requiredLabel: {
      default: text('required-label'),
    },
    changeFileLabel: {
      default: text('change-file-label'),
    },
    hideChangeBtn: {
      default: boolean('hide-change-btn'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
    manualUpload: {
      default: boolean('manual-upload'),
    },
  },
  template: `
	  <div>
	    <cs-file-input
		    :applicationId='applicationId'
        :app-id-env-var-name='appIdEnvVarName'
		    :acceptType='acceptType'
		    :upload-icon='uploadIcon'
		    :forward-icon='forwardIcon'
		    :delete-icon='deleteIcon'
		    :edit-icon='editIcon'
		    :value="value"
        :upload-note-label="uploadNoteLabel"
        :upload-label="uploadLabel"
        :required-label="requiredLabel"
        :required="required"
				:label="label"
        :change-file-label="changeFileLabel"
        :hide-change-btn="hideChangeBtn"
        :error-icon="errorIcon"
        :manual-upload="manualUpload"
		    @input="input"
        @selected="selected"
        @validate="onValidate"
	    />
      <cs-file-input
		    :applicationId='applicationId'
        :app-id-env-var-name='appIdEnvVarName'
		    :acceptType='acceptType'
		    :upload-icon='uploadIcon'
		    :forward-icon='forwardIcon'
		    :delete-icon='deleteIcon'
		    :edit-icon='editIcon'
		    :value="value"
        :upload-note-label="uploadNoteLabel"
        :upload-label="uploadLabel"
        :required-label="requiredLabel"
        :required="required"
				:label="label"
        :change-file-label="changeFileLabel"
        :hide-change-btn="hideChangeBtn"
        :error-icon="errorIcon"
        :success="true"
        :manual-upload="manualUpload"
		    @input="input"
        @selected="selected"
        @validate="onValidate"
	    >
        <cs-input-note slot="input-notes" v-if="true" type="success" label="This is a success message"/>
      </cs-file-input>
      <cs-file-input
		    :applicationId='applicationId'
        :app-id-env-var-name='appIdEnvVarName'
		    :acceptType='acceptType'
		    :upload-icon='uploadIcon'
		    :forward-icon='forwardIcon'
		    :delete-icon='deleteIcon'
		    :edit-icon='editIcon'
		    :value="value"
        :upload-note-label="uploadNoteLabel"
        :upload-label="uploadLabel"
        :required-label="requiredLabel"
        :required="required"
				:label="label"
        :change-file-label="changeFileLabel"
        :hide-change-btn="hideChangeBtn"
        :error-icon="errorIcon"
        :invalid="true"
        :manual-upload="manualUpload"
		    @input="input"
        @selected="selected"
        @validate="onValidate"
	    >
        <cs-input-note slot="input-notes" v-if="value !== null" type="info" label="This is a default message"/>
        <cs-input-note slot="input-notes" v-if="value !== null" type="error" label="This is an error message"/>
      </cs-file-input>
	  </div>
  `,
  methods: {
    input: action('input'),
    selected: action('selected'),
    onValidate: action('validate'),
  },
});
