import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs';
import CsModal from '../components/Modal.vue';
import CsButton from '../components/Button.vue';
import CsTag from '../components/SmallTag.vue';
import CsInput from '../components/Input.vue';
import CsCheckbox from '../components/Checkbox.vue';
import { layoutOptions } from '../utils/validators';


export default {
  component: CsModal,
  title: 'Modal',
  decorators: [withKnobs],
};

export const Desktop = () => ({
  components: {
    CsModal,
    CsButton,
    CsTag,
    CsInput,
    CsCheckbox,
  },
  props: {
    backdrop: {
      default: boolean('Backdrop', true),
    },
    width: {
      default: text('---cs-modal-width'),
    },
    maxHeight: {
      default: text('---cs-modal-max-height'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  data() {
    return {
      show: false,
    };
  },
  template: `
    <div>
      <cs-button variant="primary" size="small"  @click.native="showModal">Show Modal</cs-button>
      <cs-modal
        @close='onClose'
        :backdrop="backdrop"
        :show="show"
        :layout="layout"
        :style="{
					'---cs-modal-width': width,
					'---cs-modal-max-height': maxHeight,
        }"
      >
        <div slot="header">
          Hello world!
        </div>

        <div slot="body" style="width:100%;">

          <h3 class="cs-textstyle-item-heading"> Let us know who you are!</h3>
          <cs-input
            :label="'Name'"
            :type="'text'"
          />
          <cs-input
            :label="'Email'"
            :type="'email'"
          />
        </div>

        <div slot="extra">
          <i class="cs-icons-twitter"></i>
          <i class="cs-icons-like"></i>
        </div>
        
        <cs-button slot="main" variant="primary" @click.native="onClose" fill="solid"  >Share</cs-button>
        <cs-button slot="fallback" variant="secondary" @click.native="onClose" fill="outline" >Claim</cs-button>
        <cs-button slot="optional" variant="default" @click.native="onClose" fill="outline" >Close</cs-button>
      </cs-modal>
    </div>
    `,
  methods: {
    showModal() {
      this.show = true;
    },
    onClose() {
      this.show = false;
    },
  },
});
Desktop.parameters = {
  viewport: {
    defaultViewport: 'desktop',
  },
};

export const Mobile = () => ({
  components: {
    CsModal,
    CsButton,
    CsTag,
    CsInput,
    CsCheckbox,
  },
  data() {
    return {
      show: false,
    };
  },
  template: `
    <div>
      <cs-button variant="primary" size="small"  @click.native="showModal">Show Modal</cs-button>
      <cs-modal
        @close='onClose'
        :show="show"
      >
        <div slot="header">
          Greetings from us!
        </div>
        <div slot="body" style="width:100%">

          <br/>

          <img src="https://picsum.photos/100" width="100%"/>

          <cs-input
            :label="'Name'"
            :type="'text'"

          />
          <cs-input
            :label="'Email'"
            :type="'email'"
          />
          <cs-input
            :label="'Name'"
            :type="'text'"

          />
          <cs-input
            :label="'Email'"
            :type="'email'"
          />
          <cs-input
            :label="'Name'"
            :type="'text'"

          />
          <cs-input
            :label="'Email'"
            :type="'email'"
          />
        </div>

        <cs-button slot="main" variant="primary" @click.native="onClose" fill="solid" >Share</cs-button>
        <cs-button slot="fallback" variant="secondary" @click.native="onClose" fill="outline" >Claim</cs-button>
        <cs-button slot="optional" variant="default" @click.native="onClose" fill="outline" >Close</cs-button>
      </cs-modal>

    </div>
    `,
  methods: {
    showModal() {
      this.show = true;
    },
    onClose() {
      this.show = false;
    },
  },
});
Mobile.parameters = {
  viewport: {
    defaultViewport: 'mobile1',
  },
};
