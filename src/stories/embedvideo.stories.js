import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';
import CsEmbedVideo from '../components/EmbedVideo.vue';

export default {
  component: CsEmbedVideo,
  title: 'Embed Video',
  decorators: [withKnobs],
};

export const EmbedVideo = () => ({
  components: { CsEmbedVideo },
  props: {
    src: {
      default: text('src', 'https://www.youtube.com/watch?v=ldQUJJKd7j8'),
    },
    playIcon: {
      default: text('play-icon'),
    },
    title: {
      default: text('title'),
    },
    picture: {
      default: text('picture'),
    },
  },
  template: `
    <cs-embed-video :src="src" :play-icon="playIcon" :title="title" :picture="picture" ></cs-embed-video>
  `,
  methods: {
    onEvent($event) {
      action($event.type)($event);
    },
    onError: action('error'),
  },
});
