import { action } from '@storybook/addon-actions';
import { withKnobs, color, boolean, text } from '@storybook/addon-knobs';
import CsActionBarButton from '../components/ActionBarButton.vue';

export default {
	component: CsActionBarButton,
	title: 'Action Bar Button',
	decorators: [withKnobs],
};

export const ActionBarButton = () => ({
	components: { CsActionBarButton },
	props: {
		highlighted: {
			default: boolean('highlighted', false),
		},
		icon: {
			default: text('icon', 'cs-icons-audio-filled'),
		},
		color: {
			default: color('--cs-action-button-highlight-color', '#00C472'),
		},
	},
	template: `
    <div style="display:flex; flex-direction:column;align-items:center;gap:8px;">
      <label>Icon and label</label>
      <cs-action-bar-button :highlighted="highlighted" :style="{'--cs-action-button-highlight-color': color}">
        <i class="cs-icons-like-filled" slot="icon"></i>
        Like
      </cs-action-bar-button>
      <cs-action-bar-button :highlighted="highlighted" :style="{'--cs-action-button-highlight-color': color}">
        <i class="cs-icons-marker-filled" slot="icon"></i>
        Location
      </cs-action-bar-button>
      <label>Only Icon </label>
      <cs-action-bar-button :highlighted="highlighted" :style="{'--cs-action-button-highlight-color': color}">
        <i class="cs-icons-share-filled" slot="icon"></i>
      </cs-action-bar-button>
      <label>Only Label </label>
      <cs-action-bar-button :highlighted="highlighted" :style="{'--cs-action-button-highlight-color': color}">
        Say Hi
      </cs-action-bar-button>
      <label>Using Icon Prop</label>
      <cs-action-bar-button :icon="icon" :highlighted="highlighted" :style="{'--cs-action-button-highlight-color': color}">
        Play
      </cs-action-bar-button>
    </div>
  `,
});
