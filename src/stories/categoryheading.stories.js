import { action } from '@storybook/addon-actions';
import { withKnobs, text, color } from '@storybook/addon-knobs';
import CsCategoryHeading from '../components/CategoryHeading.vue';
import CsButton from '../components/Button.vue';

export default {
  component: CsCategoryHeading,
  title: 'Headers/Category Heading',
  decorators: [withKnobs],
};

export const CategoryHeading = () => ({
  components: { CsCategoryHeading },
  props: {
    title: {
      default: text('title', 'Events'),
    },
    icon: {
      default: text('icon', 'cs-icons-twitter-filled'),
    },
    button: {
      default: text('button', 'More'),
    },
    backgroundColor: {
      default: color('--cs-category-heading-background-color'),
    },
    borderColor: {
      default: color('--cs-category-heading-border-color'),
    },
  },
  template: `
    <cs-category-heading
      :title="title"
			:icon="icon"
			:button="button"
			@action="onButton"
			@title-click="onTitleClick"
			@icon-click="onIconClick"
      :style="{
        '--cs-category-heading-background-color': backgroundColor,
        '--cs-category-heading-border-color': borderColor
      }"
    />
  `,
  methods: { onButton: action('action'), onTitleClick: action('title-click'), onIconClick: action('icon-click') },
});

export const CategoryHeadingSlots = () => ({
  components: { CsCategoryHeading, CsButton },
  props: {
    backgroundColor: {
      default: color('--cs-category-heading-background-color'),
    },
    borderColor: {
      default: color('--cs-category-heading-border-color'),
    },
  },
  template: `
    <cs-category-heading
      :style="{
        '--cs-category-heading-background-color': backgroundColor,
        '--cs-category-heading-border-color': borderColor
      }"
    >
			<template slot="title">Hello World</template>
			<i slot="icon" class="cs-icons-marker-filled" />
			<cs-button slot="button">More</cs-button>
    </cs-category-heading>
  `,
});

export const CategoryHeadingDefaultSlot = () => ({
  components: { CsCategoryHeading, CsButton },
  props: {
    backgroundColor: {
      default: color('--cs-category-heading-background-color'),
    },
    borderColor: {
      default: color('--cs-category-heading-border-color'),
    },
  },
  template: `
    <cs-category-heading
      :style="{
        '--cs-category-heading-background-color': backgroundColor,
        '--cs-category-heading-border-color': borderColor
      }"
    >
			Default Slot Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
			<i slot="icon" class="cs-icons-marker-filled" />
			<cs-button slot="button">More</cs-button>
    </cs-category-heading>
  `,
});
