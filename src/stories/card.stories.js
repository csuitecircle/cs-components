import { action } from '@storybook/addon-actions';
import { withKnobs, text, select, boolean } from '@storybook/addon-knobs';
import CsCard from '../components/Card.vue';
import CsButton from '../components/Button.vue';
import CsVideoPlayer from '../components/VideoPlayer.vue';
import { layoutOptions } from '../utils/validators';

const positionOptions = {
  top: 'top',
  left: 'left',
};

export default {
  component: CsCard,
  title: 'Cards/Card',
  decorators: [withKnobs],
};

export const BasicCard = () => ({
  components: { CsCard, CsButton },
  props: {
    nodivider: {
      default: boolean('no-divider', false),
    },
    border: {
      default: text('--cs-card-border-radius', '5px'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template: `
    <div style="margin-top:10px;">
      <cs-card 
        :no-divider="nodivider"
        :layout="layout"
        :style="{
          '--cs-card-border-radius': border,
        }"
      >
        <div slot="header">
          <div class="cs-card__header--title">The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates</div>
        </div>
        <div slot="body">
          <div>There are numerous goals which can be achieved with custom illustrations. Title images for blog articles are one of them getting more</div>
        </div>
        <div slot="footer">
          <cs-button  :variant="'primary'" :fill="'solid'" :size="'small'">Useful</cs-button>
          <cs-button  :variant="'secondary'" :fill="'outline'" :size="'small'">Not useful</cs-button>
        </div>
      </cs-card>
    </div>
    `,
});
BasicCard.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const CardWithImage = () => ({
  components: { CsCard },
  props: {
    position: {
      default: select('media-position', positionOptions, 'top'),
    },
    padding: {
      default: boolean('media-padding'),
    },
    nodivider: {
      default: boolean('no-divider', false),
    },
    border: {
      default: text('--cs-card-border-radius', '5px'),
    },
    width: {
      default: text(
        '--cs-card-media-width (only for media-position=left)',
        '150px'
      ),
    },
    height: {
      default: text(
        '--cs-card-media-height (only for media-position=top)',
        '150px'
      ),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template: `
    <div style="margin-top:10px;">
      <cs-card
        :media-padding="padding"
        :media-position="position"
        :no-divider="nodivider"
        :layout="layout"
        :style="{
          '--cs-card-border-radius': border,
          '--cs-card-media-width': width,
          '--cs-card-media-height': height,
        }"
      >
      <img src="https://picsum.photos/1000" alt="img"  slot="media" />
      <div slot="header">
        <div class="cs-card__header--title">The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates</div>
      </div>
      <div slot="body">
        <div>There are numerous goals which can be achieved with custom illustrations. Title images for blog.</div>
      </div>
      <div slot="footer">
        <div style="display:flex; justify-content: space-between">
          <div>
            12 Dec, 2020
          </div>
          <div style="margin-left:10px;">
            3 min read
          </div>
        </div>
      </div>
      </cs-card>
    </div>
    `,
});
CardWithImage.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const CardWithVideo = () => ({
  components: { CsCard, CsVideoPlayer },
  props: {
    position: {
      default: select('media-position', positionOptions, 'top'),
    },
    padding: {
      default: boolean('media-padding'),
    },
    nodivider: {
      default: boolean('no-divider', false),
    },
    border: {
      default: text('--cs-card-border-radius', '5px'),
    },
    width: {
      default: text(
        '--cs-card-media-width (only for media-position=left)',
        '150px'
      ),
    },
    height: {
      default: text(
        '--cs-card-media-height (only for media-position=top)',
        '150px'
      ),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template: `
    <div style="margin-top:10px;">
      <cs-card
        :media-padding="padding"
        :media-position="position"
        :no-divider="nodivider"
        :layout="layout"
        :style="{
          '--cs-card-border-radius': border,
          '--cs-card-media-width': width,
          '--cs-card-media-height': height,
        }"
      >
      <cs-video-player picture="https://picsum.photos/500" src="https://cdn.graphexpress.com/15600fe9-713d-4ba1-a1cd-4a2d2c916b5c/8d9c1b55-7be8-4259-9ce8-b40ee95c3dc8" slot="media"></cs-video-player>
      <div slot="header">
        <div class="cs-card__header--title">The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates</div>
      </div>
      <div slot="body">
        <div>There are numerous goals which can be achieved with custom illustrations. Title images for blog.</div>
      </div>
      <div slot="footer">
        <div style="display:flex; justify-content: space-between">
          <div>
            12 Dec, 2020
          </div>
          <div style="margin-left:10px;">
            3 min read
          </div>
        </div>
      </div>
      </cs-card>
    </div>
    `,
});
CardWithVideo.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const CardWithIcon = () => ({
  components: { CsCard },
  props: {
    position: {
      default: select('media-position', positionOptions, 'left'),
    },
    padding: {
      default: boolean('media-padding'),
    },
    nodivider: {
      default: boolean('no-divider', false),
    },
    border: {
      default: text('--cs-card-border-radius', '5px'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template: `
    <div style="margin-top:10px;">
      <cs-card
        :media-padding="padding"
        :media-position="position"
        :no-divider="nodivider"
        :layout="layout"
        :style="{
          '--cs-card-border-radius': border,
        }"
      >
      <div slot="header">
        <div class="cs-card__header--title">What Happens to Stock Markets When the Fed Cuts Interest Rates</div>
        <i class="cs-icons-audio-filled"  slot="card-icon" />
      </div>
      <div slot="body">
        <div>There are numerous goals which can be achieved with custom illustrations. Title images for blog.</div>
      </div>
      <div slot="footer">
        <div style="display:flex; justify-content: space-between">
          <div>
            12 Dec, 2020
          </div>
          <div style="margin-left:10px;">
            3 min read
          </div>
        </div>
      </div>
      </cs-card>
    </div>
    `,
});
CardWithIcon.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const CardList = () => ({
  components: { CsCard },
  props: {
    position: {
      default: select('media-position', positionOptions, 'top'),
    },
    padding: {
      default: boolean('media-padding'),
    },
    nodivider: {
      default: boolean('no-divider', false),
    },
    border: {
      default: text('--cs-card-border-radius', '5px'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template: `
    <div style="margin-top:10px;">
      <cs-card
        :media-padding="padding"
        :media-position="position"
        :no-divider="nodivider"
        :layout="layout"
        style=""
      >
        <img src="https://picsum.photos/1000" alt="img"  slot="media" />
        <div slot="header">
          <div class="cs-card__header--title">The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates</div>
        </div>
        <div slot="body">
          <div>There are numerous goals which can be achieved with custom illustrations. Title images for blog.</div>
        </div>
      </cs-card>
      <cs-card
        :media-padding="padding"
        :media-position="position"
        :no-divider="nodivider"
        :layout="layout"
        style="max-width: 300px; display: inline-block"
      >
        <img src="https://picsum.photos/1000" alt="img"  slot="media" />
        <div slot="header">
          <div class="cs-card__header--title">The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates</div>
        </div>
        <div slot="body">
          <div>There are numerous goals which can be achieved with custom illustrations. Title images for blog.</div>
        </div>
      </cs-card>
      <cs-card
        :media-padding="padding"
        :media-position="position"
        :no-divider="nodivider"
        :layout="layout"
        style="max-width: 300px; display: inline-block"
      >
        <img src="https://picsum.photos/1000" alt="img"  slot="media" />
        <div slot="header">
          <div class="cs-card__header--title">The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates</div>
        </div>
        <div slot="body">
          <div>There are numerous goals which can be achieved with custom illustrations. Title images for blog.</div>
        </div>
      </cs-card>
      <cs-card
        :media-padding="padding"
        :media-position="position"
        :no-divider="nodivider"
        :layout="layout"
        style="max-width: 300px; display: inline-block"
      >
        <img src="https://picsum.photos/1000" alt="img"  slot="media" />
        <div slot="header">
          <div class="cs-card__header--title">The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates</div>
        </div>
        <div slot="body">
          <div>There are numerous goals which can be achieved with custom illustrations. Title images for blog.</div>
        </div>
      </cs-card>
      <cs-card
        :media-padding="padding"
        :media-position="position"
        :no-divider="nodivider"
        :layout="layout"
        style="max-width: 300px; display: inline-block"
      >
        <img src="https://picsum.photos/1000" alt="img"  slot="media" />
        <div slot="header">
          <div class="cs-card__header--title">The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates</div>
        </div>
        <div slot="body">
          <div>There are numerous goals which can be achieved with custom illustrations. Title images for blog.</div>
        </div>
      </cs-card>
    </div>
    `,
});
CardList.parameters = {
  viewport: {
    defaultViewport: 'desktop',
  },
};