import { withKnobs, color } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsTag from '../components/Tag.vue';

export default {
  title: 'Tag',
  component: CsTag,
  decorators: [withKnobs],
};

export const Tag = () => ({
  components: { CsTag },
  props: {
    color: {
      default: color('--cs-tag-color', ''),
    },
    textColor: {
      default: color('--cs-tag-text-color', ''),
    },
  },
  template: `
    <div>
      <cs-tag @click="onClick" :style="{'--cs-tag-color': color, '--cs-tag-text-color': textColor}">Business</cs-tag>
      <cs-tag @click="onClick" :style="{'--cs-tag-color': color, '--cs-tag-text-color': textColor}"><i class="cs-icons-twitter-filled"/></cs-tag>
      <cs-tag @click="onClick" :style="{'--cs-tag-color': color, '--cs-tag-text-color': textColor}"><i class="cs-icons-twitter-filled"/><span>Like Me</span></cs-tag>
    </div>`,
  methods: { onClick: action('click') },
});

export const TagGroup = () => ({
  components: { CsTag },
  data() {
    return {
      tags: ['Jobs', 'Growth', 'Business'],
    };
  },
  template: `
      <div>
        <cs-tag @click="onClick" v-for="tag in tags" :key="tag" style="margin-right:5px;">{{tag}}</cs-tag>
      </div>`,
  methods: { onClick: action('click') },
});
