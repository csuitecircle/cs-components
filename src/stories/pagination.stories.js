import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, select, number } from '@storybook/addon-knobs';
import CsPagination from '../components/Pagination.vue';
import { layoutOptions } from '../utils/validators';

export default {
  title: 'Pagination',
  component: CsPagination,
  decorators: [withKnobs],
};

export const Mobile = () => ({
  components: { CsPagination },
  props: {
    count: {
      default: number('count', 20),
    },
    value: {
      default: number('value', 1),
    },
    zeroIndex: {
      default: boolean('zero-index'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template: `
    <cs-pagination
      :count="count"
      :value="value"
      :zero-index="zeroIndex"
      :layout="layout"
      @input="onInput"
    />
  `,
  methods: { onInput: action('input') },
});
Mobile.parameters = {
  viewport: {
    defaultViewport: 'mobile1',
  },
};

export const Tablet = () => ({
  components: { CsPagination },
  props: {
    count: {
      default: number('count', 20),
    },
    value: {
      default: number('value', 1),
    },
    zeroIndex: {
      default: boolean('zero-index'),
    },
    prevLabel: {
      default: text('previous-label'),
    },
    nextLabel: {
      default: text('next-label'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template: `
    <cs-pagination
      :count="count"
      :value="value"
      :zero-index="zeroIndex"
      :previous-label="prevLabel"
      :next-label="nextLabel"
      :layout="layout"
      @input="onInput"
    />
  `,
  methods: { onInput: action('input') },
});
Tablet.parameters = {
  viewport: {
    defaultViewport: 'tablet',
  },
};
