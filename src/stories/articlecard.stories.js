import {
  withKnobs,
  text,
  array,
  date,
  select,
  boolean,
  color,
} from '@storybook/addon-knobs';
import CsArticleCard from '../components/ArticleCard.vue';
import { layoutOptions } from '../utils/validators';

const positionOptions = {
  top: 'top',
  left: 'left',
};

export default {
  component: CsArticleCard,
  title: 'Cards/Article Card',
  decorators: [withKnobs],
};

export const Mobile = () => ({
  components: { CsArticleCard },
  props: {
    title: {
      default: text(
        'title',
        'The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates'
      ),
    },
    summary: {
      default: text(
        'summary',
        'There are numerous goals which can be achieved with custom illustrations. Title images for blog articles are one of them getting more...'
      ),
    },
    author: {
      default: text('author', 'Monica Geller'),
    },
    authorPicture: {
      default: text('authorPicture', 'https://picsum.photos/1000'),
    },
    date: {
      default: date('date'),
    },
    duration: {
      default: text('duration', '3 min'),
    },
    tags: {
      default: array('tags', ['Jobs', 'Business', 'Growth'], ','),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/1000'),
    },
    position: {
      default: select('picture-position', positionOptions, 'top'),
    },
    padding: {
      default: boolean('picture-padding'),
    },
    hideDivider: {
      default: boolean('hide-divider', false),
    },
    mediaIcon: {
      default: text('media-icon'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
    mediaWidthDesktop: {
      default: text('---cs-article-media-width-desktop', '200px'),
    },
    mediaWidthMobile: {
      default: text('---cs-article-media-width-mobile', '100px'),
    },
    mediaHeightDesktop: {
      default: text('---cs-article-media-height-desktop', '200px'),
    },
    mediaHeightMobile: {
      default: text('---cs-article-media-height-mobile', '150px'),
    },
    tagBackground: {
      default: color('---cs-article-tag-background', '#87939b'),
    },
    tagText: {
      default: color('---cs-article-tag-text-color', '#ffffff'),
    },
  },
  template: `
    <div style="margin-top:10px;">
      <cs-article-card
        :picture="picture"
        :tags="tags"
        :title="title"
        :summary="summary"
        :author="author"
				:date="date"
        :duration="duration"
        :picture-position="position"
        :media-icon="mediaIcon"
        :picture-padding="padding"
        :author-picture="authorPicture"
        :hide-divider="hideDivider"
        :layout="layout"
        :style="{
          '---cs-article-media-width-desktop': mediaWidthDesktop,
          '---cs-article-media-width-mobile': mediaWidthMobile,
          '---cs-article-media-height-desktop': mediaHeightDesktop,
          '---cs-article-media-height-mobile': mediaHeightMobile,
          '---cs-article-tag-background': tagBackground,
          '---cs-article-tag-text-color': tagText,
        }"
      >
        <i class="cs-icons-download" slot="options" ></i>
        <i class="cs-icons-share" slot="options" ></i>
        <i class="cs-icons-options" slot="options" ></i>
      </cs-article-card>
    </div>
    `,
});

Mobile.parameters = {
  viewport: {
    defaultViewport: 'mobile1',
  },
};

export const Tablet = () => ({
  components: { CsArticleCard },
  props: {
    title: {
      default: text(
        'title',
        'The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates'
      ),
    },
    summary: {
      default: text(
        'summary',
        'There are numerous goals which can be achieved with custom illustrations. Title images for blog articles are one of them getting more...'
      ),
    },
    author: {
      default: text('author', 'Monica Geller'),
    },
    authorPicture: {
      default: text('authorPicture', 'https://picsum.photos/1000'),
    },
    date: {
      default: date('date'),
    },
    duration: {
      default: text('duration', '3 min'),
    },
    tags: {
      default: array('tags', ['Jobs', 'Business', 'Growth'], ','),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/1000'),
    },
    position: {
      default: select('picture-position', positionOptions, 'top'),
    },
    padding: {
      default: boolean('picture-padding'),
    },
    hideDivider: {
      default: boolean('hide-divider', false),
    },
    mediaIcon: {
      default: text('media-icon'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
    mediaWidthDesktop: {
      default: text('---cs-article-media-width-desktop', '200px'),
    },
    mediaWidthMobile: {
      default: text('---cs-article-media-width-mobile', '100px'),
    },
    mediaHeightDesktop: {
      default: text('---cs-article-media-height-desktop', '200px'),
    },
    mediaHeightMobile: {
      default: text('---cs-article-media-height-mobile', '150px'),
    },
    tagBackground: {
      default: color('---cs-article-tag-background', '#87939b'),
    },
    tagText: {
      default: color('---cs-article-tag-text-color', '#ffffff'),
    },
  },
  template: `
    <div style="margin-top:10px;">
      <cs-article-card
        :picture="picture"
        :tags="tags"
        :title="title"
        :summary="summary"
        :author="author"
        :date="date"
        :duration="duration"
        :picture-position="position"
        :media-icon="mediaIcon"
        :picture-padding="padding"
        :author-picture="authorPicture"
        :hide-divider="hideDivider"
        :layout="layout"
        :style="{
          '---cs-article-media-width-desktop': mediaWidthDesktop,
          '---cs-article-media-width-mobile': mediaWidthMobile,
          '---cs-article-media-height-desktop': mediaHeightDesktop,
          '---cs-article-media-height-mobile': mediaHeightMobile,
          '---cs-article-tag-background': tagBackground,
          '---cs-article-tag-text-color': tagText,
        }"
      >
        <i class="cs-icons-info-filled" slot="media-icon" ></i>
      </cs-article-card>
    </div>
    `,
});

Tablet.parameters = {
  viewport: {
    defaultViewport: 'tablet',
  },
};

export const Desktop = () => ({
  components: { CsArticleCard },
  props: {
    title: {
      default: text(
        'title',
        'The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates'
      ),
    },
    summary: {
      default: text('summary', ''),
    },
    author: {
      default: text('author', 'Monica Geller'),
    },
    authorPicture: {
      default: text('authorPicture', 'https://picsum.photos/100'),
    },
    date: {
      default: date('date'),
    },
    duration: {
      default: text('duration', '3 min'),
    },
    tags: {
      default: array('tags', [], ','),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/1000'),
    },
    position: {
      default: select('picture-position', positionOptions, 'left'),
    },
    padding: {
      default: boolean('picture-padding'),
    },
    hideDivider: {
      default: boolean('hide-divider', true),
    },
    mediaIcon: {
      default: text('media-icon'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
    mediaWidthDesktop: {
      default: text('---cs-article-media-width-desktop', '200px'),
    },
    mediaWidthMobile: {
      default: text('---cs-article-media-width-mobile', '100px'),
    },
    mediaHeightDesktop: {
      default: text('---cs-article-media-height-desktop', '200px'),
    },
    mediaHeightMobile: {
      default: text('---cs-article-media-height-mobile', '150px'),
    },
    tagBackground: {
      default: color('---cs-article-tag-background', '#87939b'),
    },
    tagText: {
      default: color('---cs-article-tag-text-color', '#ffffff'),
    },
  },
  template: `
    <div style="margin-top:10px;">
      <cs-article-card
        :picture="picture"
        :tags="tags"
        :title="title"
        :summary="summary"
        :author="author"
        :date="date"
        :duration="duration"
        :picture-padding="padding"
        :media-icon="mediaIcon"
        :picture-position="position"
        :hide-divider="hideDivider"
        :layout="layout"
        :style="{
          '---cs-article-media-width-desktop': mediaWidthDesktop,
          '---cs-article-media-width-mobile': mediaWidthMobile,
          '---cs-article-media-height-desktop': mediaHeightDesktop,
          '---cs-article-media-height-mobile': mediaHeightMobile,
          '---cs-article-tag-background': tagBackground,
          '---cs-article-tag-text-color': tagText,
        }"
      >
        <i class="cs-icons-audio-filled" slot="media-icon" ></i>
      </cs-article-card>
    </div>
    `,
});

Desktop.parameters = {
  viewport: {
    defaultViewport: 'desktop',
  },
};

export const ArticlesHorizontalList = () => ({
  components: { CsArticleCard },
  props: {
    title: {
      default: text(
        'title',
        'The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates'
      ),
    },
    summary: {
      default: text('summary', ''),
    },
    author: {
      default: text('author', 'Monica Geller'),
    },
    authorPicture: {
      default: text('authorPicture', 'https://picsum.photos/100'),
    },
    date: {
      default: date('date'),
    },
    duration: {
      default: text('duration', '3 min'),
    },
    tags: {
      default: array('tags', [], ','),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/1000'),
    },
    position: {
      default: select('picture-position', positionOptions, 'top'),
    },
    padding: {
      default: boolean('picture-padding'),
    },
    hideDivider: {
      default: boolean('hide-divider', true),
    },
    mediaIcon: {
      default: text('media-icon'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
    mediaWidthDesktop: {
      default: text('---cs-article-media-width-desktop', '200px'),
    },
    mediaWidthMobile: {
      default: text('---cs-article-media-width-mobile', '100px'),
    },
    mediaHeightDesktop: {
      default: text('---cs-article-media-height-desktop', '200px'),
    },
    mediaHeightMobile: {
      default: text('---cs-article-media-height-mobile', '150px'),
    },
  },
  template: `
    <div class="article-list" style="margin-top:10px;">
      <cs-article-card
        :picture="picture"
        :tags="tags"
        :title="title"
        :summary="summary"
        :author="author"
        :author-picture="authorPicture"
        :date="date"
        :duration="duration"
        :picture-padding="padding"
        :media-icon="mediaIcon"
        :picture-position="position"
        :hide-divider="hideDivider"
        :layout="layout"
        :style="{
          '---cs-article-media-width-desktop': mediaWidthDesktop,
          '---cs-article-media-width-mobile': mediaWidthMobile,
          '---cs-article-media-height-desktop': mediaHeightDesktop,
          '---cs-article-media-height-mobile': mediaHeightMobile,
          'max-width': '380px',
          'display': 'inline-block',
        }"
      >
        <i class="cs-icons-audio-filled" slot="media-icon" ></i>
      </cs-article-card>
      <cs-article-card
        :picture="picture"
        :tags="tags"
        :title="title"
        :summary="summary"
        :author="author"
        :author-picture="authorPicture"
        :date="date"
        :duration="duration"
        :picture-padding="padding"
        :media-icon="mediaIcon"
        :picture-position="position"
        :hide-divider="hideDivider"
        :layout="layout"
        :style="{
          '---cs-article-media-width-desktop': mediaWidthDesktop,
          '---cs-article-media-width-mobile': mediaWidthMobile,
          '---cs-article-media-height-desktop': mediaHeightDesktop,
          '---cs-article-media-height-mobile': mediaHeightMobile,
          'max-width': '380px',
          'display': 'inline-block',
        }"
      >
        <i class="cs-icons-audio-filled" slot="media-icon" ></i>
      </cs-article-card>


    </div>
    `,
});

export const ArticlesVerticalList = () => ({
  components: { CsArticleCard },
  props: {
    title: {
      default: text(
        'title',
        'The Surprising Thing That Happens to Stock Markets When the Fed Cuts Interest Rates'
      ),
    },
    summary: {
      default: text('summary', ''),
    },
    author: {
      default: text('author', 'Monica Geller'),
    },
    authorPicture: {
      default: text('authorPicture', 'https://picsum.photos/100'),
    },
    date: {
      default: date('date'),
    },
    duration: {
      default: text('duration', '3 min'),
    },
    tags: {
      default: array('tags', [], ','),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/1000'),
    },
    position: {
      default: select('picture-position', positionOptions, 'left'),
    },
    padding: {
      default: boolean('picture-padding'),
    },
    hideDivider: {
      default: boolean('hide-divider', false),
    },
    mediaIcon: {
      default: text('media-icon'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
    mediaWidthDesktop: {
      default: text('---cs-article-media-width-desktop', '200px'),
    },
    mediaWidthMobile: {
      default: text('---cs-article-media-width-mobile', '100px'),
    },
    mediaHeightDesktop: {
      default: text('---cs-article-media-height-desktop', '200px'),
    },
    mediaHeightMobile: {
      default: text('---cs-article-media-height-mobile', '150px'),
    },
  },
  template: `
    <div class="article-list" style="margin-top:10px;">
      <cs-article-card
        :picture="picture"
        :tags="tags"
        :title="title"
        :summary="summary"
        :author="author"
        :date="date"
        :duration="duration"
        :picture-padding="padding"
        :media-icon="mediaIcon"
        :picture-position="position"
        :hide-divider="hideDivider"
        :layout="layout"
        :style="{
          '---cs-article-media-width-desktop': mediaWidthDesktop,
          '---cs-article-media-width-mobile': mediaWidthMobile,
          '---cs-article-media-height-desktop': mediaHeightDesktop,
          '---cs-article-media-height-mobile': mediaHeightMobile,
          'max-width': '500px',
        }"
      >
        <i class="cs-icons-download" slot="options" ></i>
        <i class="cs-icons-share" slot="options" ></i>
        <i class="cs-icons-options" slot="options" ></i>
      </cs-article-card>
      <cs-article-card
        :picture="picture"
        :tags="tags"
        :title="title"
        :summary="summary"
        :author="author"
        :date="date"
        :duration="duration"
        :picture-padding="padding"
        :media-icon="mediaIcon"
        :picture-position="position"
        :hide-divider="hideDivider"
        :layout="layout"
        :style="{
          '---cs-article-media-width-desktop': mediaWidthDesktop,
          '---cs-article-media-width-mobile': mediaWidthMobile,
          '---cs-article-media-height-desktop': mediaHeightDesktop,
          '---cs-article-media-height-mobile': mediaHeightMobile,
          'max-width': '500px',
        }"
      >
        <i class="cs-icons-options" slot="options" ></i>
      </cs-article-card>


    </div>
    `,
});
