import {
  withKnobs,
  text,
  select,
  date,
  number,
  object,
  boolean,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import CsMessage from '../components/Message.vue';

export default {
  component: CsMessage,
  title: 'Message',
  decorators: [
    withKnobs,
    () => '<div style="background-color:#FFF;padding:20px"><story/></div>',
  ],
};
const sender = {
  name: 'John Doe',
  picture: 'https://picsum.photos/100',
};

export const Message = () => ({
  components: { CsMessage },
  props: {
    outgoing: {
      default: boolean('outgoing', false),
    },
    sender: {
      default: object('sender', sender),
    },
    senderPicture: {
      default: text('sender-picture', sender.picture),
    },
    senderName: {
      default: text('sender-name', sender.name),
    },
    date: {
      default: date('date', new Date()),
    },
    dateString: {
      default: text('date-string', ''),
    },
    slotText: {
      default: text('slot-text', 'Example Message'),
    },
  },
  template: `
    <div>
      <cs-message>Hi, this is David.  I've added you to a new project.  Please accept</cs-message>
      <cs-message>Let me know when that's done</cs-message</cs-message>
      <cs-message outgoing>I'll get back to you the day after the day after the day after tomorrow.  Your patience is appreciated</cs-message>
      <cs-message outgoing>kthxbai</cs-message>
      <cs-message
        :outgoing="outgoing"
        :sender="sender"
        :sender-picture="senderPicture"
        :sender-name="senderName"
        :date="date"
        :date-string="dateString"
        @click="onClick"
        @options="onOptions"
      >{{ slotText }}
      </cs-message>
    </div>
  `,
  methods: {
    onOptions: action('options'),
    onClick: action('click'),
  },
});
