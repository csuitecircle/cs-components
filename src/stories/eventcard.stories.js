import { action } from '@storybook/addon-actions';
import {
  withKnobs,
  text,
  boolean,
  select,
  array,
  date,
} from '@storybook/addon-knobs';
import CsEventCard from '../components/EventCard.vue';
import { layoutOptions } from '../utils/validators';

export default {
  component: CsEventCard,
  title: 'Cards/Event Card',
  decorators: [withKnobs],
};

const positionOptions = {
  null: '',
  top: 'top',
  left: 'left',
};

const template = `
  <div>
    <cs-event-card
      :title="title"
      :picture="picture"
      :datetime="datetime"
      :tags="tags"
      :location="location"
      :price="price"
      :organiser="organiser"
      :attending="attending"
      :date-over-image="dateOverImage"
      :price-icon="priceIcon"
      :media-position="position"
      :layout="layout"
      :hide-buttons="hideButtons"
      @rsvp="response"
      @click="onClick"
      :style="{
        '--cs-card-media-width': width,
        '--cs-card-media-height': height,
      }"
    >
    </cs-event-card >
  </div>
`;

export const Mobile = () => ({
  components: { CsEventCard },
  props: {
    title: {
      default: text(
        'title',
        'C-Suite Woman forum deforestation to low carbon economies forum low carbon'
      ),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/300'),
    },
    datetime: {
      default: date('datetime', new Date()),
    },
    tags: {
      default: array('tags', ['Business', 'Marketing', 'Growth', 'Jobs']),
    },
    location: {
      default: text('location', 'Kwadrat. Students club • Kraków'),
    },
    price: {
      default: text('price', 'INR 1500'),
    },
    organiser: {
      default: text('organiser', 'Booking Machine Agen LC'),
    },
    attending: {
      default: select('attending', [true, false, null], null),
    },
    dateOverImage: {
      default: boolean('dateOverImage', true),
    },
    priceIcon: {
      default: text('price-icon'),
    },
    position: {
      default: select('media-position', positionOptions, null),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
    hideButtons: {
      default: boolean('hide-buttons', false),
    },
    width: {
      default: text(
        '--cs-card-media-width (only for media-position=left)',
        '100px'
      ),
    },
    height: {
      default: text(
        '--cs-card-media-height (only for media-position=top)',
        '120px'
      ),
    },
  },
  template,
  methods: {
    response: action('rsvp'),
    onClick: action('click'),
  },
});
Mobile.parameters = {
  viewport: {
    defaultViewport: 'mobile1',
  },
};

export const Tablet = () => ({
  components: { CsEventCard },
  props: {
    title: {
      default: text(
        'title',
        'C-Suite Woman forum deforestation to low carbon economies forum low carbon'
      ),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/300'),
    },
    datetime: {
      default: date('datetime', new Date()),
    },
    tags: {
      default: array('tags', ['Business', 'Marketing']),
    },
    location: {
      default: text('location', 'Mumbai'),
    },
    price: {
      default: text('price', 'INR 1500'),
    },
    organiser: {
      default: text('organiser', 'Google'),
    },
    attending: {
      default: select('attending', [true, false, null], null),
    },
    dateOverImage: {
      default: boolean('dateOverImage', true),
    },
    priceIcon: {
      default: text('price-icon'),
    },
    position: {
      default: select('media-position', positionOptions, null),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
    hideButtons: {
      default: boolean('hide-buttons', false),
    },
    width: {
      default: text(
        '--cs-card-media-width (only for media-position=left)',
        '100px'
      ),
    },
    height: {
      default: text(
        '--cs-card-media-height (only for media-position=top)',
        '120px'
      ),
    },
  },
  template,
  methods: {
    response: action('rsvp'),
    onClick: action('click'),
  },
});
Tablet.parameters = {
  viewport: {
    defaultViewport: 'tablet',
  },
};

export const Desktop = () => ({
  components: { CsEventCard },
  props: {
    title: {
      default: text(
        'title',
        'C-Suite Woman forum deforestation to low carbon economies forum low carbon'
      ),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/300'),
    },
    datetime: {
      default: date('datetime', new Date()),
    },
    tags: {
      default: array('tags', ['Business', 'Marketing']),
    },
    location: {
      default: text('location', 'Mumbai'),
    },
    price: {
      default: text('price', 'INR 1500'),
    },
    organiser: {
      default: text('organiser', 'Google'),
    },
    attending: {
      default: select('attending', [true, false, null], null),
    },
    dateOverImage: {
      default: boolean('dateOverImage', true),
    },
    priceIcon: {
      default: text('price-icon'),
    },
    position: {
      default: select('media-position', positionOptions, null),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
    hideButtons: {
      default: boolean('hide-buttons', false),
    },
    width: {
      default: text(
        '--cs-card-media-width (only for media-position=left)',
        '100px'
      ),
    },
    height: {
      default: text(
        '--cs-card-media-height (only for media-position=top)',
        '120px'
      ),
    },
  },
  template,
  methods: {
    response: action('rsvp'),
    onClick: action('click'),
  },
});
Desktop.parameters = {
  viewport: {
    defaultViewport: 'desktop',
  },
};
