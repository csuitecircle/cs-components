import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, select, color } from '@storybook/addon-knobs';

import CsButton from '../components/Button.vue';

export default {
  component: CsButton,
  title: 'Button',
  decorators: [withKnobs],
};

const variantOptions = ['primary', 'secondary', 'success', 'warning', 'danger', 'info', 'default'];

const sizeOptions = ['small', 'medium', 'large'];

const fillOptions = ['solid', 'clear', 'outline'];

const cornerOptions = ['round', 'rounded', 'straight'];

export const Button = () => ({
  components: { CsButton },
  props: {
    href: {
      default: text('href', ''),
    },
    to: {
      default: text('to (Router link)', ''),
    },
    variant: {
      default: select('variant', variantOptions, 'primary'),
    },
    fill: {
      default: select('fill', fillOptions, 'solid'),
    },
    size: {
      default: select('size', sizeOptions, 'medium'),
    },
    corners: {
      default: select('corners', cornerOptions, 'round'),
    },
    uppercase: {
      default: boolean('uppercase', false),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    block: {
      default: boolean('block', false),
    },
    padding: {
      default: text('--cs-button-padding'),
    },
    color: {
      default: color('--cs-button-color'),
    },
    hoverColor: {
      default: color('--cs-button-color-hover'),
    },
    fontColor: {
      default: color('--cs-button-text-color'),
    },
    hoverFontColor: {
      default: color('--cs-button-text-color-hover'),
    },
  },
  template: `
    <div>
      <cs-button
        :href="href"
        :to="to"
        :disabled="disabled"
        :variant="variant"
        :fill="fill"
        :block="block"
        :size="size"
        :corners="corners"
        :uppercase="uppercase"
        @click="action"
        :style="{
          '--cs-button-padding': padding,
          '--cs-button-color': color,
          '--cs-button-color-hover': hoverColor,
          '--cs-button-text-color': fontColor,
          '--cs-button-text-color-hover': hoverFontColor,
        }"
      >
       Hello World
      </cs-button >
      <cs-button
        :href="href"
        :to="to"
        :disabled="disabled"
        :variant="variant"
        :fill="fill"
        :block="block"
        :size="size"
        :corners="corners"
        :uppercase="uppercase"
        @click="action"
        :style="{
          '--cs-button-padding': padding,
          '--cs-button-color': color,
          '--cs-button-color-hover': hoverColor,
          '--cs-button-text-color': fontColor,
          '--cs-button-text-color-hover': hoverFontColor,
        }"
      >
        <i class="cs-icons-twitter-filled"></i>
      </cs-button >
      <cs-button
        :href="href"
        :to="to"
        :disabled="disabled"
        :variant="variant"
        :fill="fill"
        :corners="corners"
        :uppercase="uppercase"
        :block="block"
        :size="size"
        @click="action"
        :style="{
          '--cs-button-padding': padding,
          '--cs-button-color': color,
          '--cs-button-color-hover': hoverColor,
          '--cs-button-text-color': fontColor,
          '--cs-button-text-color-hover': hoverFontColor,
        }"
      >
        <i class="cs-icons-twitter-filled"></i>
        <span>Tweet</span>
      </cs-button >
    </div>
    `,
  methods: { action: action('clicked') },
});
