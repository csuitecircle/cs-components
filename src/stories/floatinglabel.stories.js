import { action } from '@storybook/addon-actions';
import { withKnobs, text, color } from '@storybook/addon-knobs';

import CsFloatingLabel from '../components/FloatingLabel.vue';
import CsProfile from '../components/Profile.vue';
import CsEventCard from '../components/EventCard.vue';

export default {
  component: CsFloatingLabel,
  title: 'Floating Label',
  decorators: [withKnobs],
};

export const FloatingLabel = () => ({
  components: { CsFloatingLabel, CsProfile, CsEventCard },
  props: {
    label: {
      default: text('label', 'Coming Soon'),
    },
    color: {
      default: color('--cs-floating-label-color'),
    },
    textColor: {
      default: color('--cs-floating-label-text-color'),
    },
  },
  template: `
    <div>
      <cs-floating-label :label="label" :style="{'margin-right': 'var(--cs-card-margin-x)', '--cs-floating-label-color': color, '--cs-floating-label-text-color': textColor}">
        <cs-event-card
          style="margin-right: 0;"
          picture="https://picsum.photos/200"
          title="Awesome New Event"
          datetime="2021-12-31"
          :tags="['Cold', 'Penguins', 'Ice']"
          location="South Pole"
          price="Free"
          organiser="Santa & Co."
          attending
        />
      </cs-floating-label >
      <br/>
      <cs-floating-label :label="label" :style="{'margin-right': 'var(--cs-card-margin-x)', '--cs-floating-label-color': color, '--cs-floating-label-text-color': textColor}">
        <cs-event-card
          style="margin-right: 0;"
          picture="https://picsum.photos/200"
          title="Awesome New Event"
          datetime="2021-12-31"
          :tags="['Cold', 'Penguins', 'Ice']"
          location="South Pole"
          price="Free"
          organiser="Santa & Co."
          attending
        />
        <template slot="label">Slot Label</template>
      </cs-floating-label >
    </div>
    `,
});
