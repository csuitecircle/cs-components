import {
  withKnobs,
  text,
  number,
  boolean,
  object,
  select,
} from '@storybook/addon-knobs';
import CsStaticMap from '../components/StaticMap.vue';
import { layoutOptions } from '../utils/validators';

export default {
  component: CsStaticMap,
  title: 'Static Map',
  decorators: [withKnobs],
};

const template = `
  <cs-static-map
    :apiKey="apiKey"
    :address="address"
    :zoom="zoom"
    :showAddress="showAddress"
    :valid-api-key-label="validApiKeyLabel"
    :valid-address-label="validAddressLabel"
    :center="center"
    :layout="layout"
		:style="{
			'--cs-static-map-width': width,
			'--cs-static-map-height': height,
		}"
  >
  </cs-static-map>
  `;

export const Mobile = () => ({
  components: { CsStaticMap },
  props: {
    apiKey: {
      default: text('api-key', ''),
    },
    address: {
      default: text('address', '9-01 33rd Rd, Queens, NY 11106, United States'),
    },
    zoom: {
      default: number('zoom', 16),
    },
    showAddress: {
      default: boolean('showAddress', true),
    },
    center: {
      default: object('center', { lat: 40.767023, lng: -73.9404448 }),
    },
    width: {
      default: text('--cs-static-map-width', '100%'),
    },
    height: {
      default: text('--cs-static-map-height', '140px'),
    },
    validApiKeyLabel: {
      default: text('valid-api-key-label'),
    },
    validAddressLabel: {
      default: text('valid-address-label'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template,
});
Mobile.parameters = {
  viewport: {
    defaultViewport: 'mobile1',
  },
};

export const Tablet = () => ({
  components: { CsStaticMap },
  props: {
    apiKey: {
      default: text('api-key', ''),
    },
    address: {
      default: text('address', '9-01 33rd Rd, Queens, NY 11106, United States'),
    },
    zoom: {
      default: number('zoom', 16),
    },
    showAddress: {
      default: boolean('showAddress', true),
    },
    center: {
      default: object('center', { lat: 40.767023, lng: -73.9404448 }),
    },
    width: {
      default: text('--cs-static-map-width', '100%'),
    },
    height: {
      default: text('--cs-static-map-height', '310px'),
    },
    validApiKeyLabel: {
      default: text('valid-api-key-label'),
    },
    validAddressLabel: {
      default: text('valid-address-label'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template,
});
Tablet.parameters = {
  viewport: {
    defaultViewport: 'tablet',
  },
};

export const Desktop = () => ({
  components: { CsStaticMap },
  props: {
    apiKey: {
      default: text('api-key', ''),
    },
    address: {
      default: text('address', '9-01 33rd Rd, Queens, NY 11106, United States'),
    },
    zoom: {
      default: number('zoom', 16),
    },
    showAddress: {
      default: boolean('showAddress', true),
    },
    center: {
      default: object('center', { lat: 40.767023, lng: -73.9404448 }),
    },
    width: {
      default: text('--cs-static-map-width', '100%'),
    },
    height: {
      default: text('--cs-static-map-height', '310px'),
    },
    validApiKeyLabel: {
      default: text('valid-api-key-label'),
    },
    validAddressLabel: {
      default: text('valid-address-label'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template,
});
Desktop.parameters = {
  viewport: {
    defaultViewport: 'desktop',
  },
};
