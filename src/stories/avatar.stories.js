import { withKnobs, text, boolean, color, select, number } from '@storybook/addon-knobs';
import CsAvatar from '../components/Avatar.vue';

const sizeOptions = ['small', 'medium', 'large'];
const variantOptions = {
  ' ': null,
  primary: 'primary',
  secondary: 'secondary',
  default: 'default',
  success: 'success',
  warning: 'warning',
  danger: 'danger',
  info: 'info',
};
export default {
  title: 'Avatar',
  component: CsAvatar,
  decorators: [withKnobs],
};

export const Default = () => ({
  components: { CsAvatar },
  props: {
    picture: {
      default: text('Picture', 'https://picsum.photos/100'),
    },
    variant: {
      default: select('variant', variantOptions, null),
    },
    name: {
      default: text('Name', 'Walt Smith Whitman'),
    },
    size: {
      default: select('Size', sizeOptions, 'medium'),
    },
  },
  template: `
    <cs-avatar
      :picture="picture"
      :variant="variant"
      :name="name"
      :size="size"
    />
  `,
});

export const CustomAvatar = () => ({
  components: { CsAvatar },
  props: {
    picture: {
      default: text('Picture', 'https://picsum.photos/100'),
    },
    name: {
      default: text('Name', 'Walt Smith Whitman'),
    },
    size: {
      default: text('--cs-avatar-size', '55px'),
    },
    color: {
      default: color('--cs-avatar-color', '#C40088'),
    },
    fontSize: {
      default: text('--cs-avatar-font-size', '14px'),
    },
    fontColor: {
      default: color('--cs-avatar-font-color', '#FFFFFF'),
    },
    borderWidth: {
      default: text('--cs-avatar-border-width', '2px'),
    },
  },
  template: `
    <cs-avatar
      :picture="picture"
      :name="name"
      :style="{
        '--cs-avatar-size': size,
        '--cs-avatar-color': color,
        '--cs-avatar-font-size': fontSize,
				'--cs-avatar-font-color': fontColor,
				'--cs-avatar-border-width': borderWidth,
      }"
    />
  `,
});
