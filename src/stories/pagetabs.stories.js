import {
  withKnobs,
  text,
  number,
  color,
  array,
  object,
  select,
  boolean,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsPageTabs from '../components/PageTabs.vue';
import CsPageTab from '../components/PageTab.vue';
import { layoutOptions } from '../utils/validators';

export default {
  title: 'PageTabs',
  component: CsPageTabs,
  decorators: [withKnobs],
};

const tabs = [
  { label: 'Twitter', icon: 'cs-icons-twitter-filled' },
  { label: 'Jobs' },
  { label: 'Email', icon: 'cs-icons-email-filled' },
  { label: 'Search', icon: 'cs-icons-search' },
];

const alignments = ['left', 'right', 'center', 'justify'];

export const Default = () => ({
  components: { CsPageTabs },
  props: {
    tabs: {
      default: array(
        'tabs',
        tabs.map((t) => t.label)
      ),
    },
    index: {
      default: number('v-model', 1),
    },
    align: {
      default: select('align', alignments),
    },
    noBorder: {
      default: boolean('no-border'),
    },
    noUnderline: {
      default: boolean('no-underline'),
    },
    layout: {
      default: select('layout (affects default label padding)', layoutOptions),
    },
    activeColor: {
      default: color('--cs-page-tabs-active-color'),
    },
    labelPadding: {
      default: text('--cs-page-tabs-label-padding', ''),
    },
    contentPadding: {
      default: text('--cs-page-tabs-content-padding', '16px'),
    },
  },
  template: `
    <cs-page-tabs
			:tabs="tabs"
			:value="index"
			:align="align"
			:no-border="noBorder"
			:no-underline="noUnderline"
			:layout="layout"
			@action="action"
			:style="styleObject"
		/>
  `,
  computed: {
    styleObject() {
      const style = {
        '--cs-page-tabs-active-color': this.activeColor,
        '--cs-page-tabs-content-padding': this.contentPadding,
      };
      if (this.labelPadding)
        style['--cs-page-tabs-label-padding'] = this.labelPadding;
      return style;
    },
  },
  methods: {
    action: action('action'),
  },
});

export const WithIcons = () => ({
  components: { CsPageTabs },
  props: {
    tabs: {
      default: object('tabs', tabs),
    },
    index: {
      default: number('v-model', 1),
    },
    align: {
      default: select('align', alignments),
    },
    noBorder: {
      default: boolean('no-border'),
    },
    noUnderline: {
      default: boolean('no-underline'),
    },
    layout: {
      default: select('layout (affects default label padding)', layoutOptions),
    },
    activeColor: {
      default: color('--cs-page-tabs-active-color'),
    },
    labelPadding: {
      default: text('--cs-page-tabs-label-padding', ''),
    },
    contentPadding: {
      default: text('--cs-page-tabs-content-padding', '16px'),
    },
  },
  template: `
    <cs-page-tabs
			:tabs="tabs"
			:value="index"
			:align="align"
			:no-border="noBorder"
			:no-underline="noUnderline"
			:layout="layout"
			@action="action"
			:style="styleObject"
		/>
  `,
  computed: {
    styleObject() {
      const style = {
        '--cs-page-tabs-active-color': this.activeColor,
        '--cs-page-tabs-content-padding': this.contentPadding,
      };
      if (this.labelPadding)
        style['--cs-page-tabs-label-padding'] = this.labelPadding;
      return style;
    },
  },
  methods: {
    action: action('action'),
  },
});

export const SubComponents = () => ({
  components: { CsPageTabs, CsPageTab },
  props: {
    index: {
      default: number('v-model', 1),
    },
    align: {
      default: select('align', alignments),
    },
    noBorder: {
      default: boolean('no-border'),
    },
    noUnderline: {
      default: boolean('no-underline'),
    },
    layout: {
      default: select('layout (affects default label padding)', layoutOptions),
    },
    activeColor: {
      default: color('--cs-page-tabs-active-color'),
    },
    labelPadding: {
      default: text('--cs-page-tabs-label-padding', ''),
    },
    contentPadding: {
      default: text('--cs-page-tabs-content-padding', '16px'),
    },
  },
  template: `
    <cs-page-tabs 
			:value="index" 
			@input="action" 
			:align="align"
			:no-border="noBorder" 
			:no-underline="noUnderline"
			:layout="layout"
			:style="styleObject"
		>
      <cs-page-tab label="Tweet" icon="cs-icons-twitter-filled"></cs-page-tab>
      <cs-page-tab label="Maps">world</cs-page-tab>
      <cs-page-tab label="Image">
        <h3>Hello World</h3>
        <img src="https://picsum.photos/1000" />
      </cs-page-tab>
    </cs-page-tabs>`,
  computed: {
    styleObject() {
      const style = {
        '--cs-page-tabs-active-color': this.activeColor,
        '--cs-page-tabs-content-padding': this.contentPadding,
      };
      if (this.labelPadding)
        style['--cs-page-tabs-label-padding'] = this.labelPadding;
      return style;
    },
  },
  methods: {
    action: action('input'),
  },
});
