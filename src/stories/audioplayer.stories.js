import { action } from '@storybook/addon-actions';
import {
  withKnobs,
  text,
  boolean,
  color,
  select,
  number,
} from '@storybook/addon-knobs';
import CsAudioPlayer from '../components/AudioPlayer.vue';
import CsTag from '../components/SmallTag.vue';

export default {
  title: 'Audio Player',
  component: CsAudioPlayer,
  decorators: [withKnobs],
};

export const AudioPlayer = () => ({
  components: { CsAudioPlayer, CsTag },
  props: {
    src: {
      default: text(
        'src',
        'https://upload.wikimedia.org/wikipedia/commons/2/26/Hairy_Larry-Iriver_Blues-Mono_Monster.ogg'
      ),
    },
    title: {
      default: text('title', 'Architecture Spotlight: Indonesia'),
    },
    seekTime: {
      default: number('seek-time', 10),
    },
    errorLabel: {
      default: text('error-label'),
    },
    transparent: {
      default: boolean('transparent', false),
    },
  },
  data() {
    return {
      audioEvents: {
        audioprocess: this.onEvent,
        canplay: this.onEvent,
        canplaythrough: this.onEvent,
        complete: this.onEvent,
        durationchange: this.onEvent,
        emptied: this.onEvent,
        ended: this.onEvent,
        loadeddata: this.onEvent,
        loadedmetadata: this.onEvent,
        pause: this.onEvent,
        play: this.onEvent,
        playing: this.onEvent,
        ratechange: this.onEvent,
        seeked: this.onEvent,
        seeking: this.onEvent,
        stalled: this.onEvent,
        suspend: this.onEvent,
        timeupdate: this.onEvent,
        volumechange: this.onEvent,
        waiting: this.onEvent,
      },
    };
  },
  template: `
  <div style="margin-top: 30px">
    <div style="margin-bottom: 20px;">
    <cs-audio-player
        :src="src"
        :title="title"
        :seek-time="seekTime"
        :transparent="transparent"
        :error-label="errorLabel"
				v-on="audioEvents"
    ></cs-audio-player>
    </div>
    <div style="font-weight: var(--cs-font-weight-black); margin-bottom: 10px; overflow: hidden;">
      Architecture Spotlight: Indonesia
    </div>
    <div style="display: flex; justify-content: space-between; margin-bottom: 20px" class="cs-textstyle-paragraph-small">
      <span>
        <cs-tag
        >
            Architecture
        </cs-tag >
      </span>
      <span>
        2 hours ago
      </span>
    </div>
    <div class="cs-textstyle-paragraph">
      Lorem ipsum dolor sit amet, consetet sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam dolore magna aliquyam erat, sed diam eirmod tempore
    </div>
	</div>
  `,
  methods: {
    onEvent($event) {
      action($event.type)($event);
    },
  },
});
AudioPlayer.parameters = {
  viewport: {
    defaultViewport: 'mobile1',
  },
};
