import { action } from '@storybook/addon-actions';
import { withKnobs, text, select, color } from '@storybook/addon-knobs';
import CsSecondaryHeader from '../components/SecondaryHeader.vue';

export default {
  component: CsSecondaryHeader,
  title: 'Headers/Secondary Header',
  decorators: [withKnobs],
};

export const SecondaryHeader = () => ({
  components: { CsSecondaryHeader },
  props: {
    title: {
      default: text('title', 'Secondary Header'),
    },
    headerSlot: {
      default: text('(slot) title', 'Slot Header'),
    },
    alignTitle: {
      default: select('align-title', ['center', 'left', 'right'], 'left'),
    },
    backIcon: {
      default: text('back-icon'),
    },
    backgroundColor: {
      default: color('--cs-secondary-header-background-color'),
    },
    textColor: {
      default: color('--cs-secondary-header-text-color'),
    },
  },
  template: `
    <cs-secondary-header
      :title="title"
      :align-title="alignTitle"
      :back-icon="backIcon"
      @back='onBack'
      :style="{
        '--cs-secondary-header-background-color': backgroundColor,
        '--cs-secondary-header-text-color': textColor
      }"
    >
      <template slot="title">{{headerSlot}}</template>
    </cs-secondary-header>
    `,
  methods: { onBack: action('back') },
});
SecondaryHeader.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const SecondaryHeaderIcons = () => ({
  components: { CsSecondaryHeader },
  props: {
    title: {
      default: text('title', 'Secondary Header'),
    },
    headerSlot: {
      default: text('(slot) title', 'Slot Header'),
    },
    alignTitle: {
      default: select('align-title', ['center', 'left', 'right'], 'left'),
    },
    backIcon: {
      default: text('back-icon', 'cs-icons-twitter'),
    },
    backgroundColor: {
      default: color('--cs-secondary-header-background-color'),
    },
    textColor: {
      default: color('--cs-secondary-header-text-color'),
    },
  },
  template: `
    <cs-secondary-header
      :title="title"
      :align-title="alignTitle"
      :back-icon="backIcon"
      @back='onBack'
      :style="{
        '--cs-secondary-header-background-color': backgroundColor,
        '--cs-secondary-header-text-color': textColor
      }"
    >
      <template slot="title">{{headerSlot}}</template>
      <template  v-slot:buttons>
        <i class="cs-icons-search-filled" ></i>
        <i class="cs-icons-info"></i>
      </template>
    </cs-secondary-header>
    `,
  methods: { onBack: action('back') },
});
SecondaryHeaderIcons.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const SecondaryHeaderButton = () => ({
  components: { CsSecondaryHeader },
  props: {
    title: {
      default: text('title'),
    },
    headerSlot: {
      default: text('(slot) title', 'Slot Header'),
    },
    alignTitle: {
      default: select('align-title', ['center', 'left', 'right'], 'left'),
    },
    backIcon: {
      default: text('back-icon'),
    },
    backgroundColor: {
      default: color('--cs-secondary-header-background-color'),
    },
    textColor: {
      default: color('--cs-secondary-header-text-color'),
    },
  },
  template: `
    <cs-secondary-header
      :title="title"
      :align-title="alignTitle"
      :back-icon="backIcon"
      @back='onBack'
      :style="{
        '--cs-secondary-header-background-color': backgroundColor,
        '--cs-secondary-header-text-color': textColor
      }"
    >
      <template slot="title">{{headerSlot}}</template>
      <cs-button slot="buttons" fill="clear">Send</cs-button>
    </cs-secondary-header>
    `,
  methods: { onBack: action('back') },
});
SecondaryHeaderButton.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};
