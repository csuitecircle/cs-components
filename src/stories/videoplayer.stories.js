import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';
import CsVideoPlayer from '../components/VideoPlayer.vue';

export default {
  component: CsVideoPlayer,
  title: 'Video Player',
  decorators: [withKnobs],
};

export const Default = () => ({
  components: { CsVideoPlayer },
  props: {
    picture: {
      default: text('picture', 'https://picsum.photos/500'),
    },
    src: {
      default: text(
        'src',
        'https://cdn.graphexpress.com/15600fe9-713d-4ba1-a1cd-4a2d2c916b5c/8d9c1b55-7be8-4259-9ce8-b40ee95c3dc8'
      ),
    },
    playIcon: {
      default: text('play-icon'),
    },
    loadingErrorLabel: {
      default: text('loading-error-label'),
    },
  },
  data() {
    return {
      videoEvents: {
        canplay: this.onEvent,
        canplaythrough: this.onEvent,
        complete: this.onEvent,
        durationchange: this.onEvent,
        emptied: this.onEvent,
        ended: this.onEvent,
        loadeddata: this.onEvent,
        loadedmetadata: this.onEvent,
        pause: this.onEvent,
        play: this.onEvent,
        playing: this.onEvent,
        progress: this.onEvent,
        ratechange: this.onEvent,
        seeked: this.onEvent,
        seeking: this.onEvent,
        stalled: this.onEvent,
        suspend: this.onEvent,
        timeupdate: this.onEvent,
        volumechange: this.onEvent,
        waiting: this.onEvent,
      },
    };
  },
  template: `
    <cs-video-player 
      :picture="picture"
      :src="src"
      :play-icon="playIcon"
      :loading-error-label="loadingErrorLabel"
      v-on="videoEvents"
      @error="onError"></cs-video-player>
  `,
  methods: {
    onEvent($event) {
      action($event.type)($event);
    },
    onError: action('error'),
  },
});
