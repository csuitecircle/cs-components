import { withKnobs, object, select, text, number, color } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsAvatarList from '../components/AvatarList.vue';

export default {
  title: 'Avatar List',
  component: CsAvatarList,
  decorators: [withKnobs],
};

const userList = [
  {
    name: 'Walt Smith Whitman',
    picture: '',
  },
  {
    name: 'Eddie Murphy',
    picture: 'https://picsum.photos/100',
  },
  {
    name: 'Jamie Foxx',
  },
  {
    name: 'Juan Deag',
    picture: 'https://picsum.photos/120',
  },
  {
    name: 'Chad Buoi',
    picture: 'https://picsum.photos/140',
  },
  {
    name: 'Michael Jackson',
    picture: 'https://picsum.photos/160',
  },
];

const sizeOptions = ['small', 'medium', 'large'];
const variantOptions = ['primary', 'secondary', 'success', 'warning', 'danger', 'info', 'default'];

export const AvatarList = () => ({
  components: { CsAvatarList },
  props: {
    userList: {
      default: object('users', userList),
    },
    variant: {
      default: select('variant', variantOptions, 'primary'),
    },
    size: {
      default: select('size', sizeOptions, 'medium'),
    },
    max: {
      default: number('max', 5),
    },
    totalUserCount: {
      default: number('total-user-count'),
    },
  },
  template: `
    <cs-avatar-list
      :userList="userList"
      :total-user-count="totalUserCount"
      :size="size"
      :variant="variant"
      :max="max"
			@user-click='click'
			@more-profiles='more'
    >
    </cs-avatar-list>
  `,
  methods: { click: action('user-click'), more: action('more-profiles') },
});

export const CustomAvatarList = () => ({
  components: { CsAvatarList },
  props: {
    userList: {
      default: object('users', userList),
    },
    totalUserCount: {
      default: number('total-user-count', 10),
    },
    max: {
      default: number('max', 5),
    },
    size: {
      default: text('--cs-avatar-list-size', '55px'),
    },
    color: {
      default: color('--cs-avatar-list-color'),
    },
    fontSize: {
      default: text('--cs-avatar-list-font-size', '14px'),
    },
    fontColor: {
      default: color('--cs-avatar-list-font-color'),
    },
    borderWidth: {
      default: text('--cs-avatar-list-border-width', '2px'),
    },
    borderColor: {
      default: color('--cs-avatar-list-border-color'),
    },
    negMargin: {
      default: text('--cs-avatar-list-negative-margin', '-12px'),
    },
  },

  template: `
    <cs-avatar-list
      :userList="userList"
      :max="max"
      :total-user-count="totalUserCount"
			@user-click='click'
			@more-profiles='more'
      :style="{
				'--cs-avatar-list-size': size,
        '--cs-avatar-list-color': color,
        '--cs-avatar-list-font-size': fontSize,
        '--cs-avatar-list-font-color': fontColor,
				'--cs-avatar-list-border-width': borderWidth,
				'--cs-avatar-list-border-color': borderColor,
				'--cs-avatar-list-negative-margin': negMargin,
      }"
    >
    </cs-avatar-list>
  `,
  methods: { click: action('user-click'), more: action('more-profiles') },
});
