import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs';
import CsLinkPreview from '../components/LinkPreview.vue';

export default {
  component: CsLinkPreview,
  title: 'Link Preview',
  decorators: [withKnobs],
};

export const SmallMobile = () => ({
  components: { CsLinkPreview },
  props: {
    title: {
      default: text(
        'title',
        'Indonesia Architecture spotlight informational document'
      ),
    },
    src: {
      default: text('source', 'https://www.lipsum.com/'),
    },
    picture: {
      default: text('picture'),
    },
    picturePadding: {
      default: boolean('picture-padding', false),
    },
    picturePosition: {
      default: select('picture-position', ['top', 'left'], 'left'),
    },
    description: {
      default: text('description'),
    },
    linkIcon: {
      default: text('link-icon'),
    },
    linkLabel: {
      default: text('link-label'),
    },
    hideLabel: {
      default: boolean('hide-label', false),
    },
  },
  template: `
    <div>
      <cs-link-preview
        @open='onOpen'
        :title='title'
        :src='src'
        :picture='picture'
        :picture-padding="picturePadding"
        :picture-position="picturePosition"
				:description='description'
        :link-icon='linkIcon'
        :link-label='linkLabel'
        :hide-label='hideLabel'
      >
      </cs-link-preview>
    </div>
    `,
  methods: { onOpen: action('open') },
});

SmallMobile.parameters = {
  viewport: {
    defaultViewport: 'mobile1',
  },
};

export const LargeMobile = () => ({
  components: { CsLinkPreview },
  props: {
    title: {
      default: text(
        'title',
        'Indonesia Architecture spotlight informational document'
      ),
    },
    src: {
      default: text('source', 'https://www.lipsum.com/'),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/300'),
    },
    picturePadding: {
      default: boolean('picture-padding', false),
    },
    picturePosition: {
      default: select('picture-position', ['top', 'left'], 'left'),
    },
    description: {
      default: text('description'),
    },
    linkIcon: {
      default: text('link-icon'),
    },
    linkLabel: {
      default: text('link-label'),
    },
    hideLabel: {
      default: boolean('hide-label', false),
    },
  },
  template: `
    <div>
      <cs-link-preview
        @open='onOpen'
        :title='title'
        :src='src'
        :picture='picture'
        :picture-padding="picturePadding"
        :picture-position="picturePosition"
        :description='description'
				:link-icon='linkIcon'
        :link-label='linkLabel'
        :hide-label='hideLabel'
      >
      </cs-link-preview>
    </div>
    `,
  methods: { onOpen: action('open') },
});

LargeMobile.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const Tablet = () => ({
  components: { CsLinkPreview },
  props: {
    title: {
      default: text(
        'title',
        'Indonesia Architecture spotlight informational document'
      ),
    },
    src: {
      default: text('source', 'https://www.lipsum.com/'),
    },
    picture: {
      default: text('picture', 'https://picsum.photos/300'),
    },
    picturePadding: {
      default: boolean('picture-padding', false),
    },
    picturePosition: {
      default: select('picture-position', ['top', 'left'], 'left'),
    },
    description: {
      default: text(
        'description',
        'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'
      ),
    },
    linkIcon: {
      default: text('link-icon'),
    },
    linkLabel: {
      default: text('link-label'),
    },
    hideLabel: {
      default: boolean('hide-label', false),
    },
  },
  template: `
    <div>
      <cs-link-preview
        @open='onOpen'
        :title='title'
        :src='src'
        :picture='picture'
        :picture-padding="picturePadding"
        :picture-position="picturePosition"
        :description='description'
				:link-icon='linkIcon'
        :link-label='linkLabel'
        :hide-label='hideLabel'
      >
      </cs-link-preview>
    </div>
    `,
  methods: { onOpen: action('open') },
});

Tablet.parameters = {
  viewport: {
    defaultViewport: 'tablet',
  },
};
