import { withKnobs, select, text, color } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsBadge from '../components/Badge.vue';

export default {
  title: 'Badge',
  component: CsBadge,
  decorators: [withKnobs],
};

const variants = ['primary', 'secondary', 'success', 'warning', 'danger', 'info', 'default'];

export const Badge = () => ({
  components: { CsBadge },
  props: {
    text: {
      default: text('text', '8'),
    },
    variant: {
      default: select('variant', variants, 'primary'),
    },
    color: {
      default: color('--cs-badge-color'),
    },
    textColor: {
      default: color('--cs-badge-text-color'),
    },
    fontSize: {
      default: text('--cs-badge-font-size', '10px'),
    },
  },
  template: `
    <div>
      <cs-badge :variant="variant"
				:style="{
					'--cs-badge-color': color,
					'--cs-badge-text-color': textColor,
					'--cs-badge-font-size': fontSize,
				}"
			>
        Hello
      </cs-badge>
      <br/>
      <cs-badge :text="text" :variant="variant"
				:style="{
					'--cs-badge-color': color,
					'--cs-badge-text-color': textColor,
					'--cs-badge-font-size': fontSize,
				}"
			/>
    </div>
  `,
  methods: { action: action('clicked') },
});
