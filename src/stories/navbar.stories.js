import { withKnobs, object, text, number, select } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import StoryRouter from 'storybook-vue-router';
import CsNavbar from '../components/Navbar.vue';
import CsButton from '../components/Button.vue';
import { layoutOptions } from '../utils/validators';

const Home = { template: '<div>foo</div>' };
const Contacts = { template: '<div>bar</div>' };
const About = { template: '<div>bar</div>' };
const Products = { template: '<div>bar</div>' };

export default {
  title: 'Navbar',
  component: CsNavbar,
  decorators: [withKnobs, StoryRouter()],
};

const navLinks = [
  {
    name: 'Home',
    link: '#home',
  },
  { name: 'About', link: '#about' },
  {
    name: 'Contacts',
    link: '#contact',
  },
  {
    name: 'Products',
    link: '/products',
  },
];

const template = `
  <div>
    <cs-navbar
    :name="name"
    :nav-links="links"
		:active="active"
    :layout="layout"
    >
    <cs-button variant="secondary" block="block"> Login </cs-button>
    </cs-navbar >
  </div>
`;
export const Mobile = () => ({
  components: { CsButton, CsNavbar },
  props: {
    links: {
      default: object('navlinks', navLinks),
    },
    active: {
      default: number('active', 0),
    },
    name: {
      default: text('Brand name', 'Hello World'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template,
});
Mobile.parameters = {
  viewport: {
    defaultViewport: 'mobile1',
  },
};
export const Tablet = () => ({
  components: { CsButton, CsNavbar },
  props: {
    links: {
      default: object('navlinks', navLinks),
    },
    active: {
      default: number('active', 0),
    },
    name: {
      default: text('Brand name', 'Hello World'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template,
});
Tablet.parameters = {
  viewport: {
    defaultViewport: 'tablet',
  },
};
export const Desktop = () => ({
  components: { CsButton, CsNavbar },
  props: {
    links: {
      default: object('navlinks', navLinks),
    },
    active: {
      default: number('active', 0),
    },
    name: {
      default: text('Brand name', 'Hello World'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template,
});
Desktop.parameters = {
  viewport: {
    defaultViewport: 'desktop',
  },
};

export const LogoNavbar = () => ({
  components: { CsNavbar, CsButton },
  props: {
    links: {
      default: object('navlinks', navLinks),
    },
    active: {
      default: number('active', 0),
    },
    name: {
      default: text('Brand name', 'Hello World'),
    },
    logoImg: {
      default: text('Logo', 'https://cdn.mos.cms.futurecdn.net/BVb3Wzn9orDR8mwVnhrSyd-970-80.jpg'),
    },
    layout: {
      default: select('layout', layoutOptions, null),
    },
  },
  template: `
    <cs-navbar
      :name='name'
      :nav-links="links"
      :logoImg="logoImg"
			:active="active"
      :layout="layout"
    >
      <cs-button variant="secondary" block="block"> Login </cs-button>
    </cs-navbar>
  `,
});
