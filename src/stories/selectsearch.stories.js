import { withKnobs, text, select, boolean, object, array } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsSelectSearch from '../components/SelectSearch.vue';

export default {
  title: 'Select Search',
  component: CsSelectSearch,
  decorators: [withKnobs],
};

const selectOptions = [
  {
    city: 'City A',
    id: 'A',
  },
  {
    city: 'City B',
    id: 'B',
  },
  {
    city: 'City C',
    id: 'C',
  },
  {
    city: 'City D',
    id: 'D',
  },
];
const selectOptionGroups = [
  {
    title: 'Group 1',
    options: [
      {
        label: 'Option 1.1',
        value: 'value1.1',
      },
      {
        label: 'Option 1.2',
        value: 'value1.2',
      },
    ],
  },
  {
    title: 'Group 2',
    options: [
      {
        label: 'Option 2.1',
        value: 'value2.1',
      },
      {
        label: 'Option 2.2',
        value: 'value2.2',
      },
      {
        label: 'Option 2.3',
        value: 'value2.3',
      },
    ],
  },
];

export const SelectSearch = () => ({
  components: { CsSelectSearch },
  data() {
    return {
      val: 'B',
    };
  },
  props: {
    placeholder: {
      default: text('placeholder', 'Search'),
    },
    options: {
      default: object('options', selectOptions),
    },
    // searchProperties: {
    //   default: array('search-properties', ['value', 'label']),
    // },
    customSearch: {
      default: boolean('custom-search', false),
    },
    noResultsLabel: {
      default: text('no-results-label'),
    },
    labelKey: {
      default: text('label-key', 'city'),
    },
    valueKey: {
      default: text('value-key', 'id'),
    },
    icon: {
      default: text('icon'),
    },
  },
  template: `
    <div>
      <cs-select-search
        :placeholder="placeholder"
        :options="options"
  			:custom-search="customSearch"
        :no-results-label="noResultsLabel"
        :label-key="labelKey"
        :value-key="valueKey"
        :icon="icon"
        @select="onSelect"
        @input="onInput"
  			@query="onQuery"
      >
      </cs-select-search>
    </div>`,
  methods: {
    onSelect: action('select'),
    onInput: action('input'),
    onQuery: action('query'),
  },
});

export const SelectSearchGroup = () => ({
  components: { CsSelectSearch },
  data() {
    return {
      val: 'value2.2',
    };
  },
  props: {
    placeholder: {
      default: text('placeholder', 'Search'),
    },
    optionGroups: {
      default: object('option-groups', selectOptionGroups),
    },
    // searchProperties: {
    //   default: array('search-properties', ['value', 'label']),
    // },
    customSearch: {
      default: boolean('custom-search', false),
    },
    noResultsLabel: {
      default: text('no-results-label'),
    },
    labelKey: {
      default: text('label-key'),
    },
    valueKey: {
      default: text('value-key'),
    },
    groupLabelKey: {
      default: text('group-label-key', 'title'),
    },
    icon: {
      default: text('icon'),
    },
  },
  template: `
  <div>
    <cs-select-search
      v-model="val"
      :placeholder="placeholder"
      :optionGroups="optionGroups"
      :label-key="labelKey"
      :value-key="valueKey"
      :group-label-key="groupLabelKey"
      :custom-search="customSearch"
      :no-results-label="noResultsLabel"
      :icon="icon"
			@select="onSelect"
			@input="onInput"
			@query="onQuery"
    >
    </cs-select-search>
      <div>Note: <code>value2.2</code> is already selected</div>
    </div>`,
  methods: {
    onSelect: action('select'),
    onInput: action('input'),
    onQuery: action('query'),
  },
});
