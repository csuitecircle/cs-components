import { withKnobs, object, text, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsSelect from '../components/Select.vue';

import { selectEvents } from '@/utils/formevents';
import formEventsMixin from './mixins/formevents';

export default {
  title: 'Form/Select',
  component: CsSelect,
  decorators: [
    withKnobs,
    () => '<div style="max-width: 500px;"><story/></div>',
  ],
};

const selectOptions = [
  {
    label: 'Option 1',
    value: 'value 1',
  },
  {
    label: 'Option 2',
    value: 'value 2',
  },
  {
    label:
      'A quick brown fox jumped over the lazy dog. A quick brown fox jumped over the lazy dog',
    value: 'value 3',
  },
];

export const Select = () => ({
  mixins: [formEventsMixin],
  components: { CsSelect },
  data() {
    return {
      $_formEventTriggers: selectEvents,
    };
  },
  props: {
    label: {
      default: text('label', 'Select Label'),
    },
    placeholder: {
      default: text('placeholder'),
    },
    icon: {
      default: text('icon'),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    required: {
      default: boolean('required', false),
    },
    options: {
      default: object('options', selectOptions),
    },
    selectedValue: {
      default: text('value', 'value 3'),
    },
    requiredLabel: {
      default: text('requiredLabel', '* Required'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
  },
  template: `
    <div> 
      <cs-select
        v-model="selectedValue"
        :options="options"
        :label="label"
        :placeholder="placeholder"
        :icon="icon"
        :disabled="disabled"
        :required="required"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        @select="onSelect"
        @validate="onValidate"
        v-on="$_formEvents"
      />
      <cs-select
        v-model="selectedValue"
        :options="options"
        :label="label"
        :placeholder="placeholder"
        :icon="icon"
        :disabled="disabled"
        :required="required"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        :success="true"
        @select="onSelect"
        @validate="onValidate"
      >
        <cs-input-note slot="input-notes" type="success" label="This is a success message"/>
      </cs-select>
      <cs-select
        v-model="selectedValue"
        :options="options"
        :label="label"
        :placeholder="placeholder"
        :icon="icon"
        :disabled="disabled"
        :required="required"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        :invalid="true"
        @select="onSelect"
        @validate="onValidate"
      >
        <cs-input-note slot="input-notes" type="info" label="This is a default message"/>
        <cs-input-note slot="input-notes" type="error" label="This is an error message"/>
      </cs-select>
    </div>

    `,
  methods: {
    onSelect: action('select'),
    onValidate: action('validate'),
  },
});
