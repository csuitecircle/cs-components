import { withKnobs, object, text, boolean, select } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsFilterDropdown from '../components/FilterDropdown.vue';

export default {
  title: 'Filter Dropdown',
  component: CsFilterDropdown,
  decorators: [withKnobs, () => '<div style="max-width: 200px;"><story/></div>'],
};

const selectOptionsWithObjects = [
  { label: 'Today', value: 'today' },
  { label: 'This Weekend', value: 'weekend' },
  { label: 'This Month', value: 'month' },
];
const selectOptions = selectOptionsWithObjects.map((option) => option.label);
const selectOptionsWithCustomObjects = selectOptionsWithObjects.map((option) => {
  return { title: option.label, id: option.value };
});
const selectObjectValues = [''].concat(selectOptionsWithObjects.map((option) => option.value));

export const FilterDropdownWithArrayOfStrings = () => ({
  components: { CsFilterDropdown },
  props: {
    placeholder: {
      default: text('placeholder'),
    },
    value: {
      default: select('value', [''].concat(selectOptions)),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    options: {
      default: object('options', selectOptions),
    },
    icon: {
      default: text('icon'),
    },
  },
  template: `
    <cs-filter-dropdown
      :value="value"
      :placeholder="placeholder"
      :options="options"
      :disabled="disabled"
      :icon="icon"
      @input="onInput"
      @select="onSelect"
    />
    `,
  methods: {
    onInput: action('input'),
    onSelect: action('select'),
  },
});

export const FilterDropdownWithArrayOfObjects = () => ({
  components: { CsFilterDropdown },
  props: {
    placeholder: {
      default: text('placeholder'),
    },
    value: {
      default: select('value', selectObjectValues),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    options: {
      default: object('options', selectOptionsWithObjects),
    },
    icon: {
      default: text('icon'),
    },
  },
  template: `
    <cs-filter-dropdown
      :value="value"
      :placeholder="placeholder"
      :options="options"
      :disabled="disabled"
      :icon="icon"
      @input="onInput"
      @select="onSelect"
    />
    `,
  methods: {
    onInput: action('input'),
    onSelect: action('select'),
  },
});

export const FilterDropdownWithArrayOfCustomObjects = () => ({
  components: { CsFilterDropdown },
  props: {
    placeholder: {
      default: text('placeholder'),
    },
    value: {
      default: select('value', selectObjectValues),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    options: {
      default: object('options', selectOptionsWithCustomObjects),
    },
    labelKey: {
      default: text('label-key', 'title'),
    },
    valueKey: {
      default: text('value-key', 'id'),
    },
    icon: {
      default: text('icon'),
    },
  },
  template: `
    <cs-filter-dropdown
      :value="value"
      :placeholder="placeholder"
      :options="options"
      :disabled="disabled"
      :icon="icon"
      :label-key="labelKey"
      :value-key="valueKey"
      @input="onInput"
      @select="onSelect"
    />
    `,
  methods: {
    onInput: action('input'),
    onSelect: action('select'),
  },
});
