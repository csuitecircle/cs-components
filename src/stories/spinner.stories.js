import { withKnobs, select, color } from '@storybook/addon-knobs';
import CsSpinner from '../components/Spinner.vue';

const nameOptions = {
  dots: 'dots',
  circles: 'circles',
};

export default {
  component: CsSpinner,
  title: 'Spinner',
  decorators: [withKnobs],
};

export const Spinner = () => ({
  components: { CsSpinner },
  props: {
    type: {
      default: select('type', nameOptions, 'dots'),
    },
    color: {
      default: color('--cs-spinner-color'),
    },
  },
  template: `
    <div>
      <h3>Hello world</h3>
      <p>I hope you like this while we wait for more content</p>
      <cs-spinner :type='type' :style="{'--cs-spinner-color': color}"></cs-spinner>
    </div>
    `,
});
