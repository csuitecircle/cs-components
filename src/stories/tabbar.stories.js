import { withKnobs, number, object } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsTabBar from '../components/TabBar.vue';
import CsTab from '../components/Tab.vue';
import CsBadge from '../components/Badge.vue';
import CsLabel from '../components/Label.vue';

export default {
  title: 'TabBar',
  component: CsTabBar,
  decorators: [withKnobs],
};

const tabs = [
  {
    label: 'Tweet',
    icon: 'cs-icons-twitter-filled',
    route: 'tabbar--default',
  },
  {
    label: 'Maps',
    icon: 'cs-icons-marker-filled',
    route: 'tabbar--sub-components',
    badge: '3',
    activeColor: 'var(--cs-danger-base)',
    action: () => alert('Maps'),
  },
];

export const Default = () => ({
  components: { CsTabBar },
  props: {
    tabs: {
      default: object('tabs', tabs),
    },
    value: {
      default: number('value', 1),
    },
  },
  template: `
    <cs-tab-bar :tabs="tabs" :value="value" @action="action" @input="input"/>
  `,
  methods: {
    action: action('action'),
    input: action('input'),
  },
});

export const SubComponents = () => ({
  components: {
    CsTabBar,
    CsTab,
    CsLabel,
    CsBadge,
  },
  props: {
    value: {
      default: number('value', 1),
    },
  },
  template: `
    <cs-tab-bar @input="input" :value="value">
      <cs-tab icon="cs-icons-twitter-filled" label="Tweet" badge="8"></cs-tab>
      <cs-tab icon="cs-icons-marker-filled" label="Maps"></cs-tab>
      <cs-tab active-color="var(--cs-danger-base)" route="hello/world">
        <i class="cs-icons-info" />
        <cs-label>Info</cs-label>
        <cs-badge>999+</cs-badge>
      </cs-tab>
      <cs-tab active-color="#00F">
        <cs-badge>999+</cs-badge>
        <i class="cs-icons-google-plus-filled" />
      </cs-tab>
    </cs-tab-bar>`,
  methods: {
    input: action('input'),
  },
});
