import { action } from '@storybook/addon-actions';
import { withKnobs } from '@storybook/addon-knobs';

import CsSlides from '../components/Slides.vue';
import CsSlide from '../components/Slide.vue';

export default {
  component: CsSlide,
  title: 'Slides/Slide',
  decorators: [withKnobs],
};

export const Slide = () => ({
  components: { CsSlides, CsSlide },
  props: {},
  template: `
    <cs-slides>
			<cs-slide>
				<cs-slide>Slide 1</cs-slide>
			</cs-slide>
			<cs-slide>
				<cs-slide>Slide 2</cs-slide>
			</cs-slide>
			<cs-slide>Slide 3</cs-slide>
    </cs-slides>
  `,
});
