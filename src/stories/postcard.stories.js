import { action } from '@storybook/addon-actions';
import { withKnobs, text, number, date, boolean, object, select, array } from '@storybook/addon-knobs';
import CsPostCard from '../components/PostCard.vue';
import CsVideoPlayer from '../components/VideoPlayer.vue';
import CsLinkPreview from '../components/LinkPreview.vue';
import CsActionBar from '../components/ActionBar.vue';
import CsActionBarButton from '../components/ActionBarButton.vue';
import CsComment from '../components/Comment.vue';
import CsMessageBox from '../components/MessageBox.vue';

export default {
  component: CsPostCard,
  title: 'Cards/Post Card',
  decorators: [withKnobs],
};

const author = {
  name: 'Monica Geller',
  picture: 'https://picsum.photos/300',
};
const actionButtons = [
  {
    label: 'Like',
    icon: 'cs-icons-like-filled',
    enableToggle: true,
    highlight: false, 
    action: () => { console.log('like')}
  },
  {
    label: 'Share',
    icon: 'cs-icons-share-filled',
    enableToggle: false,
    highlight: false, 
    action: () => { console.log('Share')}
  },
  {
    label: 'Comment',
    icon: 'cs-icons-comment-filled',
    enableToggle: false,
    highlight: false, 
    action: () => { console.log('Comment')}
  },
];

export const PostCardWithText = () => ({
  components: { CsPostCard, CsActionBar, CsActionBarButton },
  props: {
    author: {
      default: object('author', author),
    },
    authorName: {
      default: text('author-name', author.name),
    },
    authorPicture: {
      default: text('author-picture', author.picture),
    },
    date: {
      default: date('date', new Date()),
    },
    message: {
      default: text('message'),
    },
    counterLeft: {
      default: number('counter-left', 1),
    },
    counterCenter: {
      default: number('counter-center'),
    },
    counterRight: {
      default: number('counter-right', 2),
    },
    menuIcon: {
      default: text('menu-icon'),
    },
    showMenu: {
      default: boolean('show-menu', true),
    },
    counterLeftLabel: {
      default: text('counter-left-label'),
    },
    counterLeftLabelSingular: {
      default: text('counter-left-label-singular'),
    },
    counterCenterLabel: {
      default: text('counter-center-label'),
    },
    counterCenterLabelSingular: {
      default: text('counter-center-label-singular'),
    },
    counterRightLabel: {
      default: text('counter-right-label'),
    },
    counterRightLabelSingular: {
      default: text('counter-right-label-singular'),
    },
    actionButtons: {
      default: object('actionButtons', actionButtons),
    },
  },
  template: `
    <cs-post-card
			@click="onClick"
			@author="onAuthor"
			@counter-left="onCounterLeft"
			@counter-center="onCounterCenter"
			@counter-right="onCounterRight"
      @open-menu="onOpen"
      @action-button="onActionButton"
      :author="author"
      :author-name="authorName"
      :authorPicture="authorPicture"
      :date="date"
      :message="message"
      :menu-icon="menuIcon"
      :show-menu="showMenu"
      :counter-left="counterLeft"
      :counter-center="counterCenter"
			:counter-right="counterRight"
			:counter-left-label="counterLeftLabel"
      :counter-left-label-singular="counterLeftLabelSingular"
      :counter-center-label="counterCenterLabel"
      :counter-center-label-singular="counterCenterLabelSingular"
			:counter-right-label="counterRightLabel"
      :counter-right-label-singular="counterRightLabelSingular"
      :action-buttons="actionButtons"
    >
      <div slot="message">Check your emails! Booking has gone out for Pathfinders, Groundbreakers and Rocksolid - see you on Sunday</div>
    </cs-post-card>
  `,
  methods: {
    onClick: action('click'),
    onAuthor: action('author'),
    onOpen: action('open-menu'),
    onCounterLeft: action('counter-left'),
    onCounterCenter: action('counter-center'),
    onCounterRight: action('counter-right'),
    onActionButton: action('action-button'),
  },
});

PostCardWithText.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const PostCardWithImage = () => ({
  components: { CsPostCard },
  props: {
    author: {
      default: object('author', author),
    },
    authorName: {
      default: text('author-name', author.name),
    },
    authorPicture: {
      default: text('author-picture', author.picture),
    },
    date: {
      default: date('date', new Date()),
    },
    message: {
      default: text('message'),
    },
    counterLeft: {
      default: number('counter-left', 1),
    },
    counterCenter: {
      default: number('counter-center'),
    },
    counterRight: {
      default: number('counter-right', 2),
    },
    menuIcon: {
      default: text('menu-icon', 'cs-icons-options-filled'),
    },
    showMenu: {
      default: boolean('show-menu', true),
    },
    counterLeftLabel: {
      default: text('counter-left-label', 'likes'),
    },
    counterLeftLabelSingular: {
      default: text('counter-left-label-singular', 'like'),
    },
    counterCenterLabel: {
      default: text('counter-center-label', 'shares'),
    },
    counterCenterLabelSingular: {
      default: text('counter-center-label-singular', 'share'),
    },
    counterRightLabel: {
      default: text('counter-right-label', 'comments'),
    },
    counterRightLabelSingular: {
      default: text('counter-right-label-singular', 'comment'),
    },
    actionButtons: {
      default: object('actionButtons', actionButtons),
    },
  },
  template: `
    <cs-post-card
			@click="onClick"
			@author="onAuthor"
      @counter-left="onCounterLeft"
			@counter-center="onCounterCenter"
			@counter-right="onCounterRight"
      @open-menu="onOpen"
      @action-button="onActionButton"
      :author-name="authorName"
      :date="date"
      :authorPicture="authorPicture"
      :author="author"
      :message="message"
      :menu-icon="menuIcon"
      :show-menu="showMenu"
      :counter-left="counterLeft"
      :counter-center="counterCenter"
			:counter-right="counterRight"
			:counter-left-label="counterLeftLabel"
      :counter-left-label-singular="counterLeftLabelSingular"
      :counter-center-label="counterCenterLabel"
      :counter-center-label-singular="counterCenterLabelSingular"
			:counter-right-label="counterRightLabel"
      :counter-right-label-singular="counterRightLabelSingular"
      :action-buttons="actionButtons"
    >
      <div slot="message">
        IMPORTANT ANNOUNCEMENT! Booking for youth church 1 WEEK TODAY and football starting 11th October is now LIVE! Check your emails and drop us a message with any questions! We can’t wait to see you!
      </div>
      <img slot="media" src="https://picsum.photos/300/100" alt="image" />
    </cs-post-card>
  `,

  methods: {
    onClick: action('click'),
    onAuthor: action('author'),
    onOpen: action('open-menu'),
    onCounterLeft: action('counter-left'),
    onCounterCenter: action('counter-center'),
    onCounterRight: action('counter-right'),
    onActionButton: action('action-button'),
  },
});

PostCardWithImage.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const PostCardWithVideo = () => ({
  components: { CsPostCard, CsVideoPlayer },
  props: {
    author: {
      default: object('author', author),
    },
    authorName: {
      default: text('author-name', author.name),
    },
    authorPicture: {
      default: text('author-picture', author.picture),
    },
    date: {
      default: date('date', new Date()),
    },
    message: {
      default: text('message'),
    },
    counterLeft: {
      default: number('counter-left', 1),
    },
    counterCenter: {
      default: number('counter-center', 3),
    },
    counterRight: {
      default: number('counter-right', 2),
    },
    menuIcon: {
      default: text('menu-icon', 'cs-icons-options-filled'),
    },
    showMenu: {
      default: boolean('show-menu', true),
    },
    counterLeftLabel: {
      default: text('counter-left-label', 'likes'),
    },
    counterLeftLabelSingular: {
      default: text('counter-left-label-singular', 'like'),
    },
    counterCenterLabel: {
      default: text('counter-center-label', 'shares'),
    },
    counterCenterLabelSingular: {
      default: text('counter-center-label-singular', 'share'),
    },
    counterRightLabel: {
      default: text('counter-right-label', 'comments'),
    },
    counterRightLabelSingular: {
      default: text('counter-right-label-singular', 'comment'),
    },
    actionButtons: {
      default: object('actionButtons', actionButtons),
    },
  },
  template: `
    <div style="margin-top:10px;">
      <cs-post-card
				@click="onClick"
				@author="onAuthor"
        @counter-left="onCounterLeft"
        @counter-center="onCounterCenter"
        @counter-right="onCounterRight"
        @open-menu="onOpen"
        @action-button="onActionButton"
        :author="author"
        :author-name="authorName"
        :author-picture="authorPicture"
        :date="date"
        :message="message"
        :show-menu="showMenu"
        :counter-left="counterLeft"
        :counter-center="counterCenter"
        :counter-right="counterRight"
        :counter-left-label="counterLeftLabel"
        :counter-left-label-singular="counterLeftLabelSingular"
        :counter-center-label="counterCenterLabel"
        :counter-center-label-singular="counterCenterLabelSingular"
        :counter-right-label="counterRightLabel"
        :counter-right-label-singular="counterRightLabelSingular"
        :action-buttons="actionButtons"
      >
        <div slot="message">
          IMPORTANT ANNOUNCEMENT! Booking for youth church 1 WEEK TODAY and football starting 11th October is now LIVE! Check your emails and drop us a message with any questions! We can’t wait to see you!
        </div>
        <cs-video-player
					slot="media"
          :picture="'https://picsum.photos/300'"
          :src="'https://cdn.graphexpress.com/15600fe9-713d-4ba1-a1cd-4a2d2c916b5c/8d9c1b55-7be8-4259-9ce8-b40ee95c3dc8'">
        </cs-video-player>
      </cs-post-card>
    </div>
  `,
  methods: {
    onClick: action('click'),
    onAuthor: action('author'),
    onOpen: action('open-menu'),
    onCounterLeft: action('counter-left'),
    onCounterCenter: action('counter-center'),
    onCounterRight: action('counter-right'),
    onActionButton: action('action-button'),
  },
});

PostCardWithVideo.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const PostCardWithLinkPreview = () => ({
  components: { CsPostCard, CsLinkPreview },
  props: {
    author: {
      default: object('author', author),
    },
    authorName: {
      default: text('author-name', author.name),
    },
    authorPicture: {
      default: text('author-picture', author.picture),
    },
    date: {
      default: date('date', new Date()),
    },
    message: {
      default: text('message'),
    },
    counterLeft: {
      default: number('counter-left', 1),
    },
    counterCenter: {
      default: number('counter-center'),
    },
    counterRight: {
      default: number('counter-right', 2),
    },
    menuIcon: {
      default: text('menu-icon', 'cs-icons-options-filled'),
    },
    showMenu: {
      default: boolean('show-menu', true),
    },
    counterLeftLabel: {
      default: text('counter-left-label', 'likes'),
    },
    counterLeftLabelSingular: {
      default: text('counter-left-label-singular', 'like'),
    },
    counterCenterLabel: {
      default: text('counter-center-label', 'shares'),
    },
    counterCenterLabelSingular: {
      default: text('counter-center-label-singular', 'share'),
    },
    counterRightLabel: {
      default: text('counter-right-label', 'comments'),
    },
    counterRightLabelSingular: {
      default: text('counter-right-label-singular', 'comment'),
    },
    actionButtons: {
      default: object('actionButtons', actionButtons),
    },
  },
  template: `
		<cs-post-card
			@click="onClick"
			@author="onAuthor"
			@open-menu="onOpen"
      @action-button="onActionButton"
			:author="author"
			:author-name="authorName"
			:author-picture="authorPicture"
			:date="date"
      :message="message"
      :menu-icon="menuIcon"
			:show-menu="showMenu"
      :counter-left="counterLeft"
      :counter-center="counterCenter"
			:counter-right="counterRight"
			:counter-left-label="counterLeftLabel"
      :counter-left-label-singular="counterLeftLabelSingular"
      :counter-center-label="counterCenterLabel"
      :counter-center-label-singular="counterCenterLabelSingular"
			:counter-right-label="counterRightLabel"
      :counter-right-label-singular="counterRightLabelSingular"
      :action-buttons="actionButtons"
		>
			<div slot="message">
				IMPORTANT ANNOUNCEMENT! Booking for youth church 1 WEEK TODAY and football starting 11th October is now LIVE! Check your emails and drop us a message with any questions! We can’t wait to see you!  <b>Note: LinkPreview is in the message slot</b>
			</div>
			<cs-link-preview
				slot="message"
				title="Welcome to Group!  This is the place to get started"
				src="https://storybook.csuitegroup.com/?path=/story/pagetabs--with-icons"
        hide-label
				picture="https://picsum.photos/100"
				description="IMPORTANT ANNOUNCEMENT! Booking for youth church 1 WEEK TODAY and football starting 11th October is now LIVE!... "
			/>
		</cs-post-card>
  `,
  methods: {
    onClick: action('click'),
    onAuthor: action('author'),
    onOpen: action('open-menu'),
    onActionButton: action('action-button'),
  },
});

PostCardWithLinkPreview.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const PostCardActionBarSlot = () => ({
  components: { CsPostCard, CsActionBar, CsActionBarButton },
  props: {
    author: {
      default: object('author', author),
    },
    authorName: {
      default: text('author-name', author.name),
    },
    authorPicture: {
      default: text('author-picture', author.picture),
    },
    date: {
      default: date('date', new Date()),
    },
    message: {
      default: text('message'),
    },
    counterLeft: {
      default: number('counter-left', 1),
    },
    counterCenter: {
      default: number('counter-center'),
    },
    counterRight: {
      default: number('counter-right', 2),
    },
    menuIcon: {
      default: text('menu-icon', 'cs-icons-options-filled'),
    },
    showMenu: {
      default: boolean('show-menu', true),
    },
    counterLeftLabel: {
      default: text('counter-left-label', 'likes'),
    },
    counterLeftLabelSingular: {
      default: text('counter-left-label-singular', 'like'),
    },
    counterCenterLabel: {
      default: text('counter-center-label', 'shares'),
    },
    counterCenterLabelSingular: {
      default: text('counter-center-label-singular', 'share'),
    },
    counterRightLabel: {
      default: text('counter-right-label', 'comments'),
    },
    counterRightLabelSingular: {
      default: text('counter-right-label-singular', 'comment'),
    },
  },
  template: `
		<cs-post-card
      @click="onClick"
      @author="onAuthor"
      @open-menu="onOpen"
      :author="author"
      :author-name="authorName"
      :author-picture="authorPicture"
      :date="date"
      :message="message"
      :menu-icon="menuIcon"
      :show-menu="showMenu"
      :counter-left="counterLeft"
      :counter-center="counterCenter"
			:counter-right="counterRight"
			:counter-left-label="counterLeftLabel"
      :counter-left-label-singular="counterLeftLabelSingular"
      :counter-center-label="counterCenterLabel"
      :counter-center-label-singular="counterCenterLabelSingular"
			:counter-right-label="counterRightLabel"
      :counter-right-label-singular="counterRightLabelSingular"
		>
			<div slot="message">
				IMPORTANT ANNOUNCEMENT! Booking for youth church 1 WEEK TODAY and football starting 11th October is now LIVE! Check your emails and drop us a message with any questions! We can’t wait to see you!
			</div>
      <cs-action-bar slot="action-bar">
        <cs-action-bar-button icon="cs-icons-twitter-filled" :highlighted="true" @click="onTweet">
          Tweet
        </cs-action-bar-button>
        <cs-action-bar-button icon="cs-icons-email-filled" @click="onEmail">
          Email
        </cs-action-bar-button>
        <cs-action-bar-button icon="cs-icons-phone-filled" @click="onCall">
          Call
        </cs-action-bar-button>
      </cs-action-bar>
		</cs-post-card>
  `,
  methods: {
    onClick: action('click'),
    onAuthor: action('author'),
    onOpen: action('open-menu'),
    onTweet: action('tweet'),
    onEmail: action('email'),
    onCall: action('call'),
  },
});

export const PostCardCommentsSlot = () => ({
  components: { CsPostCard, CsComment, CsMessageBox },
  data() {
    return {
      messageTwo: 'Hello world',
    };
  },
  props: {
    author: {
      default: object('author', author),
    },
    date: {
      default: date('date', new Date()),
    },
    message: {
      default: text('message', 'The quick brown fox jumped over the lazy dog'),
    },
    counterRight: {
      default: number('counter-right', 2),
    },
    actionButtons: {
      default: object('actionButtons', actionButtons),
    },
  },
  template: `
		<cs-post-card
			@click="onClick"
			@author="onAuthor"
      @action-button="onActionButton"
			:author="author"
			:date="date"
      :message="message"
			:counter-right="counterRight"
      :action-buttons="actionButtons"
		>
      <div slot="comments">
        <cs-comment :author="author" :date="date" :message="message" enable-likes enable-replies/>
        <cs-comment :author="author" :date="date" :message="messageTwo" enable-likes enable-replies/>
      </div>
      <div slot="message-box">
        <cs-message-box v-model="messageTwo" show-border variant="secondary">
          <i slot="attachment-icon" class="cs-icons-twitter"/>
          <i slot="attachment-icon" class="cs-icons-email"/>
        </cs-message-box>
      </div>
    </cs-post-card>
  `,
  methods: {
    onClick: action('click'),
    onAuthor: action('author'),
    onOpen: action('open-menu'),
    onActionButton: action('action-button'),
  },
});

export const PostCardReactionSlot = () => ({
  components: { CsPostCard },
  props: {
    author: {
      default: object('author', author),
    },
    date: {
      default: date('date', new Date()),
    },
    message: {
      default: text('message'),
    },
    countsSpacing: {
      default: select('counts-spacing', ['even', 'split'], 'split'),
    },
    counterLeft: {
      default: number('counter-left', 1),
    },
    counterCenter: {
      default: number('counter-center'),
    },
    counterRight: {
      default: number('counter-right', 2),
    },
    showMenu: {
      default: boolean('show-menu', true),
    },
    counterLeftLabel: {
      default: text('counter-left-label', 'likes'),
    },
    counterLeftLabelSingular: {
      default: text('counter-left-label-singular', 'like'),
    },
    counterCenterLabel: {
      default: text('counter-center-label', 'shares'),
    },
    counterCenterLabelSingular: {
      default: text('counter-center-label-singular', 'share'),
    },
    counterRightLabel: {
      default: text('counter-right-label', 'comments'),
    },
    counterRightLabelSingular: {
      default: text('counter-right-label-singular', 'comment'),
    },
    actionButtons: {
      default: object('actionButtons', actionButtons),
    },
  },
  template: `
		<cs-post-card
			@click="onClick"
			@author="onAuthor"
			@counter-left="onCounterLeft"
      @counter-center="onCounterCenter"
      @counter-right="onCounterRight"
      @action-button="onActionButton"
			:author="author"
			:date="date"
      :message="message"
      :countsSpacing="countsSpacing"
      :counter-left="counterLeft"
      :counter-center="counterCenter"
			:counter-right="counterRight"
			:counter-left-label="counterLeftLabel"
      :counter-left-label-singular="counterLeftLabelSingular"
      :counter-center-label="counterCenterLabel"
      :counter-center-label-singular="counterCenterLabelSingular"
			:counter-right-label="counterRightLabel"
      :counter-right-label-singular="counterRightLabelSingular"
      :action-buttons="actionButtons"
		>
			<div slot="message">
				IMPORTANT ANNOUNCEMENT! Booking for youth church 1 WEEK TODAY and football starting 11th October is now LIVE! Check your emails and drop us a message with any questions! We can’t wait to see you!
			</div>
      <span slot="reactions">
        <i class="cs-icons-smile"/>
        <i class="cs-icons-like"/>
        <i class="cs-icons-twitter"/>
      </span>
		</cs-post-card>
  `,
  methods: {
    onClick: action('click'),
    onAuthor: action('author'),
    onOpen: action('open-menu'),
    onCounterLeft: action('counter-left'),
    onCounterCenter: action('counter-center'),
    onCounterRight: action('counter-right'),
    onActionButton: action('action-button'),
  },
});

export const PostCardList = () => ({
  components: { CsPostCard },
  props: {
    display: {
      default: select('display', ['columns', 'block'], 'block'),
    },
  },
  template: `
    <div class="post-list" :style="{
        columns: display == 'block' ? 'initial' : 2
      }">
      <cs-post-card
              author-name="John Doe"
              author-picture="https://picsum.photos/100"
              message="Check your emails! Booking has gone out for Pathfinders, Groundbreakers and Rocksolid - see you on Sunday."
              :counter-left=30
              :counter-right=10
              :style="{
                display: display == 'columns' ? 'inline-block' : initial
              }"
              >
      </cs-post-card>
      <cs-post-card
              author-name="Jane Doe"
              author-picture="https://picsum.photos/100"
              message="Check your emails! Booking has gone out for Pathfinders, Groundbreakers and Rocksolid - see you on Sunday."
              :counter-left=30
              :counter-right=10
              :style="{
                display: display == 'columns' ? 'inline-block' : initial
              }"
              >
        <img slot="media" src="https://picsum.photos/300/100" alt="image" />
      </cs-post-card>
      <cs-post-card
              author-name="Robert Smith"
              author-picture="https://picsum.photos/100"
              message="Check your emails! Booking has gone out for Pathfinders, Groundbreakers and Rocksolid - see you on Sunday."
              :counter-left=30
              :counter-right=10
              :style="{
                display: display == 'columns' ? 'inline-block' : initial
              }"
              >
      </cs-post-card>
      <cs-post-card
              author-name="Jane Doe"
              author-picture="https://picsum.photos/100"
              message="Check your emails! Booking has gone out for Pathfinders, Groundbreakers and Rocksolid - see you on Sunday."
              :counter-left=30
              :counter-right=10
              :style="{
                display: display == 'columns' ? 'inline-block' : initial
              }"
              >
        <img slot="media" src="https://picsum.photos/300/100" alt="image" />
      </cs-post-card>
      <cs-post-card
              author-name="Jane Doe"
              author-picture="https://picsum.photos/100"
              message="Check your emails! Booking has gone out for Pathfinders, Groundbreakers and Rocksolid - see you on Sunday."
              :counter-left=30
              :counter-right=10
              :style="{
                display: display == 'columns' ? 'inline-block' : initial
              }"
              >
        <img slot="media" src="https://picsum.photos/300/100" alt="image" />
      </cs-post-card>
    </div>
      
  `,

  methods: {
    onClick: action('click'),
    onAuthor: action('author'),
    onOpen: action('open-menu'),
    onCounterLeft: action('counter-left'),
    onCounterCenter: action('counter-center'),
    onCounterRight: action('counter-right'),
    onActionButton: action('action-button'),
  },
});

PostCardList.parameters = {
  viewport: {
    defaultViewport: 'tablet',
  },
};
