import { withKnobs, text, select, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsDateInput from '../components/DateInput.vue';

import { datetimeEvents } from '@/utils/formevents';
import formEventsMixin from './mixins/formevents';

export default {
  title: 'Form/Date Input',
  component: CsDateInput,
  decorators: [withKnobs],
};

export const DateInput = () => ({
  mixins: [formEventsMixin],
  components: { CsDateInput },
  props: {
    label: {
      default: text('label', 'Start Date'),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    required: {
      default: boolean('required', false),
    },
    icon: {
      default: text('icon'),
    },
    value: {
      default: text('value', new Date().toDateString()),
    },
    requiredLabel: {
      default: text('requiredLabel', '* Required'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
  },
  data() {
    return {
      val: new Date(),
      $_formEventTriggers: datetimeEvents,
    };
  },
  template: `
    <div>
      <cs-date-input
        :v-model="value"
        :label="label"
        :disabled="disabled"
        :required="required"
        :icon="icon"
        :requiredLabel="requiredLabel"
        :error-icon="errorIcon"
        @input="onInput"
        @validate="onValidate"
        v-on="$_formEvents"
      />

      <cs-date-input
        :v-model="value"
        :label="label"
        :disabled="disabled"
        :required="required"
        :icon="icon"
        :requiredLabel="requiredLabel"
        :error-icon="errorIcon"
        :success="true"
        @input="onInput"
        @validate="onValidate"
      >
        <cs-input-note slot="input-notes" v-if="true" type="success" label="This is a success message"/>
      </cs-date-input>

      <cs-date-input
        :v-model="value"
        :label="label"
        :disabled="disabled"
        :required="required"
        :icon="icon"
        :requiredLabel="requiredLabel"
        :error-icon="errorIcon"
        :invalid="true"
        @input="onInput"
        @validate="onValidate"
      >
      <cs-input-note slot="input-notes" v-if="value !== null" type="info" label="This is a default message"/>
      <cs-input-note slot="input-notes" v-if="value !== null" type="error" label="This is an error message"/>
      </cs-date-input>
    </div>
    `,
  methods: {
    onInput: action('input'),
    onValidate: action('validate'),
  },
});
