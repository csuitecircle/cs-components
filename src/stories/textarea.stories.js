import { withKnobs, text, boolean, select } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import CsTextarea from '../components/Textarea.vue';

import { textareaEvents } from '@/utils/formevents';
import formEventsMixin from './mixins/formevents';

export default {
  title: 'Form/Textarea',
  component: CsTextarea,
  decorators: [withKnobs],
};

const sizeOptions = ['medium', 'large'];

export const Textarea = () => ({
  mixins: [formEventsMixin],
  components: { CsTextarea },
  data() {
    return {
      val: '',
      $_formEventTriggers: textareaEvents,
    };
  },
  props: {
    label: {
      default: text('label', 'Text Area Label'),
    },
    placeholder: {
      default: text('placeholder', 'Placeholder'),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    required: {
      default: boolean('required', false),
    },
    size: {
      default: select('size', sizeOptions, 'medium'),
    },
    resize: {
      default: select(
        'resize',
        ['auto', 'both', 'horizontal', 'vertical', 'none'],
        'none'
      ),
    },
    value: {
      default: text('value'),
    },
    requiredLabel: {
      default: text('requiredLabel', '* Required'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
  },
  template: `
    <div>
      <cs-textarea
        v-model="value"
        :label="label"
        :disabled="disabled"
        :required="required"
        :placeholder="placeholder"
        :size="size"
        :resize="resize"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        @input="onInput"
        @validate="onValidate"
        v-on="$_formEvents"
      />
      <cs-textarea
      v-model="value"
      :label="label"
      :disabled="disabled"
      :required="required"
      :placeholder="placeholder"
      :size="size"
      :resize="resize"
      :required-label="requiredLabel"
      :error-icon="errorIcon"
      :success="true"
      >
        <cs-input-note slot="input-notes" v-if="true" type="success" label="This is a success message"/>
      </cs-textarea>
      <cs-textarea
      v-model="value"
      :label="label"
      :disabled="disabled"
      :required="required"
      :placeholder="placeholder"
      :size="size"
      :resize="resize"
      :required-label="requiredLabel"
      :error-icon="errorIcon"
      :invalid="true"
      >
        <cs-input-note slot="input-notes" v-if="value !== null" type="info" label="This is a default message"/>
        <cs-input-note slot="input-notes" v-if="value !== null" type="error" label="This is an error message"/>
      </cs-textarea>
    </div>`,
  methods: {
    onInput: action('input'),
    onValidate: action('validate'),
  },
});
