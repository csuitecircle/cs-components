import {
  withKnobs,
  text,
  select,
  color,
  boolean,
} from '@storybook/addon-knobs';
import CsMainHeader from '../components/MainHeader.vue';

export default {
  component: CsMainHeader,
  title: 'Headers/Main Header',
  decorators: [withKnobs],
};

export const MainHeader = () => ({
  components: { CsMainHeader },
  props: {
    title: {
      default: text('title', 'Header'),
    },
    alignTitle: {
      default: select('align-title', ['center', 'left', 'right'], 'center'),
    },
    backgroundColor: {
      default: color('--cs-main-header-background-color'),
    },
    textColor: {
      default: color('--cs-main-header-text-color'),
    },
  },
  template: `
    <cs-main-header
      :title="title"
      :align-title="alignTitle"
      :style="{
        '--cs-main-header-background-color': backgroundColor,
        '--cs-main-header-text-color': textColor,
      }"
    >
      <i slot="buttons-left" class="cs-icons-search" />
      <i slot="buttons-right" class="cs-icons-notification" />
    </cs-main-header>
    `,
});
MainHeader.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const MainHeaderIconLeft = () => ({
  components: { CsMainHeader },
  props: {
    title: {
      default: text('title', 'Header'),
    },
    alignTitle: {
      default: select('align-title', ['center', 'left', 'right'], 'center'),
    },
    backgroundColor: {
      default: color('--cs-main-header-background-color'),
    },
    textColor: {
      default: color('--cs-main-header-text-color'),
    },
  },
  template: `
    <cs-main-header
      :title="title"
      :align-title="alignTitle"
      :style="{
        '--cs-main-header-background-color': backgroundColor,
        '--cs-main-header-text-color': textColor,
      }"
    >
      <i slot="buttons-left" class="cs-icons-search" />
    </cs-main-header>
    `,
});
MainHeaderIconLeft.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const MainHeaderIconRight = () => ({
  components: { CsMainHeader },
  props: {
    title: {
      default: text('title', 'Header'),
    },
    alignTitle: {
      default: select('align-title', ['center', 'left', 'right'], 'center'),
    },
    backgroundColor: {
      default: color('--cs-main-header-background-color'),
    },
    textColor: {
      default: color('--cs-main-header-text-color'),
    },
  },
  template: `
    <cs-main-header
      :title="title"
      :align-title="alignTitle"
      :style="{
        '--cs-main-header-background-color': backgroundColor,
        '--cs-main-header-text-color': textColor,
      }"
    >
    <i slot="buttons-right" class="cs-icons-notification" />
    </cs-main-header>
    `,
});
MainHeaderIconRight.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const MainHeaderNoIcon = () => ({
  components: { CsMainHeader },
  props: {
    title: {
      default: text('title', 'Header'),
    },
    alignTitle: {
      default: select('align-title', ['center', 'left', 'right'], 'center'),
    },
    backgroundColor: {
      default: color('--cs-main-header-background-color'),
    },
    textColor: {
      default: color('--cs-main-header-text-color'),
    },
  },
  template: `
    <cs-main-header
      :title="title"
      :align-title="alignTitle"
      :style="{
        '--cs-main-header-background-color': backgroundColor,
        '--cs-main-header-text-color': textColor,
      }"
    >
    </cs-main-header>
    `,
});
MainHeaderNoIcon.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const MainHeaderTwoIcons = () => ({
  components: { CsMainHeader },
  props: {
    title: {
      default: text('title', 'Header'),
    },
    alignTitle: {
      default: select('align-title', ['center', 'left', 'right'], 'center'),
    },
    backgroundColor: {
      default: color('--cs-main-header-background-color'),
    },
    textColor: {
      default: color('--cs-main-header-text-color'),
    },
  },
  template: `
    <cs-main-header
      :title="title"
      :align-title="alignTitle"
      :style="{
        '--cs-main-header-background-color': backgroundColor,
        '--cs-main-header-text-color': textColor,
      }"
    >
      <i slot="buttons-right" class="cs-icons-search" />
      <i slot="buttons-right" class="cs-icons-notification" />
    </cs-main-header>
    `,
});
MainHeaderTwoIcons.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const MainHeaderButtons = () => ({
  components: { CsMainHeader },
  props: {
    title: {
      default: text('title', 'Header'),
    },
    alignTitle: {
      default: select('align-title', ['center', 'left', 'right'], 'center'),
    },
    backgroundColor: {
      default: color('--cs-main-header-background-color'),
    },
    textColor: {
      default: color('--cs-main-header-text-color'),
    },
    buttonLeft: {
      default: text('Left cs-button text', 'Cancel'),
    },
    buttonRight: {
      default: text('Right cs-button text', 'Send'),
    },
  },
  template: `
    <cs-main-header
      :title="title"
      :align-title="alignTitle"
      :style="{
        '--cs-main-header-background-color': backgroundColor,
        '--cs-main-header-text-color': textColor,
      }"
    >
      <cs-button slot="buttons-left" fill="clear">{{buttonLeft}}</cs-button>
      <cs-button slot="buttons-right" fill="clear">{{buttonRight}}</cs-button>
    </cs-main-header>
  `,
});
MainHeaderButtons.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};

export const MainHeaderDebugButtons = () => ({
  components: { CsMainHeader },
  props: {
    title: {
      default: text('title', 'Header'),
    },
    alignTitle: {
      default: select('align-title', ['center', 'left', 'right'], 'center'),
    },
    backgroundColor: {
      default: color('--cs-main-header-background-color'),
    },
    textColor: {
      default: color('--cs-main-header-text-color'),
    },
    buttonLeft: {
      default: text('Left cs-button text', 'Cancel'),
    },
    buttonRight: {
      default: text('Right cs-button text', 'Send'),
    },
  },
  template: `
    <div>
      <h4>Used to debug the alignment of buttons and title</h4>
      <cs-main-header
        :title="title"
        :align-title="alignTitle"
        :style="{
          '--cs-main-header-background-color': backgroundColor,
          '--cs-main-header-text-color': textColor,
        }"
      >
        <cs-button slot="buttons-left" fill="clear">{{buttonLeft}}</cs-button>
        <cs-button slot="buttons-right" fill="clear">{{buttonRight}}</cs-button>
      </cs-main-header>
      <br>
      <cs-main-header
        class="cs-main-header---debug"
        :title="title"
        :align-title="alignTitle"
        :style="{
          '--cs-main-header-background-color': backgroundColor,
          '--cs-main-header-text-color': textColor,
        }"
      >
        <cs-button slot="buttons-left" fill="clear">{{buttonLeft}}</cs-button>
        <cs-button slot="buttons-right" fill="clear">{{buttonRight}}</cs-button>
      </cs-main-header>
    </div>
  `,
});
MainHeaderDebugButtons.parameters = {
  viewport: {
    defaultViewport: 'mobile2',
  },
};
