import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';
import CsSearchBar from '../components/SearchBar.vue';

export default {
	component: CsSearchBar,
	title: 'SearchBar',
	decorators: [withKnobs],
};

export const SearchBar = () => ({
	data() {
		return {
			val: '',
		};
	},
	props: {
		placeholder: {
			default: text('placeholder-text', 'Search Products...'),
		},
		icon: {
			default: text('icon', 'cs-icons-search-filled'),
		},
	},
	components: { CsSearchBar },
	template: `
      <cs-search-bar
      v-model="val"
      :placeholder="placeholder"
      :icon="icon"
      @submit="search"
      @input="input"
    />
  `,
	methods: { search: action('Search'), input: action('Input') },
});
