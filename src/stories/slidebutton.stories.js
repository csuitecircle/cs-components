import { action } from '@storybook/addon-actions';
import { withKnobs, text, number, boolean, select, color } from '@storybook/addon-knobs';

import CsSlides from '../components/Slides.vue';
import CsSlide from '../components/Slide.vue';
import CsSlideButton from '../components/SlideButton.vue';

export default {
  component: CsSlides,
  title: 'Slides/SlideButton',
  decorators: [withKnobs],
};

export const SlideButton = () => ({
  components: { CsSlides, CsSlide, CsSlideButton },
  props: {},
  template: `
    <cs-slides>
			<cs-slide>
				<cs-slide-button @click="action">Button 1</cs-slide-button>
				<span>Accepts all <code>cs-button</code> attributes</span>
				<cs-slide-button @click="action" fill="clear" variant="danger" size="large">Button 2</cs-slide-button>
				<cs-slide-button @click="action" block fill="outline" variant="info" size="small">Button 3</cs-slide-button>
			</cs-slide>
			<cs-slide>
				<cs-slide-button @click="action">Button 1</cs-slide-button>
				<cs-slide-button @click="action">Button 2</cs-slide-button>
			</cs-slide>
			<cs-slide>
				<cs-slide-button @click="action">Button 3</cs-slide-button>
			</cs-slide>
    </cs-slides>
  `,
  methods: { action: action('click') },
});
