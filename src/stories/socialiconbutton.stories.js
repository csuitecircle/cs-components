import { action } from '@storybook/addon-actions';
import { withKnobs, text, select, color } from '@storybook/addon-knobs';

import CsSocialIconButton from '../components/SocialIconButton.vue';

export default {
  component: CsSocialIconButton,
  title: 'Social Icon Button',
  decorators: [withKnobs],
};

const networkOptions = ['facebook', 'linkedin', 'twitter', 'email', 'phone', 'google', 'instagram', 'youtube', 'network'];

export const Default = () => ({
  components: { CsSocialIconButton },
  props: {
    href: {
      default: text('href', ''),
    },
    network: {
      default: select('network', networkOptions, 'facebook'),
    },
    size: {
      default: select('size', ['small', 'medium', 'large'], 'medium'),
    },
    cssSize: {
      default: text('--cs-social-icon-button-size'),
    },
    cssColor: {
      default: color('--cs-social-icon-button-hover-color'),
    },
  },
  template: `
    <div>
      <cs-social-icon-button
        :href="href"
        :network="network"
        :size="size"
        :style="{
					'--cs-social-icon-button-size': cssSize,
					'--cs-social-icon-button-hover-color': cssColor,
        }"
      />
      <cs-social-icon-button
        :href="href"
        :network="network"
        :size="size"
        :style="{
					'--cs-social-icon-button-size': cssSize,
					'--cs-social-icon-button-hover-color': cssColor,
        }"
      />
      <cs-social-icon-button
        :href="href"
        :network="network"
        :size="size"
        :style="{
					'--cs-social-icon-button-size': cssSize,
					'--cs-social-icon-button-hover-color': cssColor,
        }"
      />
    </div>`,
});

export const CustomIcon = () => ({
  components: { CsSocialIconButton },
  props: {
    icon: {
      default: text('icon', 'cs-icons-twitter-filled'),
    },
    href: {
      default: text('href', ''),
    },
    network: {
      default: select('network', networkOptions, 'facebook'),
    },
    cssSize: {
      default: text('--cs-social-icon-button-size', '36px'),
    },
    cssColor: {
      default: color('--cs-social-icon-button-hover-color'),
    },
  },
  template: `
    <div>
      <cs-social-icon-button
        :icon="icon"
        :href="href"
        :network="network"
        :style="{
					'--cs-social-icon-button-size': cssSize,
					'--cs-social-icon-button-hover-color': cssColor,
        }"
      />
    </div>`,
});

export const SlotIcon = () => ({
  components: { CsSocialIconButton },
  props: {
    href: {
      default: text('href', ''),
    },
    network: {
      default: select('network', networkOptions, 'facebook'),
    },
    cssSize: {
      default: text('--cs-social-icon-button-size', '36px'),
    },
    cssColor: {
      default: color('--cs-social-icon-button-hover-color'),
    },
  },
  template: `
    <div>
      <cs-social-icon-button
        :href="href"
        :network="network"
        :style="{
					'--cs-social-icon-button-size': cssSize,
					'--cs-social-icon-button-hover-color': cssColor,
        }"
      >
        <i class="cs-icons-marker-filled"/>
      </cs-social-icon-button>
    </div>`,
});
