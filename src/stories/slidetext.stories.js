import { action } from '@storybook/addon-actions';
import { withKnobs, text, number, boolean, select, color } from '@storybook/addon-knobs';

import CsSlides from '../components/Slides.vue';
import CsSlide from '../components/Slide.vue';
import CsSlideText from '../components/SlideText.vue';

export default {
  component: CsSlides,
  title: 'Slides/SlideText',
  decorators: [withKnobs],
};

export const SlideText = () => ({
  components: { CsSlides, CsSlide, CsSlideText },
  props: {
    color: {
      default: color('--cs-slide-text-color'),
    },
  },
  template: `
    <cs-slides>
			<cs-slide>
				<span>Has default style class of <br/><code>cs-textstyle-informative-paragraph</code></span>
				<cs-slide-text @click="action" :style="{ '--cs-slide-text-color': color }">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</cs-slide-text>
				<cs-slide-text @click="action" :style="{ '--cs-slide-text-color': color }">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</cs-slide-text>
			</cs-slide>
			<cs-slide>
				<cs-slide-text @click="action" :style="{ '--cs-slide-text-color': color }">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</cs-slide-text>
			</cs-slide>
			<cs-slide>
				<cs-slide-text @click="action" :style="{ '--cs-slide-text-color': color }">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</cs-slide-text>
			</cs-slide>
    </cs-slides>
  `,
  methods: { action: action('click') },
});
