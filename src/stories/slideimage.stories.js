import { action } from '@storybook/addon-actions';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import CsSlides from '../components/Slides.vue';
import CsSlide from '../components/Slide.vue';
import CsSlideImage from '../components/SlideImage.vue';

export default {
  component: CsSlides,
  title: 'Slides/SlideImage',
  decorators: [withKnobs],
};

export const SlideImage = () => ({
  components: { CsSlides, CsSlide, CsSlideImage },
  props: {
    fill: {
      default: boolean('fill', false),
    },
  },
  template: `
    <cs-slides>
			<cs-slide>
				<span>Shows image at native size unless it is too wide.  If too wide, scales down to fit width.</span>
				<cs-slide-image @click="action" :fill="fill" src="https://picsum.photos/200" />
			</cs-slide>
			<cs-slide>
				<cs-slide-image @click="action" :fill="fill" src="https://picsum.photos/200" />
				<cs-slide-image @click="action" :fill="fill" src="https://picsum.photos/500" />
			</cs-slide>
			<cs-slide>
				<cs-slide-image @click="action" :fill="fill" src="https://picsum.photos/500" />
			</cs-slide>
    </cs-slides>
  `,
  methods: { action: action('click') },
});
