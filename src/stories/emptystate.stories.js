import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import CsEmptyState from '../components/EmptyState.vue';
import CsButton from '../components/Button.vue';

export default {
  title: 'EmptyState',
  component: CsEmptyState,
  decorators: [withKnobs],
};

export const EmptyState = () => ({
  components: { CsEmptyState, CsButton },
  props: {
    title: {
      default: text('title', 'You haven’t joined any groups yet.'),
    },
    description: {
      default: text('description', 'Find one near you now.'),
    },
    visible: {
      default: boolean('v-show', true),
    },
  },
  template: `
    <cs-empty-state
      v-show='visible'
      :title='title'
      :description='description'
    >
      <cs-button
				v-slot="button"
        variant="primary"
      >
				<i class="cs-icons-map"/>
				<span>Find Groups</span>
			</cs-button>
    </cs-empty-state>
    `,
});

export const EmptyStateSlots = () => ({
  components: { CsEmptyState, CsButton },
  props: {
    title: {
      default: text('title', 'You haven’t joined any groups yet.'),
    },
    description: {
      default: text('description', 'Find one near you now.'),
    },
    visible: {
      default: boolean('v-show', true),
    },
  },
  template: `
    <cs-empty-state
      v-show='visible'
    >
      <template v-slot:title>{{title}}</template>
      <template v-slot:description>{{description}}</template>
      <template  v-slot:button>
        <cs-button
          variant="primary"
          :style="{
              '--cs-button-color': '#BF0012',
              '--cs-button-color-hover': '#BF0012',
          }"
      	>
        	<i class="cs-icons-map"></i> <span>Find Groups</span>
				</cs-button>
      </template>
    </cs-empty-state>
    `,
});
