import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import CsNavigationPage from '../components/NavigationPage.vue';
import CsNavigationPageLink from '../components/NavigationPageLink.vue';
import CsProfile from '../components/Profile.vue';

export default {
  title: 'NavigationPage',
  component: CsNavigationPage,
  decorators: [withKnobs],
};

export const NavigationPage = () => ({
  components: { CsNavigationPage, CsProfile, CsNavigationPageLink },
  template: `
    <cs-navigation-page
    >
      <cs-profile
        slot="profile"
        picture="https://picsum.photos/300"
        background
        name="John Doe"
        location="Singapore"
        avatar-position="left"
        avatar-size="medium"
      ></cs-profile>
      <cs-navigation-page-link slot="link" title="Profile" icon="cs-icons-profile-filled"/>
      <cs-navigation-page-link slot="link" title="About Us" icon="cs-icons-info-filled" href="https://csuitecircle.com"/>
      <cs-navigation-page-link slot="link" title="Privacy Policy" icon="cs-icons-doc-filled" />
      <cs-navigation-page-link slot="link" title="Log out" icon="cs-icons-logout-filled" />
    </cs-navigation-page>
    `,
});
