import { action } from '@storybook/addon-actions';
import { withKnobs, text, number, boolean, select, color } from '@storybook/addon-knobs';

import CsSlides from '../components/Slides.vue';
import CsSlide from '../components/Slide.vue';
import CsSlideHeader from '../components/SlideHeader.vue';

export default {
  component: CsSlides,
  title: 'Slides/SlideHeader',
  decorators: [withKnobs],
};

export const SlideHeader = () => ({
  components: { CsSlides, CsSlide, CsSlideHeader },
  props: {
    color: {
      default: color('--cs-slide-header-color'),
    },
  },
  template: `
    <cs-slides>
			<cs-slide>
				<cs-slide-header @click="action" :style="{ '--cs-slide-header-color': color }">Slide 1</cs-slide-header>
				<span>Has default style class of <br/><code>cs-textstyle-page-sectiontitle</code></span>
			</cs-slide>
			<cs-slide>
				<cs-slide-header @click="action" :style="{ '--cs-slide-header-color': color }">Slide 2</cs-slide-header>
			</cs-slide>
			<cs-slide>
				<cs-slide-header @click="action" :style="{ '--cs-slide-header-color': color }">Slide 3</cs-slide-header>
			</cs-slide>
    </cs-slides>
  `,
  methods: { action: action('click') },
});
