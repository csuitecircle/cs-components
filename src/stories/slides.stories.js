import { action } from '@storybook/addon-actions';
import { withKnobs, text, number, boolean, select, color } from '@storybook/addon-knobs';

import CsSlides from '../components/Slides.vue';
import CsSlide from '../components/Slide.vue';
import CsSlideHeader from '../components/SlideHeader.vue';
import CsSlideText from '../components/SlideText.vue';
import CsSlideButton from '../components/SlideButton.vue';
import CsSlideImage from '../components/SlideImage.vue';

export default {
  component: CsSlides,
  title: 'Slides/Slides',
  decorators: [withKnobs],
};

export const Slides = () => ({
  components: {
    CsSlides,
    CsSlide,
    CsSlideHeader,
    CsSlideText,
    CsSlideButton,
    CsSlideImage,
  },
  props: {
		initialSlide: {
			default: number('initial-slide', 0),
		},
		speed: {
			default: number('speed', 300),
		},
		loop: {
			default: boolean('loop', false),
		},
		hideBackButton: {
			default: boolean('hide-back-button', false),
		},
		hideNextButton: {
			default: boolean('hide-next-button', false),
		},
		hideFinishButton: {
			default: boolean('hide-finish-button', false),
		},
		hideSkipButton: {
			default: boolean('hide-skip-button', false),
		},
		hidePagination: {
			default: boolean('hide-pagination', false),
		},
		width: {
			default: text('--cs-slides-width', '100%'),
		},
		height: {
			default: text('--cs-slides-height', '300px'),
		},
		background: {
			default: color('--cs-slides-background', '#FFFFFF'),
		},
		skipLabel: {
			default: text('skip-label')
		},
		nextLabel: {
			default: text('next-label')
		},
		backLabel: {
			default: text('back-label')
		},
		finishLabel: {
			default: text('finish-label')
		},
  },
  template: `
    <cs-slides
			:initial-slide="initialSlide"
			:speed="speed"
			:loop="loop"
			:hide-back-button="hideBackButton"
			:hide-next-button="hideNextButton"
			:hide-finish-button="hideFinishButton"
			:hide-skip-button="hideSkipButton"
			:hide-pagination="hidePagination"
			:skip-label="skipLabel"
			:next-label="nextLabel"
			:back-label="backLabel"
			:finish-label="finishLabel"
			:style="{
				'--cs-slides-width': width,
				'--cs-slides-height': height,
				'--cs-slides-background': background,
			}"
			@finish="finish"
			@skip="skip"
		>
			<cs-slide>
				<cs-slide-header>Slide 1</cs-slide-header>
				<cs-slide-text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</cs-slide-text>
				<cs-slide-button>Get Started</cs-slide-button>
				<cs-slide-button>Login</cs-slide-button>
				<cs-slide-text>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</cs-slide-text>
				<cs-slide-text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</cs-slide-text>
			</cs-slide>
			<cs-slide>
				<cs-slide-image src="https://picsum.photos/200" />
				<cs-slide-image src="https://picsum.photos/500" />
				<cs-slide-header>Slide 1</cs-slide-header>
				<cs-slide-text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</cs-slide-text>
			</cs-slide>
			<cs-slide>Slide3</cs-slide>
    </cs-slides>
  `,
  methods: {
    finish: action('finish'),
    skip: action('skip'),
  },
});
