import { withKnobs, text, boolean, number, select } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsFilePreview from '../components/FilePreview.vue';

export default {
  title: 'FilePreview',
  component: CsFilePreview,
  decorators: [withKnobs],
};
const acceptTypes = [
  'image/*',
  'audio/*',
  'video/*',
  'text/plain',
  'application/*',
  'application/pdf',
  'application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'application/vnd.ms-excel',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/zip',
  'application/x-zip-compressed',
];

export const FilePreview = () => ({
  components: { CsFilePreview },
  props: {
    src: {
      default: text('src', 'https://www.google.com'),
    },
    fileType: {
      default: select('file-type', acceptTypes, 'text/plain'),
    },
    title: {
      default: text('title', 'Indonesia Architecture informational document for training'),
    },
    fileSize: {
      default: number('file-size', 1024),
    },
    showHeader: {
      default: boolean('show-header', true),
    },
    linkIcon: {
      default: text('link-icon'),
    },
    fileIcon: {
      default: text('file-icon'),
    },
    attachmentLabel: {
      default: text('attachment-label'),
    },
  },
  template: `
  <div style="margin-top: 10px">
    <cs-file-preview
        :src='src'
        :file-type='fileType'
        :title='title'
        :file-size='fileSize'
        :show-header='showHeader'
        :link-icon='linkIcon'
        :attachment-label='attachmentLabel'
        :file-icon='fileIcon'
        @open='onOpen'
    >
    </cs-file-preview>
    </div>
    `,
  methods: { onOpen: action('open') },
});
