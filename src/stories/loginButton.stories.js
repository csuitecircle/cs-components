import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, select, color } from '@storybook/addon-knobs';

import CsLoginButton from '../components/LoginButton.vue';

export default {
  component: CsLoginButton,
  title: 'Login Button',
  decorators: [withKnobs],
};

const network = ['default', 'google', 'microsoft', 'facebook', 'linkedin', 'twitter'];

const sizeOptions = ['small', 'medium', 'large'];

const fillOptions = ['solid', 'clear', 'outline'];

const cornerOptions = ['round', 'rounded', 'straight'];

export const LoginButton = () => ({
  components: { CsLoginButton },
  props: {
    network: {
      default: select('network', network, 'google'),
    },
    icon: {
      default: text('icon'),
    },
    // These are just passed on to the button component
    href: {
      default: text('href', ''),
    },
    to: {
      default: text('to (Router link)', ''),
    },
    fill: {
      default: select('fill', fillOptions, 'solid'),
    },
    corners: {
      default: select('corners', cornerOptions),
    },
    size: {
      default: select('size', sizeOptions, 'medium'),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    block: {
      default: boolean('block', false),
    },
    padding: {
      default: text('--cs-button-padding'),
    },
    color: {
      default: color('--cs-button-color'),
    },
    hoverColor: {
      default: color('--cs-button-color-hover'),
    },
    fontColor: {
      default: color('--cs-button-text-color'),
    },
    hoverFontColor: {
      default: color('--cs-button-text-color-hover'),
    },
  },
  template: `
    <div>
      <cs-login-button
        :network="network"
        :icon="icon"
        @click="action"
        :href="href"
        :to="to"
        :disabled="disabled"
        :fill="fill"
        :corners="corners"
        :block="block"
        :size="size"
        :style="{
          '--cs-button-padding': padding,
          '--cs-button-color': color,
          '--cs-button-color-hover': hoverColor,
          '--cs-button-text-color': fontColor,
          '--cs-button-text-color-hover': hoverFontColor,
        }"
      />
      <cs-login-button
        :network="network"
        :icon="icon"
        @click="action"
        :href="href"
        :to="to"
        :disabled="disabled"
        :fill="fill"
        :corners="corners"
        :block="block"
        :size="size"
        :style="{
          '--cs-button-color': color,
          '--cs-button-color-hover': hoverColor,
          '--cs-button-text-color': fontColor,
          '--cs-button-text-color-hover': hoverFontColor,
        }"
      >
        Slot Login Button
      </cs-login-button>
    </div>
    `,
  methods: { action: action('clicked') },
});
