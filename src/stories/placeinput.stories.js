import {
  withKnobs,
  text,
  number,
  object,
  boolean,
  select,
} from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import CsPlaceInput from '../components/PlaceInput.vue';
import CsMap from '../components/Map.vue';

import { placeEvents } from '@/utils/formevents';
import formEventsMixin from './mixins/formevents';

export default {
  title: 'Form/Place Input',
  component: CsPlaceInput,
  decorators: [withKnobs],
};

const acceptedTypes = [
  '',
  'geocode',
  'address',
  'establishment',
  '(regions)',
  '(cities)',
];

export const Default = () => ({
  mixins: [formEventsMixin],
  components: { CsPlaceInput },
  data() {
    return {
      $_formEventTriggers: placeEvents,
    };
  },
  props: {
    api: {
      default: text('api-key', ''),
    },
    label: {
      default: text('label', 'Place Input'),
    },
    value: {
      default: text('value', 'ChIJL_P_CXMEDTkRw0ZdG-0GVvw'),
    },
    placeholder: {
      default: text('placeholder'),
    },
    required: {
      default: boolean('required', false),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    icon: {
      default: text('icon'),
    },
    type: {
      default: select('type', acceptedTypes),
    },
    missingApiKeyLabel: {
      default: text('missing-api-key-label'),
    },
    noResultsLabel: {
      default: text('no-results-label'),
    },
    startTypingLabel: {
      default: text('start-typing-label'),
    },
    requiredLabel: {
      default: text('requiredLabel', '* Required'),
    },
    errorIcon: {
      default: text('errorIcon', 'cs-icons-error'),
    },
  },
  template: `
		<div>
	    <cs-place-input
				:api-key="api"
				:label="label"
				:value="value"
				:placeholder="placeholder"
				:required="required"
				:disabled="disabled"
				:icon="icon"
				:missing-api-key-label="missingApiKeyLabel"
				:no-results-label="noResultsLabel"
				:start-typing-label="startTypingLabel"
				:type="type"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
				@resolve="resolve"
				@input="input"
        @validate="onValidate"
        v-on="$_formEvents"
			/>
      <cs-place-input
				:api-key="api"
				:label="label"
				:value="value"
				:placeholder="placeholder"
				:required="required"
				:disabled="disabled"
				:icon="icon"
				:missing-api-key-label="missingApiKeyLabel"
				:no-results-label="noResultsLabel"
				:start-typing-label="startTypingLabel"
				:type="type"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        :success="true"
				@resolve="resolve"
				@input="input"
        @validate="onValidate"
			>
        <cs-input-note slot="input-notes" v-if="true" type="success" label="This is a success message"/>
      </cs-place-input>
      <cs-place-input
				:api-key="api"
				:label="label"
				:value="value"
				:placeholder="placeholder"
				:required="required"
				:disabled="disabled"
				:icon="icon"
				:missing-api-key-label="missingApiKeyLabel"
				:no-results-label="noResultsLabel"
				:start-typing-label="startTypingLabel"
				:type="type"
        :required-label="requiredLabel"
        :error-icon="errorIcon"
        :invalid="true"
				@resolve="resolve"
				@input="input"
        @validate="onValidate"
			>
        <cs-input-note slot="input-notes" v-if="value !== null" type="info" label="This is a default message"/>
        <cs-input-note slot="input-notes" v-if="value !== null" type="error" label="This is an error message"/>
      </cs-place-input>
      <br>
			<div><a href="https://developers.google.com/maps/documentation/javascript/reference/places-autocomplete-service#AutocompletionRequest" target="_blank">
				Autocomplete Request Options
			</a> (using <code>location</code>, <code>radius</code>, <code>types</code>)</div>
			<div><a href="https://developers.google.com/places/supported_types#table3" target="_blank">
				Supported Types
			</a></div>
		</div>
  `,
  methods: {
    resolve: action('resolve'),
    input: action('input'),
    onValidate: action('validate'),
  },
});

export const WithOptions = () => ({
  components: { CsPlaceInput },
  props: {
    api: {
      default: text('api-key', ''),
    },
    label: {
      default: text('label', 'Place Input'),
    },
    value: {
      default: text('v-model', 'ChIJyY4rtGcX2jERIKTarqz3AAQ'),
    },
    placeholder: {
      default: text('placeholder'),
    },
    required: {
      default: boolean('required', false),
    },
    disabled: {
      default: boolean('disabled', false),
    },
    location: {
      default: object('location [not yet supported]', {
        lat: 1.3143394,
        lng: 103.7038237,
      }),
    },
    radius: {
      default: number('radius (meters) [not yet supported]'),
    },
    type: {
      default: select('type', acceptedTypes),
    },
    missingApiKeyLabel: {
      default: text('missing-api-key-label'),
    },
    noResultsLabel: {
      default: text('no-results-label'),
    },
    startTypingLabel: {
      default: text('start-typing-label'),
    },
  },
  template: `
		<div>
	    <cs-place-input
				:api-key="api"
				:label="label"
				:value="value"
				:location="location"
				:radius="radius"
				:type="type"
				:missing-api-key-label="missingApiKeyLabel"
				:no-results-label="noResultsLabel"
				:start-typing-label="startTypingLabel"
				@resolve="resolve"
				@input="input"
			/>
			<div><a href="https://developers.google.com/maps/documentation/javascript/reference/places-autocomplete-service#AutocompletionRequest" target="_blank">
				Autocomplete Request Options
			</a> (using <code>location</code>, <code>radius</code>, <code>types</code>)</div>
			<div><a href="https://developers.google.com/places/supported_types#table3" target="_blank">
				Supported Types
			</a></div>
		</div>
  `,
  methods: {
    resolve: action('resolve'),
    input: action('input'),
    onValidate: action('validate'),
  },
});

const markers = [
  {
    title: 'Bondi Beach',
    lat: -33.890542,
    lng: 151.274856,
  },
  {
    title: 'Manly Beach',
    lat: -33.80010128657071,
    lng: 151.28747820854187,
  },
  {
    title: 'Cronulla Beach',
    lat: -34.028249,
    lng: 151.157507,
  },
  {
    title: 'Maroubra Beach',
    lat: -33.950198,
    lng: 151.259302,
  },
  {
    title: 'Coogee Beach',
    lat: -33.923036,
    lng: 151.259052,
  },
];

export const WithMap = () => ({
  components: { CsPlaceInput, CsMap },
  props: {
    api: {
      default: text('api-key', ''),
    },
    label: {
      default: text('label', 'Place Input'),
    },
    value: {
      default: text('v-model', 'ChIJyY4rtGcX2jERIKTarqz3AAQ'),
    },
    type: {
      default: select('type', acceptedTypes),
    },
    center: {
      default: object('center (for map)', { lat: -33.890542, lng: 151.274856 }),
    },
    markers: {
      default: object('markers (for map)', markers),
    },
    missingApiKeyLabel: {
      default: text('missing-api-key-label'),
    },
    noResultsLabel: {
      default: text('no-results-label'),
    },
    startTypingLabel: {
      default: text('start-typing-label'),
    },
  },
  template: `
		<div>
			<div>Working example showing both maps and place-input loaded (data is unrelated)</div>
	    <cs-place-input
				:api-key="api"
				:label="label"
				:value="value"
				:type="type"
				:missing-api-key-label="missingApiKeyLabel"
				:no-results-label="noResultsLabel"
				:start-typing-label="startTypingLabel"
				@resolve="resolve"
				@input="input"
			/>
	    <cs-map
				:api-key="api"
				:center="center"
				:markers="markers"
			/>
		</div>
  `,
  methods: {
    resolve: action('resolve'),
    input: action('input'),
    onValidate: action('validate'),
  },
});
