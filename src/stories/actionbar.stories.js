import { withKnobs, color } from '@storybook/addon-knobs';
import CsActionBar from '../components/ActionBar.vue';
import CsActionBarButton from '../components/ActionBarButton.vue';
import CsButton from '../components/Button.vue';

export default {
	component: CsActionBar,
	title: 'ActionBar',
	decorators: [withKnobs],
};

export const WithCSActionBarButtonComponent = () => ({
	components: { CsActionBar, CsActionBarButton },
	props: {
		color: {
			default: color('highlight', '#00C472'),
		},
	},
	data() {
		return {
			liked: false,
		};
	},
	template: `
    <cs-action-bar>
      <cs-action-bar-button @click="toggleLike" :highlighted="liked"  :style="{'--cs-action-button-highlight-color': color}">
        <i class="cs-icons-like-filled" slot="icon"></i>
        Like
      </cs-action-bar-button>
      <cs-action-bar-button>
        <i class="cs-icons-share-filled" slot="icon"></i>
        Share
      </cs-action-bar-button>
      <cs-action-bar-button>
        <i class="cs-icons-invite-filled" slot="icon"></i>
        Invite
      </cs-action-bar-button>
    </cs-action-bar>
  `,
	methods: {
		toggleLike() {
			this.liked = !this.liked;
		},
	},
});
WithCSActionBarButtonComponent.parameters = {
	viewport: {
		defaultViewport: 'mobile2',
	},
};

export const WithCSButtonComponent = () => ({
	components: { CsActionBar, CsButton },
	props: {
		color: {
			default: color('highlight', '#00C472'),
		},
	},
	data() {
		return {
			liked: false,
		};
	},
	template: `
    <cs-action-bar>
      <cs-button
        :variant="'default'"
        :fill="'clear'"
        @click="toggleLike"
        :style="{
          '--cs-button-color': liked ? color: '',
          '--cs-button-color-hover': liked ? color: ''}"
      >
        <i class="cs-icons-like-filled"></i>
        <label>Like</label>
      </cs-button>
      <cs-button :variant="'default'" :fill="'clear'">
        <i class="cs-icons-share-filled"></i>
        <label>Share</label>
      </cs-button>
      <cs-button :variant="'default'" :fill="'clear'" >
        <i class="cs-icons-invite-filled"></i>
        <label>Invite</label>
      </cs-button>
    </cs-action-bar>
  `,
	methods: {
		toggleLike() {
			this.liked = !this.liked;
		},
	},
});
WithCSButtonComponent.parameters = {
	viewport: {
		defaultViewport: 'mobile2',
	},
};

export const WithHtmlElement = () => ({
	components: { CsActionBar },
	props: {
		color: {
			default: color('highlight', '#00C472'),
		},
	},
	data() {
		return {
			liked: false,
		};
	},
	template: `
    <cs-action-bar>
      <div style="padding:7px;display:flex;align-items:center;" @click="toggleLike" :style="{'color' : liked ? color : ''}">
        <i class="cs-icons-like-filled"></i>
        <label style="margin-left:8px;">Like</label>
      </div>
      <div style="padding:7px;display:flex;align-items:center;">
        <i class="cs-icons-share-filled"></i>
        <label style="margin-left:8px;">Share</label>
      </div>
      <div style="padding:7px;display:flex;align-items:center;">
        <i class="cs-icons-invite-filled" ></i>
        <label style="margin-left:8px;">Invite</label>
      </div>
    </cs-action-bar>
  `,
	methods: {
		toggleLike() {
			this.liked = !this.liked;
		},
	},
});
WithHtmlElement.parameters = {
	viewport: {
		defaultViewport: 'mobile2',
	},
};
