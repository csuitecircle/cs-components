import '@/assets/css/csuite.global.css';
import Vue from 'vue';
import { kebabCase } from 'lodash-es';

const requireComponent = require.context('./', false, /\.(vue|js)$/);

requireComponent.keys().forEach((fileName) => {
  const componentConfig = requireComponent(fileName);
  const split = fileName.split('/');
  const pop = split.pop();
  const replace = pop.replace(/\.\w+$/, '');
  const kebabcase = kebabCase(replace);
  const componentName = `cs-${kebabcase}`;
  Vue.component(componentName, componentConfig.default || componentConfig);
});

// export default components

/* README
This file automatically registers component which are in the same folder (deeply)
You will not need to export anything from this file, example code provided and commented anyways
You will only need this file if the user wants to use every component
You just need to import this file and and use component without importing. Look main.js and App.vue
*/

// All components emit a click event
Vue.mixin({
  mounted() {
    this.$el.addEventListener('click', (e) => this.$emit('click', e));
  },
});
