import Quill from 'quill';
import wrap from '@vue/web-component-wrapper';
import Vue from 'vue';
import CsFilePreview from './FilePreview.vue';

const BlockEmbed = Quill.import('blots/block/embed');

const fileBlot = wrap(Vue, CsFilePreview);
window.customElements.define('quill-file-blot', fileBlot);
class FileBlot extends BlockEmbed {
  static create(value) {
    const node = super.create();
    node.setAttribute('title', value.name);
    node.setAttribute('src', value.url);
    node.setAttribute('file-type', value.type);
    node.setAttribute('file-size', value.size);
    node.data = value;
    return node;
  }

  static value(node) {
    return node.data;
  }
}
FileBlot.blotName = 'file';
FileBlot.tagName = 'quill-file-blot';
FileBlot.className = 'quill-custom-blot';

export default FileBlot;
