import Quill from 'quill';
import wrap from '@vue/web-component-wrapper';
import Vue from 'vue';
import CsAudioPlayer from './AudioPlayer.vue';

const BlockEmbed = Quill.import('blots/block/embed');

const audioBlot = wrap(Vue, CsAudioPlayer);
window.customElements.define('quill-audio-blot', audioBlot);

class AudioBlot extends BlockEmbed {
  static create(value) {
    const node = super.create();
    node.setAttribute('src', value.url);
    node.setAttribute('title', value.name);
    node.data = value;
    return node;
  }

  static value(node) {
    return node.data;
  }
}
AudioBlot.blotName = 'audio';
AudioBlot.tagName = 'quill-audio-blot';
AudioBlot.className = 'quill-custom-blot';

export default AudioBlot;
