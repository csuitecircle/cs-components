import Quill from 'quill';

const BlockEmbed = Quill.import('blots/block/embed');

// IMAGE BLOT
class ImageBlot extends BlockEmbed {
  static create(value) {
    const node = super.create();
    node.setAttribute('src', value.url);
    node.data = value;
    return node;
  }

  static value(node) {
    return node.data;
  }
}
ImageBlot.blotName = 'image';
ImageBlot.tagName = 'img';
ImageBlot.className = 'quill-custom-blot';

export default ImageBlot;
