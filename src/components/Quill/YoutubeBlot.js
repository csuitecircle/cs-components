import Quill from 'quill';

const BlockEmbed = Quill.import('blots/block/embed');

// YOUTUBE BLOT
class YoutubeBlot extends BlockEmbed {
  static create(value) {
    const node = super.create();
    node.setAttribute('src', `https://www.youtube.com/embed/${value.ytid}`);
    node.setAttribute('frameborder', '0');
    node.setAttribute('allowfullscreen', 'yes');
    node.data = value;
    return node;
  }

  static value(node) {
    return node.data;
  }
}
YoutubeBlot.blotName = 'youtube';
YoutubeBlot.tagName = 'iframe';
YoutubeBlot.className = 'quill-custom-blot';

export default YoutubeBlot;
