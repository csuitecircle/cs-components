import Quill from 'quill';
import wrap from '@vue/web-component-wrapper';
import Vue from 'vue';
import CsVideoPlayer from './VideoPlayer.vue';

const BlockEmbed = Quill.import('blots/block/embed');

// VIDEO BLOT
const videoBlot = wrap(Vue, CsVideoPlayer);
window.customElements.define('quill-video-blot', videoBlot);
class VideoBlot extends BlockEmbed {
  static create(value) {
    const node = super.create();
    node.setAttribute('src', value.url);
    node.data = value;
    return node;
  }

  static value(node) {
    return node.data;
  }
}
VideoBlot.blotName = 'video';
VideoBlot.tagName = 'quill-video-blot';
VideoBlot.className = 'quill-custom-blot';

export default VideoBlot;
