export default {
  data() {
    return {
      $_formEventTriggers: [],
    };
  },
  computed: {
    $_formEvents() {
      if (!this.$data.$_formEventTriggers) return {};
      const events = {};
      this.$data.$_formEventTriggers.forEach((e) => {
        events[e] = ($event) => {
          this.$emit($event.type, $event);
        };
      });
      return events;
    },
  },
};
