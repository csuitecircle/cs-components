export default {
  data() {
    return {
      googleMaps: {
        init(apikey) {
          // Resolves when maps are loaded.  Rejects if there is an error loading
          return new Promise((resolve, reject) => {
            if (!apikey) return reject(new Error('Missing Api Key'));

            // Initial timeout of a random time to stop two components trying to create the map script at the same time
            return setTimeout(() => {
              const gms = document.body.querySelector('.google-map__script');
              if (window.google) {
                resolve();
              } else if (gms) {
                // If google is not loaded but the script exists, keep checking until it's ready
                if (gms.getAttribute('data-loadedstatus') === 'ready') {
                  resolve();
                } else {
                  const checkInterval = window.setInterval(() => {
                    const igms = document.body.querySelector(
                      '.google-map__script'
                    );
                    const status = igms.getAttribute('data-loadedstatus');
                    if (window.google) {
                      // If google is loaded, stop the interval and resolve
                      resolve();
                      window.clearInterval(checkInterval);
                    } else if (status === 'error') {
                      // If there is an error, stop the interval
                      window.clearInterval(checkInterval);
                      reject(new Error('Failed to load script'));
                    }
                  }, 100);
                }
              } else {
                // Create the script and set it to load
                const googleMapScript = document.createElement('script');
                googleMapScript.setAttribute('data-loadedstatus', 'loading');
                googleMapScript.addEventListener('load', (load) => {
                  googleMapScript.setAttribute('data-loadedstatus', 'ready');
                  resolve(load);
                });
                googleMapScript.addEventListener('error', (err) => {
                  reject(new Error(err));
                  googleMapScript.setAttribute('data-loadedstatus', 'error');
                  // Final timeout to remove the errored script after any currently running intervals get cleared
                  setTimeout(() => {
                    const scripts = document.querySelectorAll(
                      '.google-map__script'
                    );
                    for (let i = 0; i < scripts.length; i += 1) {
                      if (scripts[i]) scripts[i].remove();
                    }
                  }, 250);
                });
                googleMapScript.className = 'google-map__script';
                googleMapScript.async = true;
                googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=${apikey}&libraries=places`;
                document.body.appendChild(googleMapScript);
              }
            }, Math.random() * 100);
          });
        },
      },
    };
  },
};
