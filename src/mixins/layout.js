import { ResponsiveMixin } from 'vue-responsive-components';
import { layoutValidator } from '@/utils/validators';

export default {
  mixins: [ResponsiveMixin],
  props: {
    layout: {
      type: String,
      default: null,
      validator: layoutValidator,
    },
  },
  breakpoints: {
    small: (el) => el.width < 768,
    medium: (el) => el.width >= 768 && el.width < 1200,
    large: (el) => el.width >= 1200,
  },
  computed: {
    layoutSize() {
      if (this.layout) return this.layout;
      if (this.el.is.small) return 'small';
      if (this.el.is.medium) return 'medium';
      if (this.el.is.large) return 'large';
      return null;
    },
    layoutSizeClass() {
      const tagName = this.$options._componentTag;
      const layoutSize = this.layoutSize;
      if (layoutSize) return `${tagName}--layout-${layoutSize}`;
      return '';
    },
  },
};
