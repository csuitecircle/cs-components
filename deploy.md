1. BitBucket: Merge any approved pull requests into master
2. Jira: Mark any approved tasks on with the "Release version" (currently 8.x)
3. Jira: Go to the "Releases" page on CS Components Project > select the appropriate release version > click the "Release notes" button > set Format to "Markdown" > click "Copy to clipboard" button
4. Paste the copied text into the appropriate place of README.md file in the cs-components project and commit your changes (master branch)
5. yarn build-bundle on master branch and commit those changes
6. Ensure you have install the np package globally (https://www.npmjs.com/package/np)
7. In the cs-components project on the command line run: np
8. Select the right semantic version A.B.C - A = breaking changes, B = new feature, C = patch. Np should do the rest and push your changes.
