import { addons } from '@storybook/addons';
import csuiteTheme from './csuiteTheme';

addons.setConfig({
    theme: csuiteTheme,
});
