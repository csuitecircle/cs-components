import './reset.css';
import './cs-main-header-debug.css';
import '../../src/assets/css/csuite.global.css';

import {
  INITIAL_VIEWPORTS,
  MINIMAL_VIEWPORTS,
} from '@storybook/addon-viewport';

export const parameters = {
  layout: 'none',
  viewport: {
    viewports: {
      ...MINIMAL_VIEWPORTS,
      desktop: {
        name: 'Desktop',
        styles: {
          width: '1280px',
          height: '1024px',
        },
      },
      // ...INITIAL_VIEWPORTS
    },
  },
};
