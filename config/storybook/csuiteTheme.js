import { create } from '@storybook/theming/create';

export default create({
  base: 'light',
  brandTitle: '<h1 style="font-weight: 700; color: #000112;">C-Suite Library</h1>',
  appContentBg: '#F2F4F5'
});