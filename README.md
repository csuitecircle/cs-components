# CS Components

## Installing C-Suite components in your project

```
yarn add cs-components

```

### Import the package in main.js

```
import 'cs-components';
import 'cs-components/dist/csuite.css';
```

# Release notes - CS Components - Version 13.x

### Breaking Changes

- FilterHeader and SubcategoryHeader components have both been removed

- MainHeader and SecondaryHeader components were significantly changed with new slots, styles, functionality

### Bug

[CC-374](https://csuitecircle.atlassian.net/browse/CC-374) Audio and Video player error labels not working

[CC-396](https://csuitecircle.atlassian.net/browse/CC-396) Event Card - The date/time container and icon still appear even if date and time are not set

[CC-425](https://csuitecircle.atlassian.net/browse/CC-425) Select component: icon overlaps text when the field size is small

[CC-444](https://csuitecircle.atlassian.net/browse/CC-444) File Upload error div renders even without error message

[CC-472](https://csuitecircle.atlassian.net/browse/CC-472) Modal doesn't close when clicking on backdrop

[CC-475](https://csuitecircle.atlassian.net/browse/CC-475) Fix issues with File Input Component

[CC-480](https://csuitecircle.atlassian.net/browse/CC-480) Fix checkbox input icon

[CC-484](https://csuitecircle.atlassian.net/browse/CC-484) Fix checkbox component

[CC-485](https://csuitecircle.atlassian.net/browse/CC-485) File Input should be appended to DOM

[CC-487](https://csuitecircle.atlassian.net/browse/CC-487) Form validation issues

### Subtask

[CC-426](https://csuitecircle.atlassian.net/browse/CC-426) Page Tabs

[CC-427](https://csuitecircle.atlassian.net/browse/CC-427) Tab Bar

[CC-428](https://csuitecircle.atlassian.net/browse/CC-428) Remove deprecated headers

[CC-429](https://csuitecircle.atlassian.net/browse/CC-429) Main Header

[CC-430](https://csuitecircle.atlassian.net/browse/CC-430) Secondary Header

[CC-431](https://csuitecircle.atlassian.net/browse/CC-431) Event Card 2.0

[CC-465](https://csuitecircle.atlassian.net/browse/CC-465) Message Box: texts

[CC-466](https://csuitecircle.atlassian.net/browse/CC-466) Map and Static Map

[CC-467](https://csuitecircle.atlassian.net/browse/CC-467) Link Preview

[CC-468](https://csuitecircle.atlassian.net/browse/CC-468) File Preview

### Task

[CC-363](https://csuitecircle.atlassian.net/browse/CC-363) Add native events to Input Components

[CC-414](https://csuitecircle.atlassian.net/browse/CC-414) Post component: make the default counter labels respect singular and plural

[CC-469](https://csuitecircle.atlassian.net/browse/CC-469) Event Card: allow hiding the attending buttons

[CC-470](https://csuitecircle.atlassian.net/browse/CC-470) Event Card - remove top margin of title when there are no tags \(image position: top, layout: small\)

[CC-471](https://csuitecircle.atlassian.net/browse/CC-471) Event Card: display organizer with ellipses if text too long

[CC-473](https://csuitecircle.atlassian.net/browse/CC-473) Update file input component functionality

[CC-476](https://csuitecircle.atlassian.net/browse/CC-476) CS-Tag colors - update default colors to the ones in Discovery app

[CC-479](https://csuitecircle.atlassian.net/browse/CC-479) Update font icon

[CC-488](https://csuitecircle.atlassian.net/browse/CC-488) Add new icon font

# Release notes - CS Components - Version 12.x

### Task

[CC-423](https://csuitecircle.atlassian.net/browse/CC-423) Component responsiveness relative to the parent size

[CC-395](https://csuitecircle.atlassian.net/browse/CC-395) Create Checkbox Group component

[CC-368](https://csuitecircle.atlassian.net/browse/CC-368) General error and validation handling for form fields

### Breaking Changes

- Card, ArticleCard, EventCard, Comment, MessageBox, FilePreview, LinkPreview, Modal, Navbar, Pagination, StaticMap components all now support changing their layout based on their width / parent width.

# Release notes - CS Components - Version 11.x

### Breaking Changes

- Comment component: The reaction bar has been changed and adapted to a more general approach, so that multiple buttons or counters can be added, alongside their specific configuration. The Events that they emit are also changed in a more generalized approach: we how have the concept of action-buttons and counters.

### Bug

[CC-452](https://csuitecircle.atlassian.net/browse/CC-452) Message Box: in FOA, the height of textarea is 0

[CC-443](https://csuitecircle.atlassian.net/browse/CC-443) Modal footer renders even when empty

[CC-442](https://csuitecircle.atlassian.net/browse/CC-442) Event Card: Click event triggered on RSVP

[CC-424](https://csuitecircle.atlassian.net/browse/CC-424) Profile component: Fix CSS of name/detail/role when avatar small and position left

### Subtask

[CC-405](https://csuitecircle.atlassian.net/browse/CC-405) Message Item

[CC-404](https://csuitecircle.atlassian.net/browse/CC-404) Conversation Item

[CC-390](https://csuitecircle.atlassian.net/browse/CC-390) Comment

### Task

[CC-453](https://csuitecircle.atlassian.net/browse/CC-453) Tag: make cursor normal \(not pointer\)

[CC-448](https://csuitecircle.atlassian.net/browse/CC-448) File Input: automatically detect graphexpress ID from env

[CC-447](https://csuitecircle.atlassian.net/browse/CC-447) File Input: Add file object as param on selected event

[CC-446](https://csuitecircle.atlassian.net/browse/CC-446) Navigation Page Link: add :to router property

[CC-445](https://csuitecircle.atlassian.net/browse/CC-445) File Upload: allow Change button to be hidden

[CC-421](https://csuitecircle.atlassian.net/browse/CC-421) Article Card: update card for when we have only 1 social icon \(option\)

# Release notes - CS Components - Version 10.x

### Breaking Changes

- Post Card does not have props based on likes, comments and shares; instead there are more general mechanisms to set the labels and icons of buttons or counters, as well as to trigger events;

- Button Group allows for more than 1 selected button, which means that the value of the component is an array now, instead of a string as before. Also the events will provide array of objects instead of values.

### Bug

[CC-417](https://csuitecircle.atlassian.net/browse/CC-417) Article card: The social icons should be aligned on the left only on mobile view

[CC-415](https://csuitecircle.atlassian.net/browse/CC-415) Avatar: component triggers error when name is not set

[CC-413](https://csuitecircle.atlassian.net/browse/CC-413) Button Group: single-select - even if there is no button selected, all buttons are faded

[CC-412](https://csuitecircle.atlassian.net/browse/CC-412) Article Card: if you set its style as inline block, picture-position=left does not work anymore

[CC-410](https://csuitecircle.atlassian.net/browse/CC-410) Remove console logs about mobile view

[CC-399](https://csuitecircle.atlassian.net/browse/CC-399) Article Card - the options slot is not shown if author, date or time to read are not set

### Subtask

[CC-406](https://csuitecircle.atlassian.net/browse/CC-406) Message Box

[CC-389](https://csuitecircle.atlassian.net/browse/CC-389) Post Card

[CC-385](https://csuitecircle.atlassian.net/browse/CC-385) Video Player

[CC-384](https://csuitecircle.atlassian.net/browse/CC-384) Audio Player

### Task

[CC-420](https://csuitecircle.atlassian.net/browse/CC-420) Event Card - Add icon for Event price

[CC-419](https://csuitecircle.atlassian.net/browse/CC-419) Audio Player: allow transparent background

[CC-418](https://csuitecircle.atlassian.net/browse/CC-418) Article Card: allow color change on tags

[CC-411](https://csuitecircle.atlassian.net/browse/CC-411) Add fill in Button Group

[CC-409](https://csuitecircle.atlassian.net/browse/CC-409) Update Button Group component to allow multiple selection

[CC-408](https://csuitecircle.atlassian.net/browse/CC-408) Update Article Component footer positioning of icons

[CC-407](https://csuitecircle.atlassian.net/browse/CC-407) Update Article Card to contain the image of author

[CC-400](https://csuitecircle.atlassian.net/browse/CC-400) Implement eslint recommended linting

# Release notes - CS Components - Version 9.x

### Breaking Changes

- Modal does not have the Buttons slot anymore - it was replaced with separate slots for different buttons based on their role.

- Event does not support the Details slot anymore.

### Bug

[CC-397](https://csuitecircle.atlassian.net/browse/CC-397) Event Card - Image not changing to top position when moving from desktop to mobile view \+ errors in console

[CC-394](https://csuitecircle.atlassian.net/browse/CC-394) Modal buttons not set correctly in stories

[CC-392](https://csuitecircle.atlassian.net/browse/CC-392) Date and Time inputs picker not opening anymore

[CC-391](https://csuitecircle.atlassian.net/browse/CC-391) Fix all inputs whose state does not change through value prop

[CC-388](https://csuitecircle.atlassian.net/browse/CC-388) State of checkbox input not changing through value prop

[CC-382](https://csuitecircle.atlassian.net/browse/CC-382) Toggle Text is bold when unchecked

### Subtask

[CC-387](https://csuitecircle.atlassian.net/browse/CC-387) Link Preview

[CC-383](https://csuitecircle.atlassian.net/browse/CC-383) Article Card

[CC-365](https://csuitecircle.atlassian.net/browse/CC-365) Event Card

[CC-348](https://csuitecircle.atlassian.net/browse/CC-348) Checkbox

[CC-343](https://csuitecircle.atlassian.net/browse/CC-343) Modal

# Release notes - CS Components - Version 8.x

### Breaking Changes

- Remove all Lato fonts (now using Roboto) and only kept .woff2 file types

### Subtask

[CC-364](https://csuitecircle.atlassian.net/browse/CC-364) Card

[CC-362](https://csuitecircle.atlassian.net/browse/CC-362) Social Media Icons

[CC-360](https://csuitecircle.atlassian.net/browse/CC-360) Toggle and Toggle Group

[CC-359](https://csuitecircle.atlassian.net/browse/CC-359) Time Input

[CC-358](https://csuitecircle.atlassian.net/browse/CC-358) Textarea

[CC-357](https://csuitecircle.atlassian.net/browse/CC-357) Place Input

[CC-350](https://csuitecircle.atlassian.net/browse/CC-350) File Input

[CC-349](https://csuitecircle.atlassian.net/browse/CC-349) Date

[CC-344](https://csuitecircle.atlassian.net/browse/CC-344) Profile

[CC-333](https://csuitecircle.atlassian.net/browse/CC-333) Input

# Release notes - CS Components - Version 7.x

### Breaking Changes

- CC-351 RadioButtons have been removed and replaced with RadioGroup

### Task

[CC-354](https://csuitecircle.atlassian.net/browse/CC-354) \[TICKET\]\[\] Add new textstyle: Paragraph Extra Small

[CC-353](https://csuitecircle.atlassian.net/browse/CC-353) \[TICKET\]\[IR-84\] Reactions and alignment update for Post Component

[CC-352](https://csuitecircle.atlassian.net/browse/CC-352) \[TICKET\]\[IR-81\] Add comments slot in the post component

[CC-351](https://csuitecircle.atlassian.net/browse/CC-351) Implement slots for radio buttons in a radio group

[CC-340](https://csuitecircle.atlassian.net/browse/CC-340) Embedded Video Component

[CC-337](https://csuitecircle.atlassian.net/browse/CC-337) \[TICKET\]\[IR-78\] Add replies support in Comment component

# Release notes - CS Components - Version 6.x

### Breaking Changes

- CC-341 NavigationPage now uses NavigationPageLink in place of Button for the links

### Subtask

[CC-347](https://csuitecircle.atlassian.net/browse/CC-347) Pagination

[CC-345](https://csuitecircle.atlassian.net/browse/CC-345) Form

[CC-341](https://csuitecircle.atlassian.net/browse/CC-341) Navigation Page

[CC-331](https://csuitecircle.atlassian.net/browse/CC-331) Tag

[CC-330](https://csuitecircle.atlassian.net/browse/CC-330) Alert Component

### Task

[CC-339](https://csuitecircle.atlassian.net/browse/CC-339) Update Message Box to support attachments

[CC-338](https://csuitecircle.atlassian.net/browse/CC-338) All background and border color of headers should be customizable

[CC-229](https://csuitecircle.atlassian.net/browse/CC-229) Update all typography styles in components

# Release notes - CS Components - Version 5.x

### Breaking Changes

- CC-315 TabBar component now uses `value` property in place of `active-index`. Also emits `input` events to support `v-model` functionality

### Bug

[CC-335](https://csuitecircle.atlassian.net/browse/CC-335) The filter dropdown displays wrong label on load

[CC-327](https://csuitecircle.atlassian.net/browse/CC-327) Floating label is too far on the right with respect to events card in StoryBook story

[CC-322](https://csuitecircle.atlassian.net/browse/CC-322) Custom icon in DateInput and TimeInput component

[CC-321](https://csuitecircle.atlassian.net/browse/CC-321) The test video on storybook seems to not work on Video Player component

[CC-320](https://csuitecircle.atlassian.net/browse/CC-320) \[TICKET\]\[IR-77\] On file preview component, the image icon does not appear for types "image/jpeg" or "image/png"

[CC-316](https://csuitecircle.atlassian.net/browse/CC-316) Message component not rendering if only sender picture or only sender-name is set

[CC-311](https://csuitecircle.atlassian.net/browse/CC-311) Event organiser and location are not respecting the spacing on Event Card

### Task

[CC-342](https://csuitecircle.atlassian.net/browse/CC-342) Update the icon font

[CC-336](https://csuitecircle.atlassian.net/browse/CC-336) Add no. of shares to Post component

[CC-334](https://csuitecircle.atlassian.net/browse/CC-334) Change the Filter Dropdown story to follow the naming convention we use in the components library

[CC-319](https://csuitecircle.atlassian.net/browse/CC-319) Update Alert Component

[CC-315](https://csuitecircle.atlassian.net/browse/CC-315) Adopt a uniform way to set active tabs

[CC-115](https://csuitecircle.atlassian.net/browse/CC-115) Pagination Component

# Release notes - CS Components - Version 4.x

### Breaking Changes

- CC-303 Place input now only has a property `type` that accepts a single string instead of a string array `types`

### Bug

[CC-317](https://csuitecircle.atlassian.net/browse/CC-317) Message box button on desktop viewport occupies the full height of the container

[CC-312](https://csuitecircle.atlassian.net/browse/CC-312) When I don't have tags, the RSVP buttons are aligned to the left

[CC-309](https://csuitecircle.atlassian.net/browse/CC-309) The Event card has a big gap between picture and content on Tablet and Desktop viewport

[CC-308](https://csuitecircle.atlassian.net/browse/CC-308) \[TICKET\]\[IR-76\] File Preview link icon is not consistent with design

[CC-307](https://csuitecircle.atlassian.net/browse/CC-307) \[TICKET\]\[IR-75\] Play icon and error messages are not displayed in the center of the video in Video Player component

[CC-304](https://csuitecircle.atlassian.net/browse/CC-304) Wrong comment icon in Post Component

[CC-303](https://csuitecircle.atlassian.net/browse/CC-303) \[TICKET\]\[IR-74\] Place component accepts an array of types instead of only 1 type

[CC-302](https://csuitecircle.atlassian.net/browse/CC-302) \[TICKET\]\[IR-72\] The menu icon is displayed by default \(instead of being hidden\) in Comment component

[CC-301](https://csuitecircle.atlassian.net/browse/CC-301) \[TICKET\]\[IR-71\] The date in comment component displays the date in a wrong format for previous years

[CC-298](https://csuitecircle.atlassian.net/browse/CC-298) Icon color not consistent on Link Preview component

[CC-296](https://csuitecircle.atlassian.net/browse/CC-296) Icons in Action Bar component are smaller than in the design

### Task

[CC-306](https://csuitecircle.atlassian.net/browse/CC-306) \[TICKET\]\[IR-73\] SelectSearch - rename icon prop to be consistent with icon slot

# Release notes - CS Components - Version 3.x

### Breaking Changes

- CC-218 a number of events have been changed from past tense to present tense:

  - FilterDropdown
    - selected → select
  - PlaceInput
    - resolved → resolve
  - QuillEditor
    - [blotType]-attached → [blotType]-attach
      - image, video, audio, file
    - attachment-added → attachment-add
  - Select
    - selected → select
  - SelectSearch
    - selected → select
  - SmallTag
    - selected → select

- CC-299 - Comment icon was changed from `cs-icons-speechbubble-filled` to `cs-icons-speech-bubble-filled`
- CC-261 - Removed ListItem component
- CC-260 - FloatingLabel component requires target components to be placed inside slot
- CC-219 - Icons prefixed with `icons8-` are no longer supported. There are new `cs-icons-` See our [ZeroHeight icons](https://zeroheight.com/6f45b4c79/p/069998) page for more info
- CC-210 - FileInput `size` property has been renamed to `file-size`
- CC-210 - FileInput expects `value` property to contain valid URLs from previously uploaded files \(if a default value is provided\)

### Possible Style issues

- CC-258 - EmptyState component no longer has a default height. Height should be set by the user
- CC-228 - PostCard default styling for slot content may have changed.
- CC-228 - LinkPreview component also had all the root padding removed.
- CC-210 - FileInput has been updated to look and behave differently

### Bug

[CC-290](https://csuitecircle.atlassian.net/browse/CC-290) \[TICKET\]\[IR-68\] Conversation Item component displays an empty circle if the picture is not provided

[CC-289](https://csuitecircle.atlassian.net/browse/CC-289) \[TICKET\]\[IR-67\] Link preview component broken in parent with display:flex

[CC-260](https://csuitecircle.atlassian.net/browse/CC-260) Floating label is floating over the target element

### Task

[CC-299](https://csuitecircle.atlassian.net/browse/CC-299) Change comment icon from speechbubble to speech-bubble

[CC-293](https://csuitecircle.atlassian.net/browse/CC-293) \[TICKET\]\[IR-69\] New file types in File Preview component

[CC-292](https://csuitecircle.atlassian.net/browse/CC-292) Message component to use Avatar component

[CC-291](https://csuitecircle.atlassian.net/browse/CC-291) \[TICKET\] \[IR-53\] Allow custom padding on button components

[CC-263](https://csuitecircle.atlassian.net/browse/CC-263) Extend Post component with slot for Action Bar

[CC-261](https://csuitecircle.atlassian.net/browse/CC-261) Remove "List Item" from the library

[CC-259](https://csuitecircle.atlassian.net/browse/CC-259) \[TICKET\]\[IR-66\]\[IR-63\] Updates on Select Search

[CC-258](https://csuitecircle.atlassian.net/browse/CC-258) \[TICKET\]\[IR-64\] Allow custom height on Empty State component

[CC-228](https://csuitecircle.atlassian.net/browse/CC-228) \[TICKET\]\[IR-58\] Update Post Card and Action Bar

[CC-219](https://csuitecircle.atlassian.net/browse/CC-219) Replace Icons with new icon-set

[CC-218](https://csuitecircle.atlassian.net/browse/CC-218) Update names of events to present tense

[CC-216](https://csuitecircle.atlassian.net/browse/CC-216) Use standard prop validators

[CC-210](https://csuitecircle.atlassian.net/browse/CC-210) \[TICKET\]\[IR-51\]\[IR-60\]\[IR-65\] File Input component update
