import Alert from '@/components/Alert'
import { mount } from '@vue/test-utils'

describe("Alert component", ()=> {
  test("test title prop", () => {
    const wrapper = mount(Alert, {
      propsData:{
        title: "Alert Title"
      }
    })
    expect(wrapper.find("h5").text()).toContain("Alert Title")
  });
})
