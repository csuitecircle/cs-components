import Alert from '@/components/Alert'
import { mount } from '@vue/test-utils'


test("mount a component", () => {
  const wrapper = mount(Alert)
  expect(wrapper).toMatchSnapshot()
});